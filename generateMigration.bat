rm liquibase.h2.db
rm liquibase.trace.db

call mvn clean install -DskipTests
call mvn liquibase:update
call mvn liquibase:diff

pause