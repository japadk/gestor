FROM maven:3.6.3-jdk-8 AS MAVEN_BUILD

MAINTAINER Marcelo Fudo Rech

COPY pom.xml /build/
COPY src /build/src/

WORKDIR /build/
RUN mvn clean install -Pbuild

FROM openjdk:8-jre-alpine

WORKDIR /app

COPY --from=MAVEN_BUILD /build/target/*.jar /app/app.jar

ENTRYPOINT ["java", "-jar", "app.jar"]
