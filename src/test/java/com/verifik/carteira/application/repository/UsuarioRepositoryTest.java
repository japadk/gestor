package com.verifik.carteira.application.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Optional;

import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.verifik.carteira.application.model.entities.Perfil;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.utils.RandomingUtils;
import com.verifik.carteira.application.utils.TestUtils;
import com.verifik.carteira.entrypoint.main.Main;

@RunWith(SpringRunner.class)
@DataJpaTest(properties = "classpath:application.properties")
@ContextConfiguration(classes=Main.class)
public class UsuarioRepositoryTest {

	@Autowired
    private TestEntityManager entityManager;
 
	@Autowired
    private UsuarioRepository usuarioRepository;
	
	@Autowired
    private PerfilRepository perfilRepository;
    
    @Before
    public void insertPerfil() {
    	
    	for(EPerfil sigla : EPerfil.values()) {
    		
    		Perfil perfil = new Perfil();
    		perfil.setRegistroAtivo(true);
    		perfil.setDescricao(sigla.toString());
    		perfil.setSigla(sigla.toString());
    		entityManager.persist(perfil);
    		
    	}
    	
    }
	
    @Test
	public void whenFindByNome_thenReturnUsuario() {
		
	    Usuario usuario = TestUtils.buildMockUser();
	    Perfil perfil = perfilRepository.findBySiglaIgnoreCase(EPerfil.ATENDENTE.toString());
	    usuario.setPerfil(perfil);
	    entityManager.persist(usuario);
	    entityManager.flush();
	 
	    Usuario found = usuarioRepository.findByNome(usuario.getNome());
	 
	    // then
	    assertThat(found.getNome())
	      .isEqualTo(usuario.getNome());
	}
    
    @Test
	public void whenFindById_thenReturnUsuario() {
		
	    Usuario usuario = TestUtils.buildMockUser();
	    Perfil perfil = perfilRepository.findBySiglaIgnoreCase(EPerfil.ATENDENTE.toString());
	    usuario.setPerfil(perfil);
	    
	    usuario = entityManager.persist(usuario);
	    entityManager.flush();
	 
	    Optional<Usuario> found = usuarioRepository.findById(usuario.getId());
	    
	    Usuario encontrado = found.orElseGet(() -> {
	    	return null;
	    });
	    
	    //Não pode ser null
	    assertNotNull(encontrado);
	    
	    // then
	    assertThat(encontrado.getId())
	      .isEqualTo(usuario.getId());
	}
    
    @Test
    public void whenFindByLogin_thenReturnUsuario() {
    	
    	Usuario usuario = TestUtils.buildMockUser();
    	Perfil perfil = perfilRepository.findBySiglaIgnoreCase(EPerfil.ATENDENTE.toString());
	    usuario.setPerfil(perfil);
	    
	    usuario = entityManager.persist(usuario);
	    entityManager.flush();
	    
	    Usuario found = usuarioRepository.findByLoginIgnoreCase(usuario.getLogin());
	    
	    //Não pode ser null
	    assertNotNull(found);
	    
	    // then
	    assertThat(found.getId())
	      .isEqualTo(usuario.getId());
    	
    }
    
    @Test
    public void whenFindByLoginDifferentCase_thenReturnUsuario() {
    	
    	Usuario usuario = TestUtils.buildMockUser();
    	Perfil perfil = perfilRepository.findBySiglaIgnoreCase(EPerfil.ATENDENTE.toString());
	    usuario.setPerfil(perfil);
	    
	    usuario = entityManager.persist(usuario);
	    entityManager.flush();
	    
	    Usuario found = usuarioRepository.findByLoginIgnoreCase(usuario.getLogin().toUpperCase());
	    
	    //Não pode ser null
	    assertNotNull(found);
	    
	    // then
	    assertThat(found.getId())
	      .isEqualTo(usuario.getId());
    	
    }
    
    @Test(expected = PersistenceException.class)
	public void whenLoginDuplicated_thenThrowException() {
    	
    	String login = RandomingUtils.random(10);
    	
    	Usuario usuario1 = TestUtils.buildMockUser();
	    usuario1.setLogin(login);
	    Perfil perfil = perfilRepository.findBySiglaIgnoreCase(EPerfil.ATENDENTE.toString());
	    usuario1.setPerfil(perfil);
		
	    
	    usuario1 = entityManager.persist(usuario1);
	    entityManager.flush();
	    
	    Usuario usuario2 = TestUtils.buildMockUser();
	    usuario2.setLogin(login);
	    usuario2.setPerfil(perfil);
	    
	    usuario2 = entityManager.persist(usuario2);
	    entityManager.flush();
	    
	    
    }

}
