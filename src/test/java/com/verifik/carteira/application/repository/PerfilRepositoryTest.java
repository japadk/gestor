package com.verifik.carteira.application.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.Optional;

import javax.persistence.PersistenceException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.verifik.carteira.application.model.entities.Perfil;
import com.verifik.carteira.application.utils.TestUtils;
import com.verifik.carteira.entrypoint.main.Main;

@RunWith(SpringRunner.class)
@DataJpaTest(properties = "classpath:application.properties")
@ContextConfiguration(classes=Main.class)
public class PerfilRepositoryTest {

	@Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private PerfilRepository perfilRepositor;
	
    @Test
	public void whenFindByDescricao_thenReturnPerfil() {
		
	    Perfil perfil = TestUtils.buildMockPerfil();
	    entityManager.persist(perfil);
	    entityManager.flush();
	 
	    Perfil found = perfilRepositor.findByDescricaoIgnoreCase(perfil.getDescricao());
	 
	    // then
	    assertThat(found.getDescricao())
	      .isEqualTo(perfil.getDescricao());
	}
    
    @Test
	public void whenFindById_thenReturnPerfil() {
		
	    Perfil perfil = TestUtils.buildMockPerfil();
	    
	    perfil = entityManager.persist(perfil);
	    entityManager.flush();
	 
	    Optional<Perfil> found = perfilRepositor.findById(perfil.getId());
	    
	    Perfil encontrado = found.orElseGet(() -> {
	    	return null;
	    });
	    
	    //Não pode ser null
	    assertNotNull(encontrado);
	    
	    // then
	    assertThat(encontrado.getId())
	      .isEqualTo(perfil.getId());
	}
    
    @Test
    public void whenFindBySigla_thenReturnPerfil() {
    	
    	Perfil perfil = TestUtils.buildMockPerfil();
	    
	    perfil = entityManager.persist(perfil);
	    entityManager.flush();
	    
	    Perfil found = perfilRepositor.findBySiglaIgnoreCase(perfil.getSigla());
	    
	    //Não pode ser null
	    assertNotNull(found);
	    
	    // then
	    assertThat(found.getId())
	      .isEqualTo(perfil.getId());
    	
    }
    
    @Test
    public void whenFindByDescricaoDifferentCase_thenReturnUsuario() {
    	
    	Perfil perfil = TestUtils.buildMockPerfil();
	    
	    perfil = entityManager.persist(perfil);
	    entityManager.flush();
	    
	    Perfil found = perfilRepositor.findByDescricaoIgnoreCase(perfil.getDescricao().toUpperCase());
	    
	    //Não pode ser null
	    assertNotNull(found);
	    
	    // then
	    assertThat(found.getId())
	      .isEqualTo(perfil.getId());
    	
    }
    
    @Test
	public void whenFindBySiglaDifferentCase_thenReturnUsuario() {
		
		Perfil perfil = TestUtils.buildMockPerfil();
	    
	    perfil = entityManager.persist(perfil);
	    entityManager.flush();
	    
	    Perfil found = perfilRepositor.findBySiglaIgnoreCase(perfil.getSigla().toUpperCase());
	    
	    //Não pode ser null
	    assertNotNull(found);
	    
	    // then
	    assertThat(found.getId())
	      .isEqualTo(perfil.getId());
		
	}
    
    @Test(expected = PersistenceException.class)
	public void whenLoginDuplicated_thenThrowException() {
    	
    	Perfil perfil1 = TestUtils.buildMockPerfil();
    	String sigla = perfil1.getSigla();
    	
    	Perfil perfil2 = TestUtils.buildMockPerfil();
    	perfil2.setSigla(sigla);
    	
    	perfil1 = entityManager.persist(perfil1);
	    entityManager.flush();
	    
	    perfil2 = entityManager.persist(perfil2);
	    entityManager.flush();
	    
    }
    

}
