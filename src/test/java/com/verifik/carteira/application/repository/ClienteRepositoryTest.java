package com.verifik.carteira.application.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.model.entities.Cliente;
import com.verifik.carteira.application.utils.RandomingUtils;
import com.verifik.carteira.application.utils.TestUtils;
import com.verifik.carteira.entrypoint.main.Main;

@RunWith(SpringRunner.class)
@DataJpaTest(properties = "classpath:application.properties")
@ContextConfiguration(classes=Main.class)
public class ClienteRepositoryTest {

	@Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private ClienteRepository parceiroRepository;
	
    @Test
	public void whenFindByNome_thenReturnParceiro() {
		
	    Cliente parceiro = TestUtils.buildMockCliente();
	    Carteira carteira = TestUtils.buildMockCarteira();
    	carteira = entityManager.persist(carteira);
    	parceiro.setCarteira(carteira);
	    entityManager.persist(parceiro);
	    entityManager.flush();
	 
	    Cliente found = parceiroRepository.findByNome(parceiro.getNome());
	 
	    // then
	    assertThat(found.getNome())
	      .isEqualTo(parceiro.getNome());
	}
    
    @Test
	public void whenFindById_thenReturnParceiro() {
		
    	Cliente parceiro = TestUtils.buildMockCliente();
    	Carteira carteira = TestUtils.buildMockCarteira();
    	carteira = entityManager.persist(carteira);
    	parceiro.setCarteira(carteira);
	    parceiro = entityManager.persist(parceiro);
	    entityManager.flush();
	 
	    Optional<Cliente> found = parceiroRepository.findById(parceiro.getId());
	    
	    Cliente encontrado = found.orElseGet(() -> {
	    	return null;
	    });
	    
	    //Não pode ser null
	    assertNotNull(encontrado);
	    
	    // then
	    assertThat(encontrado.getId())
	      .isEqualTo(parceiro.getId());
	}
    
    @Test
    public void whenFindByCnpj_thenReturnCliente() {
    	
    	Cliente parceiro = TestUtils.buildMockCliente();
    	Carteira carteira = TestUtils.buildMockCarteira();
    	carteira = entityManager.persist(carteira);
    	parceiro.setCarteira(carteira);
	    parceiro = entityManager.persist(parceiro);
	    entityManager.flush();
	    
	    Cliente found = parceiroRepository.findByCnpj(parceiro.getCnpj());
	    
	    //Não pode ser null
	    assertNotNull(found);
	    
	    // then
	    assertThat(found.getId())
	      .isEqualTo(parceiro.getId());
    	
    }

}
