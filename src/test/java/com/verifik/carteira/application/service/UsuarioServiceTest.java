package com.verifik.carteira.application.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.exceptions.service.usuario.UsuarioLoginDuplicadoException;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.repository.UsuarioRepository;
import com.verifik.carteira.application.service.usuario.IUsuarioService;
import com.verifik.carteira.application.utils.RandomingUtils;
import com.verifik.carteira.application.utils.TestUtils;
import com.verifik.carteira.entrypoint.main.Main;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=Main.class)
@SpringBootTest(properties = "classpath:application.properties")
public class UsuarioServiceTest {
	
	@Autowired
	private IUsuarioService usuarioService;
 
    @MockBean
    private UsuarioRepository usuarioRepository;
    
    @Test
    public void whenValidUsuario_thenReturnUser() throws BaseServiceException {
    	
    	Usuario usuario = TestUtils.buildMockUser();
    	
    	Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuario);
    	
    	Usuario saved = usuarioService.save(usuario);
    	
    	assertThat(saved.getNome()).isEqualTo(usuario.getNome());
    	
    }
    
    @Test(expected = UsuarioLoginDuplicadoException.class)
    public void whenUsuarioLoginDuplicado_thenReturnException() throws BaseServiceException {
    	
    	String loginDuplicado = RandomingUtils.random(10);
    	
    	Usuario usuario = TestUtils.buildMockUser();
    	usuario.setLogin(loginDuplicado);
    	
    	Mockito.when(usuarioRepository.findByLoginIgnoreCase(loginDuplicado)).thenReturn(usuario);
    	
    	Usuario novoUsuario = TestUtils.buildMockUser();
    	novoUsuario.setLogin(loginDuplicado);
    	
    	Usuario saved = usuarioService.save(novoUsuario);
    	
    }
    
}
