package com.verifik.carteira.application.repository;

import com.verifik.carteira.application.model.entities.Divida;
import com.verifik.carteira.application.repository.custom.DividaRepositoryCustom;
import com.verifik.carteira.application.repository.custom.SearchRepositoryCustom;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface DividaRepository extends CrudRepository<Divida, Long>, SearchRepositoryCustom<Divida>, DividaRepositoryCustom {

    Optional<Divida> findById(Long id);

    List<Divida> findAll();

    @Query("SELECT d FROM Divida d  INNER JOIN d.cliente c INNER JOIN c.carteira ca WHERE d.dataPagamento BETWEEN :dataInicio AND :dataFim AND d.pago = true AND c.registroAtivo = true AND ca.registroAtivo = true ORDER BY c.codigo ASC")
    List<Divida> findDividasPagasPorPeriodo(LocalDate dataInicio, LocalDate dataFim);

    @Query("SELECT d FROM Divida d INNER JOIN d.cliente c INNER JOIN c.carteira ca WHERE d.dataVencimento BETWEEN :dataInicio AND :dataFim AND d.pago = false AND c.registroAtivo = true AND ca.registroAtivo = true ORDER BY c.codigo ASC")
    List<Divida> findDividasNaoPagasPorPeriodo(LocalDate dataInicio, LocalDate dataFim);

    @Query("SELECT d FROM Divida d INNER JOIN d.cliente c INNER JOIN c.carteira ca WHERE d.dataVencimento BETWEEN :dataInicio AND :dataFim AND c.registroAtivo = true AND ca.registroAtivo = true ORDER BY c.codigo ASC")
    List<Divida> findDividasPorPeriodo(LocalDate dataInicio, LocalDate dataFim);
}
