package com.verifik.carteira.application.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.repository.custom.SearchRepositoryCustom;

@Repository
public interface CarteiraRepository extends CrudRepository<Carteira, Long>, SearchRepositoryCustom<Carteira> {

	Optional<Carteira> findById(Long id);
	
	Carteira findByNome(String nome);

	List<Carteira> findAll();
	
	@Query("SELECT c FROM Carteira c WHERE c.registroAtivo = true")
	List<Carteira> findAllActive();

}
