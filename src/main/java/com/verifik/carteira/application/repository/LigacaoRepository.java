package com.verifik.carteira.application.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.verifik.carteira.application.model.entities.Divida;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.verifik.carteira.application.model.entities.Ligacao;
import com.verifik.carteira.application.repository.custom.SearchRepositoryCustom;

@Repository
public interface LigacaoRepository extends CrudRepository<Ligacao, Long>, SearchRepositoryCustom<Ligacao> {

	Optional<Ligacao> findById(Long id);

	List<Ligacao> findAll();

	@Query("SELECT l FROM Ligacao l WHERE l.dataCriacao BETWEEN :dataInicio AND :dataFim")
	List<Ligacao> findLigacoesPorPeriodo(LocalDateTime dataInicio, LocalDateTime dataFim);

}
