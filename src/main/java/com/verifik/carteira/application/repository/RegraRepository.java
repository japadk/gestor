package com.verifik.carteira.application.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.verifik.carteira.application.model.entities.Regra;
import com.verifik.carteira.application.repository.custom.SearchRepositoryCustom;

@Repository
public interface RegraRepository extends CrudRepository<Regra, Long>, SearchRepositoryCustom<Regra> {

	Regra findByDescricaoIgnoreCase(String descricao);
	
	Regra findByChaveIgnoreCase(String chave);

	Optional<Regra> findById(Long id);

	List<Regra> findAll();

}
