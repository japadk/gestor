package com.verifik.carteira.application.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.model.entities.UsuarioCarteira;
import com.verifik.carteira.application.repository.custom.SearchRepositoryCustom;

@Repository
public interface UsuarioCarteiraRepository extends CrudRepository<UsuarioCarteira, Long>, SearchRepositoryCustom<UsuarioCarteira> {
	
	@Query("SELECT uc.carteira FROM UsuarioCarteira as uc INNER JOIN uc.usuario as u WHERE u.id = :id")
	List<Carteira> findCarteirasByIdUsuario(Long id);
	
	@Query("SELECT uc FROM UsuarioCarteira as uc INNER JOIN uc.carteira as c INNER JOIN uc.usuario as u WHERE c.id = :idCarteira AND u.id = :idUsuario")
	UsuarioCarteira findByIdCateiraAndIdUsuario(Long idCarteira, Long idUsuario);
	
	Optional<UsuarioCarteira> findById(Long id);
	
	List<UsuarioCarteira> findAll();

}
