package com.verifik.carteira.application.repository.custom;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface DividaRepositoryCustom {

	BigDecimal sumPagos(Long idCliente, List<Long> idCarteiras, LocalDate vencimentoInicio, LocalDate vencimentoFim,
			LocalDate pagamentoInicio, LocalDate pagamentoFim);

	BigDecimal sumPagosRecebidos(Long idCliente, List<Long> idCarteiras, LocalDate vencimentoInicio,
			LocalDate vencimentoFim, LocalDate pagamentoInicio, LocalDate pagamentoFim);

	BigDecimal sumNaoPagos(Long idCliente, List<Long> idCarteiras, LocalDate vencimentoInicio,
			LocalDate vencimentoFim, LocalDate pagamentoInicio, LocalDate pagamentoFim);

	BigDecimal sumValor(Long idCliente, List<Long> idCarteiras, LocalDate vencimentoInicio, LocalDate vencimentoFim,
			LocalDate pagamentoInicio, LocalDate pagamentoFim);

}
