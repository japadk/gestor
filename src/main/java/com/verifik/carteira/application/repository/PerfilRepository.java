package com.verifik.carteira.application.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.verifik.carteira.application.model.entities.Perfil;
import com.verifik.carteira.application.repository.custom.SearchRepositoryCustom;

@Repository
public interface PerfilRepository extends CrudRepository<Perfil, Long>, SearchRepositoryCustom<Perfil> {

	Perfil findByDescricaoIgnoreCase(String descricao);
	
	Perfil findBySiglaIgnoreCase(String sigla);

	Optional<Perfil> findById(Long id);

	List<Perfil> findAll();

}
