package com.verifik.carteira.application.repository.custom.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.verifik.carteira.application.model.entities.Cliente;
import com.verifik.carteira.application.model.entities.Divida;
import com.verifik.carteira.application.repository.custom.DividaRepositoryCustom;

public class DividaRepositoryCustomImpl implements DividaRepositoryCustom{

	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public BigDecimal sumValor(Long idCliente, List<Long> idCarteiras, LocalDate vencimentoInicio,
			LocalDate vencimentoFim, LocalDate pagamentoInicio, LocalDate pagamentoFim) {
		
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<BigDecimal> criteria = builder.createQuery(BigDecimal.class);
        
        Root<Divida> root = criteria.from(Divida.class);
        
        List<Predicate> restrictions = buildPredicate(builder, root, idCliente,idCarteiras, vencimentoInicio,
    			vencimentoFim, pagamentoInicio, pagamentoFim);
        
        criteria
        	.select(builder.sum(root.get("valor")))
        	.where(restrictions.toArray(new Predicate[restrictions.size()]));
        
        
    	return entityManager.createQuery(criteria).getSingleResult();
	}

	@Override
	public BigDecimal sumPagos(Long idCliente, List<Long> idCarteiras, LocalDate vencimentoInicio,
			LocalDate vencimentoFim, LocalDate pagamentoInicio, LocalDate pagamentoFim) {
		
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<BigDecimal> criteria = builder.createQuery(BigDecimal.class);
        
        Root<Divida> root = criteria.from(Divida.class);
        
        List<Predicate> restrictions = buildPredicate(builder, root, idCliente,idCarteiras, vencimentoInicio,
    			vencimentoFim, pagamentoInicio, pagamentoFim);
        
        restrictions.add(builder.equal(root.get("pago"), true));
        
        criteria
        	.select(builder.sum(root.get("valor")))
        	.where(restrictions.toArray(new Predicate[restrictions.size()]));
        
        
    	return entityManager.createQuery(criteria).getSingleResult();
	}

	@Override
	public BigDecimal sumPagosRecebidos(Long idCliente, List<Long> idCarteiras, LocalDate vencimentoInicio,
			LocalDate vencimentoFim, LocalDate pagamentoInicio, LocalDate pagamentoFim) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<BigDecimal> criteria = builder.createQuery(BigDecimal.class);
        
        Root<Divida> root = criteria.from(Divida.class);
        
        List<Predicate> restrictions = buildPredicate(builder, root, idCliente,idCarteiras, vencimentoInicio,
    			vencimentoFim, pagamentoInicio, pagamentoFim);
        
        restrictions.add(builder.equal(root.get("pago"), true));
        
        criteria
        	.select(builder.sum(root.get("valorRecebido")))
        	.where(restrictions.toArray(new Predicate[restrictions.size()]));
        
        
    	return entityManager.createQuery(criteria).getSingleResult();
	}

	@Override
	public BigDecimal sumNaoPagos(Long idCliente, List<Long> idCarteiras, LocalDate vencimentoInicio,
			LocalDate vencimentoFim, LocalDate pagamentoInicio, LocalDate pagamentoFim) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<BigDecimal> criteria = builder.createQuery(BigDecimal.class);
        
        Root<Divida> root = criteria.from(Divida.class);
        
        List<Predicate> restrictions = buildPredicate(builder, root, idCliente, idCarteiras, vencimentoInicio,
    			vencimentoFim, pagamentoInicio, pagamentoFim);
        
        restrictions.add(builder.equal(root.get("pago"), false));
        
        criteria
        	.select(builder.sum(root.get("valor")))
        	.where(restrictions.toArray(new Predicate[restrictions.size()]));
        
        
    	return entityManager.createQuery(criteria).getSingleResult();
	}
	
	private List<Predicate> buildPredicate(CriteriaBuilder builder, Root root, Long idCliente, List<Long> idCarteiras, LocalDate vencimentoInicio,
			LocalDate vencimentoFim, LocalDate pagamentoInicio, LocalDate pagamentoFim) {
		
				
		List<Predicate> restrictions = new LinkedList<>();
		
		if(idCliente != null) {
			restrictions.add(builder.equal(root.join("cliente").get("id"), idCliente));
		}
		
		if(idCarteiras != null && !idCarteiras.isEmpty()) {
			Join<Divida, Cliente> clienteJoin = root.join("cliente");
			restrictions.add(clienteJoin.get("carteira").in(idCarteiras));
		}
		
		if(vencimentoInicio != null && vencimentoFim != null) {
			restrictions.add(builder.between(root.get("dataVencimento"), vencimentoInicio, vencimentoFim));
		}
		
		if(pagamentoInicio != null && pagamentoFim != null) {
			restrictions.add(builder.between(root.get("dataPagamento"), pagamentoInicio, pagamentoFim));
		}
		
		//garante que só dividas de clientes com carteira ativa sejam contados
		restrictions.add(builder.equal(root.join("cliente").join("carteira").get("registroAtivo"), true));
		//garante que só dividas de clientes ativos sejam contados
		restrictions.add(builder.equal(root.join("cliente").get("registroAtivo"), true));
		
		return restrictions;
		
	}

}
