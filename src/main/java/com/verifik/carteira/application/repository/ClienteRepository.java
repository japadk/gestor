package com.verifik.carteira.application.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.verifik.carteira.application.model.entities.Cliente;
import com.verifik.carteira.application.repository.custom.SearchRepositoryCustom;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Long>, SearchRepositoryCustom<Cliente> {

	Cliente findByNome(String nome);
	
	Cliente findByCodigo(String codigo);

	Optional<Cliente> findById(Long id);

	List<Cliente> findAll();
	
	Cliente findByCnpj(String cnpj);

	@Query("SELECT c FROM Cliente c INNER JOIN c.carteira ca WHERE c.registroAtivo = true AND ca.registroAtivo = true ORDER BY c.codigo ASC")
	List<Cliente> findAllActive();

}
