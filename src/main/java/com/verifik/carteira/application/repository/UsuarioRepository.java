package com.verifik.carteira.application.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.repository.custom.SearchRepositoryCustom;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long>, SearchRepositoryCustom<Usuario> {

	Usuario findByNome(String nome);

	Optional<Usuario> findById(Long id);

	Usuario findByLoginIgnoreCase(String login);

	List<Usuario> findAll();

}
