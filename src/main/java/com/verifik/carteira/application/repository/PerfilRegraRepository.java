package com.verifik.carteira.application.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.verifik.carteira.application.model.entities.Perfil;
import com.verifik.carteira.application.model.entities.PerfilRegra;
import com.verifik.carteira.application.model.entities.Regra;
import com.verifik.carteira.application.repository.custom.SearchRepositoryCustom;

@Repository
public interface PerfilRegraRepository extends CrudRepository<PerfilRegra, Long>, SearchRepositoryCustom<PerfilRegra> {

	@Query("SELECT pr FROM PerfilRegra as pr INNER JOIN pr.perfil as p INNER JOIN pr.regra as r WHERE p.sigla = :sigla AND r.chave = :chave")
	PerfilRegra findByPerfilSiglaAndRegraChave(String sigla, String chave);
	
	@Query("SELECT pr FROM PerfilRegra as pr INNER JOIN pr.perfil as p WHERE p.sigla = :sigla")
	List<PerfilRegra> findByPerfilSigla(String sigla);
	
	@Query("SELECT pr FROM PerfilRegra as pr INNER JOIN pr.regra as r WHERE r.chave = :chave")
	List<PerfilRegra> findByRegraChave(String chave);
	
	@Query("SELECT pr.regra FROM PerfilRegra as pr INNER JOIN pr.perfil as p WHERE p.sigla = :sigla")
	List<Regra> findRegrasByPerfilSigla(String sigla);
	
	@Query("SELECT pr.perfil FROM PerfilRegra as pr INNER JOIN pr.regra as r WHERE r.chave = :chave")
	List<Perfil> findPerfisByRegraChave(String chave);
	
	Optional<PerfilRegra> findById(Long id);

	List<PerfilRegra> findAll();

}
