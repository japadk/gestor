package com.verifik.carteira.application.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.verifik.carteira.application.model.entities.Atividade;
import com.verifik.carteira.application.repository.custom.SearchRepositoryCustom;

@Repository
public interface AtividadeRepository extends CrudRepository<Atividade, Long>, SearchRepositoryCustom<Atividade> {

	Optional<Atividade> findById(Long id);
	
	Atividade findByEntidade(String entidade);

	List<Atividade> findAll();
	
	@Query("SELECT a FROM Atividade as a INNER JOIN a.usuario as u WHERE a.dataCriacao BETWEEN :dataCriacaoFrom and :dataCriacaoTo ORDER BY a.dataCriacao DESC")
	List<Atividade> findBy(LocalDateTime dataCriacaoFrom, LocalDateTime dataCriacaoTo);
	
	@Query("SELECT a FROM Atividade as a INNER JOIN a.usuario as u WHERE a.entidade = :entidade AND a.dataCriacao BETWEEN :dataCriacaoFrom and :dataCriacaoTo ORDER BY a.dataCriacao DESC")
	List<Atividade> findBy(String entidade, LocalDateTime dataCriacaoFrom, LocalDateTime dataCriacaoTo);
	
	@Query("SELECT a FROM Atividade as a INNER JOIN a.usuario as u WHERE a.dataCriacao BETWEEN :dataCriacaoFrom and :dataCriacaoTo AND u.id = :idUsuario ORDER BY a.dataCriacao DESC")
	List<Atividade> findBy(Long idUsuario, LocalDateTime dataCriacaoFrom, LocalDateTime dataCriacaoTo);
	
	@Query("SELECT a FROM Atividade as a INNER JOIN a.usuario as u WHERE a.entidade = :entidade AND a.dataCriacao BETWEEN :dataCriacaoFrom and :dataCriacaoTo AND u.id = :idUsuario ORDER BY a.dataCriacao DESC")
	List<Atividade> findBy(String entidade, Long idUsuario, LocalDateTime dataCriacaoFrom, LocalDateTime dataCriacaoTo);
	

}
