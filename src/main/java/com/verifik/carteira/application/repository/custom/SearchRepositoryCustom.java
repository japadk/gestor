package com.verifik.carteira.application.repository.custom;

import java.util.List;

import com.verifik.carteira.application.model.base.search.BaseSearch;

public interface SearchRepositoryCustom<T> {
	
	List<T> findBySearch(BaseSearch searchDTO);	
	
	long countBySearch(BaseSearch searchDTO);	
	
	void refresh(T t);

}
