package com.verifik.carteira.application.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.verifik.carteira.application.model.entities.LigacaoRetorno;
import com.verifik.carteira.application.repository.custom.SearchRepositoryCustom;

@Repository
public interface LigacaoRetornoRepository extends CrudRepository<LigacaoRetorno, Long>, SearchRepositoryCustom<LigacaoRetorno> {

	Optional<LigacaoRetorno> findById(Long id);

	List<LigacaoRetorno> findAll();
	
	@Query("SELECT lr FROM LigacaoRetorno  AS lr JOIN lr.ligacao AS l WHERE l.id = :idLigacao")
	Optional<LigacaoRetorno> findByLigacao(Long idLigacao);
}
