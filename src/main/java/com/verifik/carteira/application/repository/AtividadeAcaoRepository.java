package com.verifik.carteira.application.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.verifik.carteira.application.model.entities.AtividadeAcao;
import com.verifik.carteira.application.repository.custom.SearchRepositoryCustom;

@Repository
public interface AtividadeAcaoRepository extends CrudRepository<AtividadeAcao, Long>, SearchRepositoryCustom<AtividadeAcao> {

	Optional<AtividadeAcao> findById(Long id);
	
	List<AtividadeAcao> findAll();
	
}
