package com.verifik.carteira.application.repository.custom.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.repository.custom.SearchRepositoryCustom;
import com.verifik.carteira.application.utils.SearchUtils;

public class SearchRepositoryCustomImpl<T> implements SearchRepositoryCustom<T>{

	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public List<T> findBySearch(BaseSearch searchDTO) {
		
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<T> criteria = builder.createQuery(searchDTO.getEntityClass());
        
        Root<T> root = criteria.from(searchDTO.getEntityClass());
        
        List<Predicate> restrictions = SearchUtils.buildPredicateList(builder, root, searchDTO);
        
        Predicate p = null;
        if(!restrictions.isEmpty()) {
	        //Adiciona os where
	        if(searchDTO.isOr()) {
	        	p = builder.or(restrictions.toArray(new Predicate[restrictions.size()]));
	        } else {
	        	p = builder.and(restrictions.toArray(new Predicate[restrictions.size()]));
	        }
        }
        
        if(searchDTO.getRegistroAtivo() != null) {
        	if(p != null) {
        		p = builder.and(builder.equal(root.get("registroAtivo"), searchDTO.getRegistroAtivo()), p);
        	} else {
        		p = builder.and(builder.equal(root.get("registroAtivo"), searchDTO.getRegistroAtivo()));
        	}
        }
        
        criteria.where(searchDTO.getCustomPredicate(builder, root, Optional.ofNullable(p)));

        //criteria.select(root).distinct(true);
        
        //Adiciona orderby
        List<Expression> expJoinOrderByList = new ArrayList<>();
        if(!searchDTO.getCustomOrderBy(builder, root).isEmpty()) {
        	criteria.orderBy(searchDTO.getCustomOrderBy(builder, root));
        } else if(searchDTO.getOrderBy() != null) {
        	
        	if(searchDTO.getDesc() == null || !searchDTO.getDesc()) {
        		criteria.orderBy(builder.asc(root.get(searchDTO.getOrderBy())));
        		
        	}else {
        		criteria.orderBy(builder.desc(root.get(searchDTO.getOrderBy())));
        	}
        	
        } else if (searchDTO.getOrderByList() != null && !searchDTO.getOrderByList().isEmpty()){
        	
        	List<Order> orderByList = new ArrayList<>(searchDTO.getOrderByList().size());
        	for(String ob : searchDTO.getOrderByList()) {
        		String[] split = ob.split(":");
        		
        		Expression exp = SearchUtils.getJoinExpression(split[0], root);
        		expJoinOrderByList.add(exp);
        		
        		if(split.length == 0 || split[1].trim().equalsIgnoreCase("asc")) {
        			orderByList.add(builder.asc(exp));
        		} else {
        			orderByList.add(builder.desc(exp));
        		}
        	}
        	
        	criteria.orderBy(orderByList);
        	
        }
        
        
        expJoinOrderByList.add(0, root.get("id"));
        criteria.groupBy(expJoinOrderByList.toArray(new Expression[expJoinOrderByList.size()]));
        
        if(searchDTO.getLimit() <= 0) {
        	return entityManager.createQuery(criteria).getResultList();
        }

        return entityManager.createQuery(criteria).setMaxResults(searchDTO.getLimit()).setFirstResult(searchDTO.getOffset()).getResultList();
	}

	@Override
	public long countBySearch(BaseSearch searchDTO) {
		
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
        
        Root<T> root = criteria.from(searchDTO.getEntityClass());
        
        criteria.select(builder.count(root)).distinct(true);
        
        List<Predicate> restrictions = SearchUtils.buildPredicateList(builder, root, searchDTO);
        
        Predicate p = null;
        if(!restrictions.isEmpty()) {
	        //Adiciona os where
	        if(searchDTO.isOr()) {
	        	p = builder.or(restrictions.toArray(new Predicate[restrictions.size()]));
	        } else {
	        	p = builder.and(restrictions.toArray(new Predicate[restrictions.size()]));
	        }
        }
        
        if(searchDTO.getRegistroAtivo() != null) {
        	if(p != null) {
        		p = builder.and(builder.equal(root.get("registroAtivo"), searchDTO.getRegistroAtivo()), p);        		
        	} else {
        		p = builder.and(builder.equal(root.get("registroAtivo"), searchDTO.getRegistroAtivo()));
        	}
        }
        
        criteria.where(searchDTO.getCustomPredicate(builder, root, Optional.ofNullable(p)));
        

        return entityManager.createQuery(criteria).getSingleResult();
	}

	@Override
	@Transactional
	public void refresh(T t) {
		entityManager.refresh(t);
	}

}
