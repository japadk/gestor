package com.verifik.carteira.application.model.search;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.format.annotation.DateTimeFormat;

import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.model.entities.Cliente;
import com.verifik.carteira.application.model.entities.Divida;
import com.verifik.carteira.application.model.entities.Ligacao;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.entities.UsuarioCarteira;

import io.swagger.annotations.ApiModelProperty;

public class LigacaoSearch extends BaseSearch {

	@ApiModelProperty(value = "Id do usuario")
	private Long idUsuario;

	@ApiModelProperty(value = "Se a ligacao tem retorno registrado")
	private Boolean comRetorno;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataDe;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataAte;

	@ApiModelProperty(value = "Nome do cliente")
	private String nome;

	@ApiModelProperty(value = "CNPJ do cliente")
	private String cnpj;

	@ApiModelProperty(value = "Telefone do cliente")
	private String telefone;

	@Override
	@ApiModelProperty(hidden = true)
	public Class getEntityClass() {
		return Ligacao.class;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Boolean getComRetorno() {
		return comRetorno;
	}

	public void setComRetorno(Boolean comRetorno) {
		this.comRetorno = comRetorno;
	}

	public LocalDate getDataDe() {
		return dataDe;
	}

	public void setDataDe(LocalDate dataDe) {
		this.dataDe = dataDe;
	}

	public LocalDate getDataAte() {
		return dataAte;
	}

	public void setDataAte(LocalDate dataAte) {
		this.dataAte = dataAte;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	@ApiModelProperty(hidden = true)
	public List<String> getIgnoredFields() {
		return Arrays.asList(new String[] { "idUsuario" , "dataDe", "dataAte" });
	}
	
	@ApiModelProperty(hidden = true)
	public Predicate getCustomPredicate(CriteriaBuilder builder, Root root, Optional<Predicate> previous) {

		List<Predicate> restrictions = new ArrayList<>(0);

		if (idUsuario != null) {

			restrictions.add(builder.equal(root.get("usuario"), idUsuario));

		}
		
		if(dataDe != null && dataAte != null) {
			restrictions.add(builder.between(root.get("dataCriacao"),  dataDe.atTime(0, 0), dataAte.atTime(23,59)));
		}
		
		Predicate and = builder.and(restrictions.toArray(new Predicate[restrictions.size()]));
		return !previous.isPresent() ? and : builder.and(and, previous.get());
	}

}
