package com.verifik.carteira.application.model.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.entities.UsuarioCarteira;

import io.swagger.annotations.ApiModelProperty;

public class CarteiraSearch extends BaseSearch {

	@ApiModelProperty(value = "Nome da carteira")
	private String nome;
	
	@ApiModelProperty(hidden = true)
	private Long idUsuarioVinculado;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getIdUsuarioVinculado() {
		return idUsuarioVinculado;
	}

	public void setIdUsuarioVinculado(Long idUsuarioVinculado) {
		this.idUsuarioVinculado = idUsuarioVinculado;
	}

	@Override
	@ApiModelProperty(hidden = true)
	public Class getEntityClass() {
		return Carteira.class;
	}
	
	@ApiModelProperty(hidden = true)
	public List<String> getIgnoredFields() {
		return Arrays.asList(new String[] { "idUsuarioVinculado" });
	}
	
	@ApiModelProperty(hidden = true)
	public Predicate getCustomPredicate(CriteriaBuilder builder, Root root, Optional<Predicate> previous) {

		List<Predicate> restrictions = new ArrayList<>(0);

		if (idUsuarioVinculado != null) {

			Join<Carteira, UsuarioCarteira> usuarioCarteiraJoin = root.join("usuarioCarteiraList");
			Join<UsuarioCarteira, Usuario> usuarioJoin = usuarioCarteiraJoin.join("usuario");
			restrictions.add(builder.equal(usuarioJoin.get("id"), idUsuarioVinculado));

		}
		
		Predicate and = builder.and(restrictions.toArray(new Predicate[restrictions.size()]));
		return previous.isPresent() ? builder.and(and, previous.get()) : and;
	}

}
