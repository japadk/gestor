package com.verifik.carteira.application.model.security;

import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.verifik.carteira.application.model.entities.Usuario;

public class CustomUserDetails extends User {

	private Usuario usuario;
	private List<GrantedAuthority> authorities;
	private Set<String> regras;
	private String perfilSigla;

	public CustomUserDetails(Usuario user, String perfilSigla, List<GrantedAuthority> authorities, Set<String> regras) {

		super(user.getLogin(), user.getSenha(), authorities);
		
		this.usuario = user;
		this.perfilSigla = perfilSigla;
		this.authorities = authorities;
		this.regras = regras;
		
	}

	public Usuario getUsuario() {
		return usuario;
	}
	
	public boolean containsRegra(String regra) {
		return regras.contains(regra);
	}
	
	public String getPerfilSigla() {
		return perfilSigla;
	}

}
