package com.verifik.carteira.application.model.dto;

import java.util.List;

import javax.annotation.Generated;

import io.swagger.annotations.ApiModelProperty;
import java.util.Collections;

public class UsuarioDTO {

	@ApiModelProperty(value = "Id do usuário", hidden = true)
	private Long id;

	@ApiModelProperty(value = "Nome do usuário")
	private String nome;

	@ApiModelProperty(value = "Login do usuário")
	private String login;

	@ApiModelProperty(value = "Senha do usuário")
	private String senha;

	@ApiModelProperty(value = "Se o registro está ativo")
	private Boolean registroAtivo;

	@ApiModelProperty(value = "Sigla do perfil")
	private String siglaPerfil;
	
	private List<CarteiraDTO> carteiraList;

	@Generated("SparkTools")
	private UsuarioDTO(Builder builder) {
		this.id = builder.id;
		this.nome = builder.nome;
		this.login = builder.login;
		this.senha = builder.senha;
		this.registroAtivo = builder.registroAtivo;
		this.siglaPerfil = builder.siglaPerfil;
		this.carteiraList = builder.carteiraList;
	}

	public UsuarioDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Boolean getRegistroAtivo() {
		return registroAtivo;
	}

	public void setRegistroAtivo(Boolean registroAtivo) {
		this.registroAtivo = registroAtivo;
	}

	public String getSiglaPerfil() {
		return siglaPerfil;
	}

	public void setSiglaPerfil(String siglaPerfil) {
		this.siglaPerfil = siglaPerfil;
	}

	public List<CarteiraDTO> getCarteiraList() {
		return carteiraList;
	}

	public void setCarteiraList(List<CarteiraDTO> carteiraList) {
		this.carteiraList = carteiraList;
	}

	/**
	 * Creates builder to build {@link UsuarioDTO}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link UsuarioDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private Long id;
		private String nome;
		private String login;
		private String senha;
		private Boolean registroAtivo;
		private String siglaPerfil;
		private List<CarteiraDTO> carteiraList = Collections.emptyList();

		private Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withNome(String nome) {
			this.nome = nome;
			return this;
		}

		public Builder withLogin(String login) {
			this.login = login;
			return this;
		}

		public Builder withSenha(String senha) {
			this.senha = senha;
			return this;
		}

		public Builder withRegistroAtivo(Boolean registroAtivo) {
			this.registroAtivo = registroAtivo;
			return this;
		}

		public Builder withSiglaPerfil(String siglaPerfil) {
			this.siglaPerfil = siglaPerfil;
			return this;
		}

		public Builder withCarteiraList(List<CarteiraDTO> carteiraList) {
			this.carteiraList = carteiraList;
			return this;
		}

		public UsuarioDTO build() {
			return new UsuarioDTO(this);
		}
	}

}
