package com.verifik.carteira.application.model.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.model.entities.Cliente;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.entities.UsuarioCarteira;

import io.swagger.annotations.ApiModelProperty;

public class ClienteSearch extends BaseSearch {

	@ApiModelProperty(value = "Nome do cliente")
	private String nome;
	
	@ApiModelProperty(value = "Nome fantaasia do cliente")
	private String nomeFantasia;

	@ApiModelProperty(value = "CNPJ do cliente")
	private String cnpj;
	
	@ApiModelProperty(value = "Telefone do cliente")
	private String telefone;
	
	@ApiModelProperty(value = "Email do cliente")
	private String email;
	
	@ApiModelProperty(value = "Codigo do cliente")
	private String codigo;
	
	@ApiModelProperty(value = "Id da carteira de clientes")
	private Long idCarteira;
	
	@ApiModelProperty(hidden = true)
	private Long idUsuarioVinculado;
	
	public Long getIdUsuarioVinculado() {
		return idUsuarioVinculado;
	}

	public void setIdUsuarioVinculado(Long idUsuarioVinculado) {
		this.idUsuarioVinculado = idUsuarioVinculado;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getIdCarteira() {
		return idCarteira;
	}

	public void setIdCarteira(Long idCarteira) {
		this.idCarteira = idCarteira;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Override
	@ApiModelProperty(hidden = true)
	public Class getEntityClass() {
		return Cliente.class;
	}
	
	@ApiModelProperty(hidden = true)
	public List<String> getIgnoredFields() {

		return Arrays.asList(new String[] { "idCarteira" , "idUsuarioVinculado"});
	}
	
	@ApiModelProperty(hidden = true)
	public Predicate getCustomPredicate(CriteriaBuilder builder, Root root, Optional<Predicate> previous) {

		List<Predicate> restrictions = new ArrayList<>(0);

		if (idCarteira != null) {

			restrictions.add(builder.equal(root.get("carteira"), idCarteira));

		}
		
		if (idUsuarioVinculado != null) {

			Join<Cliente, Carteira> carteiraJoin = root.join("carteira");
			Join<Carteira, UsuarioCarteira> usuarioCarteiraJoin = carteiraJoin.join("usuarioCarteiraList");
			Join<UsuarioCarteira, Usuario> usuarioJoin = usuarioCarteiraJoin.join("usuario");
			restrictions.add(builder.equal(usuarioJoin.get("id"), idUsuarioVinculado));

		}
		
		Predicate and = builder.and(restrictions.toArray(new Predicate[restrictions.size()]));
		return previous.isPresent() ? builder.and(and, previous.get()) : and;
	}

	
}
