package com.verifik.carteira.application.model.dto;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import org.apache.commons.collections4.comparators.FixedOrderComparator;

import java.io.Writer;

public class RelatorioClienteCsvDTO {

  @CsvBindByName(column = "Codigo")
  String codigo;

  @CsvBindByName(column = "Cliente")
  String cliente;

  @CsvBindByName(column = "CNPJ")
  String cnpj;

  @CsvBindByName(column = "Contato")
  String contato;

  @CsvBindByName(column = "Telefone")
  String telefone;

  @CsvBindByName(column = "Email")
  String email;

  @CsvBindByName(column = "Carteira")
  String carteira;

  public String getCodigo() {
    return codigo;
  }

  public void setCodigo(String codigo) {
    this.codigo = codigo;
  }

  public String getCliente() {
    return cliente;
  }

  public void setCliente(String cliente) {
    this.cliente = cliente;
  }

  public String getCarteira() {
    return carteira;
  }

  public void setCarteira(String carteira) {
    this.carteira = carteira;
  }

  public String getCnpj() {
    return cnpj;
  }

  public void setCnpj(String cnpj) {
    this.cnpj = cnpj;
  }

  public String getContato() {
    return contato;
  }

  public void setContato(String contato) {
    this.contato = contato;
  }

  public String getTelefone() {
    return telefone;
  }

  public void setTelefone(String telefone) {
    this.telefone = telefone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public static StatefulBeanToCsv<RelatorioClienteCsvDTO> getBeanToCsvWriter(Writer writer) {
    HeaderColumnNameMappingStrategy<RelatorioClienteCsvDTO> strategy = new HeaderColumnNameMappingStrategy<>();
    strategy.setType(RelatorioClienteCsvDTO.class);

    String[] campos = { "CODIGO", "CLIENTE", "CNPJ", "CONTATO", "TELEFONE", "EMAIL", "CARTEIRA" };
    FixedOrderComparator<String> order = new FixedOrderComparator<>(campos);
    strategy.setColumnOrderOnWrite(order);

    return new StatefulBeanToCsvBuilder<RelatorioClienteCsvDTO>(writer).withMappingStrategy(strategy)
        .withSeparator(';').build();
  }
}
