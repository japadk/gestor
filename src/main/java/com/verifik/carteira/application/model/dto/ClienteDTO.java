package com.verifik.carteira.application.model.dto;

import javax.annotation.Generated;

import io.swagger.annotations.ApiModelProperty;

public class ClienteDTO {

	@ApiModelProperty(value = "Id do cliente", hidden = true)
	private Long id;

	@ApiModelProperty(value = "Nome do cliente")
	private String nome;

	@ApiModelProperty(value = "Nome fantasia do cliente")
	private String nomeFantasia;

	@ApiModelProperty(value = "CNPJ do cliente")
	private String cnpj;
	
	@ApiModelProperty(value = "Código do cliente")
	private String codigo;
	
	@ApiModelProperty(value = "Node do contato do cliente")
	private String contato;
	
	@ApiModelProperty(value = "Telefone do cliente")
	private String telefone;
	
	@ApiModelProperty(value = "Email do cliente")
	private String email;
	
	@ApiModelProperty(value = "Se o registyro está ativo")
	private Boolean registroAtivo;
	
	@ApiModelProperty(value = "Id da carteira")
	private Long idCarteira;
	
	@ApiModelProperty(value = "Nome da carteira")
	private String nomeCarteira;

	@Generated("SparkTools")
	private ClienteDTO(Builder builder) {
		this.id = builder.id;
		this.nome = builder.nome;
		this.nomeFantasia = builder.nomeFantasia;
		this.cnpj = builder.cnpj;
		this.codigo = builder.codigo;
		this.contato = builder.contato;
		this.telefone = builder.telefone;
		this.email = builder.email;
		this.registroAtivo = builder.registroAtivo;
		this.idCarteira = builder.idCarteira;
		this.nomeCarteira = builder.nomeCarteira;
	}

	public ClienteDTO() {
		super();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Boolean getRegistroAtivo() {
		return registroAtivo;
	}

	public void setRegistroAtivo(Boolean registroAtivo) {
		this.registroAtivo = registroAtivo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Long getIdCarteira() {
		return idCarteira;
	}

	public void setIdCarteira(Long idCarteira) {
		this.idCarteira = idCarteira;
	}

	public String getNomeCarteira() {
		return nomeCarteira;
	}

	public void setNomeCarteira(String nomeCarteira) {
		this.nomeCarteira = nomeCarteira;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	/**
	 * Creates builder to build {@link ClienteDTO}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link ClienteDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private Long id;
		private String nome;
		private String nomeFantasia;
		private String cnpj;
		private String codigo;
		private String contato;
		private String telefone;
		private String email;
		private Boolean registroAtivo;
		private Long idCarteira;
		private String nomeCarteira;

		private Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withNome(String nome) {
			this.nome = nome;
			return this;
		}

		public Builder withNomeFantasia(String nomeFantasia) {
			this.nomeFantasia = nomeFantasia;
			return this;
		}

		public Builder withCnpj(String cnpj) {
			this.cnpj = cnpj;
			return this;
		}

		public Builder withCodigo(String codigo) {
			this.codigo = codigo;
			return this;
		}

		public Builder withContato(String contato) {
			this.contato = contato;
			return this;
		}

		public Builder withTelefone(String telefone) {
			this.telefone = telefone;
			return this;
		}

		public Builder withEmail(String email) {
			this.email = email;
			return this;
		}

		public Builder withRegistroAtivo(Boolean registroAtivo) {
			this.registroAtivo = registroAtivo;
			return this;
		}

		public Builder withIdCarteira(Long idCarteira) {
			this.idCarteira = idCarteira;
			return this;
		}

		public Builder withNomeCarteira(String nomeCarteira) {
			this.nomeCarteira = nomeCarteira;
			return this;
		}

		public ClienteDTO build() {
			return new ClienteDTO(this);
		}
	}
	
}
