package com.verifik.carteira.application.model.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.verifik.carteira.application.model.base.entity.BaseEntity;

@Entity
public class Usuario extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usuario_sequence")
    @SequenceGenerator(name = "usuario_sequence", sequenceName = "usuario_sequence_pk", allocationSize = 1)
	protected Long id;

	@NotNull(message = "Nome não pode ser nulo")
	@Column(nullable = false, length = 255)
	private String nome;

	@NotNull(message = "Login não pode ser nulo")
	@Column(nullable = false, length = 255, unique = true)
	private String login;

	@JsonIgnore
	@NotNull(message = "Senha não pode ser nulo")
	@Column(nullable = false, length = 255)
	private String senha;

	@NotNull(message = "Perfil não pode ser nulo")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(referencedColumnName = "id", name = "idPerfil", nullable = false)
	private Perfil perfil;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
	private List<UsuarioCarteira> usuarioCarteiraList;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable( name = "usuariocarteira", 
		joinColumns = @JoinColumn(name = "idUsuario", referencedColumnName = "id"), 
		inverseJoinColumns = @JoinColumn(name = "idCarteira", referencedColumnName = "id"))
	private List<Carteira> carteiraList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public List<UsuarioCarteira> getUsuarioCarteiraList() {
		return usuarioCarteiraList;
	}

	public void setUsuarioCarteiraList(List<UsuarioCarteira> usuarioCarteiraList) {
		this.usuarioCarteiraList = usuarioCarteiraList;
	}

	public List<Carteira> getCarteiraList() {
		return carteiraList;
	}

	public void setCarteiraList(List<Carteira> carteiraList) {
		this.carteiraList = carteiraList;
	}

}
