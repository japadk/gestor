package com.verifik.carteira.application.model.dto;

import javax.annotation.Generated;

import io.swagger.annotations.ApiModelProperty;

public class PerfilRegraDTO {

	@ApiModelProperty(value = "Id do perfil", hidden = true)
	private Long id;

	@ApiModelProperty(value = "Perfil")
	private PerfilDTO perfil;

	@ApiModelProperty(value = "Regra")
	private RegraDTO regra;

	@ApiModelProperty(value = "Se o registro está ativo")
	private Boolean registroAtivo;

	@Generated("SparkTools")
	private PerfilRegraDTO(Builder builder) {
		this.id = builder.id;
		this.perfil = builder.perfil;
		this.regra = builder.regra;
		this.registroAtivo = builder.registroAtivo;
	}

	public PerfilRegraDTO() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PerfilDTO getPerfil() {
		return perfil;
	}

	public void setPerfil(PerfilDTO perfil) {
		this.perfil = perfil;
	}

	public RegraDTO getRegra() {
		return regra;
	}

	public void setRegra(RegraDTO regra) {
		this.regra = regra;
	}

	public Boolean getRegistroAtivo() {
		return registroAtivo;
	}

	public void setRegistroAtivo(Boolean registroAtivo) {
		this.registroAtivo = registroAtivo;
	}

	/**
	 * Creates builder to build {@link PerfilRegraDTO}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link PerfilRegraDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private Long id;
		private PerfilDTO perfil;
		private RegraDTO regra;
		private Boolean registroAtivo;

		private Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withPerfil(PerfilDTO perfil) {
			this.perfil = perfil;
			return this;
		}

		public Builder withRegra(RegraDTO regra) {
			this.regra = regra;
			return this;
		}

		public Builder withRegistroAtivo(Boolean registroAtivo) {
			this.registroAtivo = registroAtivo;
			return this;
		}

		public PerfilRegraDTO build() {
			return new PerfilRegraDTO(this);
		}
	}

	
}
