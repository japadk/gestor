package com.verifik.carteira.application.model.enums;

public enum ERegra {
	
	
	// -------------------------------------------
	// USUARIO
	// -------------------------------------------
	// CRIAR 
	CREATE_USUARIO_ADMIN("Permite criar usuário", CHAVE.CREATE_USUARIO),
	
	// ATUALIZAR
	UPDATE_USUARIO("Permite atualizar usuário", CHAVE.UPDATE_USUARIO),

	// DELETAR 
	DELETE_USUARIO("Permite deletar usuário", CHAVE.DELETE_USUARIO),
	
	// BUSCAR
	SEARCH_USUARIO("Permite buscar usuário via search", CHAVE.SEARCH_USUARIO),
	
	// VISUALIZAR
	VIEW_USUARIO("Permite visualizar usuário", CHAVE.VIEW_USUARIO),
	
	// -------------------------------------------
	// CARTEIRA
	// -------------------------------------------
	// CRIAR 
	CREATE_CARTEIRA("Permite criar carteira", CHAVE.CREATE_CARTEIRA),
	
	// ATUALIZAR
	UPDATE_CARTEIRA("Permite atualizar carteira", CHAVE.UPDATE_CARTEIRA),

	// DELETAR 
	DELETE_CARTEIRA("Permite deletar carteira", CHAVE.DELETE_CARTEIRA),
	
	// BUSCAR
	SEARCH_CARTEIRA("Permite buscar carteira via search", CHAVE.SEARCH_CARTEIRA),
	
	// VISUALIZAR
	VIEW_CARTEIRA("Permite visualizar carteira", CHAVE.VIEW_CARTEIRA),
	
	// -------------------------------------------
	// CLIENTE
	// -------------------------------------------
	// CRIAR 
	CREATE_CLIENTE("Permite criar cliente", CHAVE.CREATE_CLIENTE),
	
	// ATUALIZAR
	UPDATE_CLIENTE("Permite atualizar cliente", CHAVE.UPDATE_CLIENTE),

	// DELETAR 
	DELETE_CLIENTE("Permite deletar cliente", CHAVE.DELETE_CLIENTE),
	
	// BUSCAR
	SEARCH_CLIENTE("Permite buscar cliente via search", CHAVE.SEARCH_CLIENTE),
	
	// VISUALIZAR
	VIEW_CLIENTE("Permite visualizar cliente", CHAVE.VIEW_CLIENTE),
	
	// -------------------------------------------
	// DIVIDA
	// -------------------------------------------
	// CRIAR 
	CREATE_DIVIDA("Permite criar divida", CHAVE.CREATE_DIVIDA),
	
	// ATUALIZAR
	UPDATE_DIVIDA("Permite atualizar divida", CHAVE.UPDATE_DIVIDA),

	// DELETAR 
	DELETE_DIVIDA("Permite deletar divida", CHAVE.DELETE_DIVIDA),
	
	// BUSCAR
	SEARCH_DIVIDA("Permite buscar divida via search", CHAVE.SEARCH_DIVIDA),
	
	// VISUALIZAR
	VIEW_DIVIDA("Permite visualizar divida", CHAVE.VIEW_DIVIDA),
	
	// -------------------------------------------
	// LIGACAO
	// -------------------------------------------
	// CRIAR 
	CREATE_LIGACAO("Permite criar ligacao", CHAVE.CREATE_LIGACAO),
	
	// ATUALIZAR
	UPDATE_LIGACAO("Permite atualizar ligacao", CHAVE.UPDATE_LIGACAO),

	// DELETAR 
	DELETE_LIGACAO("Permite deletar ligacao", CHAVE.DELETE_LIGACAO),
	
	// BUSCAR
	SEARCH_LIGACAO("Permite buscar ligacao via search", CHAVE.SEARCH_LIGACAO),
	
	// VISUALIZAR
	VIEW_LIGACAO("Permite visualizar ligacao", CHAVE.VIEW_LIGACAO),
	
	
	;
	
	
	
	
	
	ERegra(String descricao, String chave) {
		this.descricao = descricao;
		this.chave = chave;
	};
	
	private String descricao;
	private String chave;
	
	public String getDescricao() {
		return descricao;
	}

	public String getChave() {
		return chave;
	}
	
	public String toString() {
		return this.getChave();
	}
	
	public static class CHAVE {
		
		// -------------------------------------------
		// USUARIO
		// -------------------------------------------
		public static final String CREATE_USUARIO 				= "create.usuario";
		public static final String UPDATE_USUARIO 				= "update.usuario";
		public static final String DELETE_USUARIO 				= "delete.usuario";
		public static final String SEARCH_USUARIO 				= "search.usuario";
		public static final String VIEW_USUARIO 				= "view.usuario";
		
		// -------------------------------------------
		// CARTEIRA
		// -------------------------------------------
		public static final String CREATE_CARTEIRA 				= "create.carteira";
		public static final String UPDATE_CARTEIRA 				= "update.carteira";
		public static final String DELETE_CARTEIRA 				= "delete.carteira";
		public static final String SEARCH_CARTEIRA 				= "search.carteira";
		public static final String VIEW_CARTEIRA 				= "view.carteira";
		
		// -------------------------------------------
		// CLIENTE
		// -------------------------------------------
		public static final String CREATE_CLIENTE 				= "create.cliente";
		public static final String UPDATE_CLIENTE 				= "update.cliente";
		public static final String DELETE_CLIENTE 				= "delete.cliente";
		public static final String SEARCH_CLIENTE 				= "search.cliente";
		public static final String VIEW_CLIENTE 				= "view.cliente";
		
		// -------------------------------------------
		// DIVIDA
		// -------------------------------------------
		public static final String CREATE_DIVIDA 				= "create.divida";
		public static final String UPDATE_DIVIDA 				= "update.divida";
		public static final String DELETE_DIVIDA 				= "delete.divida";
		public static final String SEARCH_DIVIDA 				= "search.divida";
		public static final String VIEW_DIVIDA 					= "view.divida";
		
		// -------------------------------------------
		// Ligacao
		// -------------------------------------------
		public static final String CREATE_LIGACAO 				= "create.ligacao";
		public static final String UPDATE_LIGACAO 				= "update.ligacao";
		public static final String DELETE_LIGACAO 				= "delete.ligacao";
		public static final String SEARCH_LIGACAO 				= "search.ligacao";
		public static final String VIEW_LIGACAO 				= "view.ligacao";
		
		// -------------------------------------------
		// Ligacao Retorno
		// -------------------------------------------
		public static final String CREATE_LIGACAO_RETORNO		= "create.ligacao.retorno";
		public static final String UPDATE_LIGACAO_RETORNO		= "update.ligacao.retorno";
		public static final String DELETE_LIGACAO_RETORNO		= "delete.ligacao.retorno";
		public static final String SEARCH_LIGACAO_RETORNO		= "search.ligacao.retorno";
		public static final String VIEW_LIGACAO_RETORNO 		= "view.ligacao.retorno";
		
	}
	
	

}
