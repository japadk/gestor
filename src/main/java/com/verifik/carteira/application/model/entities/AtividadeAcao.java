package com.verifik.carteira.application.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.verifik.carteira.application.model.base.entity.BaseEntity;

@Entity
public class AtividadeAcao extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "atividade_acao_sequence")
    @SequenceGenerator(name = "atividade_acao_sequence", sequenceName = "atividade_acao_sequence_pk", allocationSize = 1)
	protected Long id;

	@Column(length = 4000)
	private String valorAnterior;
	
	@Column(length = 4000)
	private String valorNovo;
		
	@NotNull(message = "Campo não pode ser nulo")
	@Column(nullable = false, length = 50)
	private String campo;
	
	@NotNull(message = "Atividade")
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(referencedColumnName = "id", name = "idAtividade", nullable = false)
	private Atividade atividade;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValorAnterior() {
		return valorAnterior;
	}

	public void setValorAnterior(String valorAnterior) {
		this.valorAnterior = valorAnterior;
	}

	public String getValorNovo() {
		return valorNovo;
	}

	public void setValorNovo(String valorNovo) {
		this.valorNovo = valorNovo;
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public Atividade getAtividade() {
		return atividade;
	}

	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}

}
