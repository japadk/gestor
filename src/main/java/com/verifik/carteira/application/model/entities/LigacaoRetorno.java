package com.verifik.carteira.application.model.entities;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.verifik.carteira.application.model.base.entity.BaseEntity;

@Entity
public class LigacaoRetorno extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ligacao_retorno_sequence")
	@SequenceGenerator(name = "ligacao_retorno_sequence", sequenceName = "ligacao_retorno_sequence_pk", allocationSize = 1)
	protected Long id;

	@NotNull(message = "Data retorno não pode ser nulo")
	@Column(nullable = false)
	private LocalDate dataRetorno;

	@NotNull(message = "Hora retorno não pode ser nulo")
	@Column(nullable = false)
	private LocalTime horaRetorno;

	@NotNull(message = "Usuario não pode ser nulo")
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(referencedColumnName = "id", name = "idUsuario", nullable = false)
	private Usuario usuario;

	@NotNull(message = "Ligação não pode ser nulo")
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(referencedColumnName = "id", name = "idLigacao", nullable = false)
	private Ligacao ligacao;
	
	private Boolean retornado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDataRetorno() {
		return dataRetorno;
	}

	public void setDataRetorno(LocalDate dataRetorno) {
		this.dataRetorno = dataRetorno;
	}

	public LocalTime getHoraRetorno() {
		return horaRetorno;
	}

	public void setHoraRetorno(LocalTime horaRetorno) {
		this.horaRetorno = horaRetorno;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Ligacao getLigacao() {
		return ligacao;
	}

	public void setLigacao(Ligacao ligacao) {
		this.ligacao = ligacao;
	}

	public Boolean getRetornado() {
		return retornado;
	}

	public void setRetornado(Boolean retornado) {
		this.retornado = retornado;
	}

}
