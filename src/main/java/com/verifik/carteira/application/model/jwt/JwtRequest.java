package com.verifik.carteira.application.model.jwt;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class JwtRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "Usuario")
	private String usuario;

	@ApiModelProperty(value = "Senha")
	private String senha;

	public JwtRequest() {
		
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	

}