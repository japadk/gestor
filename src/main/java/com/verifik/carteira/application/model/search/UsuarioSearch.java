package com.verifik.carteira.application.model.search;

import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.entities.Usuario;

import io.swagger.annotations.ApiModelProperty;

public class UsuarioSearch extends BaseSearch {

	@ApiModelProperty(value = "Nome do usuário")
	private String nome;

	@ApiModelProperty(value = "Login do usuário")
	private String login;

	@ApiModelProperty(value = "E-mail do usuário")
	private String email;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	@ApiModelProperty(hidden = true)
	public Class getEntityClass() {
		return Usuario.class;
	}

}
