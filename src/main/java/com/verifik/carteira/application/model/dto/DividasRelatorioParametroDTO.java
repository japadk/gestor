package com.verifik.carteira.application.model.dto;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModelProperty;

public class DividasRelatorioParametroDTO {

	@ApiModelProperty(value = "Data inicio")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate inicio;

	@ApiModelProperty(value = "Data fim")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate fim;

	public LocalDate getInicio() {
		return inicio;
	}

	public void setInicio(LocalDate inicio) {
		this.inicio = inicio;
	}

	public LocalDate getFim() {
		return fim;
	}

	public void setFim(LocalDate fim) {
		this.fim = fim;
	}

}
