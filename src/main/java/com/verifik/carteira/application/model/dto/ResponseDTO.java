package com.verifik.carteira.application.model.dto;

import javax.annotation.Generated;

public class ResponseDTO {

	String message;
	Object data;
	boolean error = false;

	public  ResponseDTO() {};
	
	@Generated("SparkTools")
	private ResponseDTO(Builder builder) {
		this.message = builder.message;
		this.data = builder.data;
		this.error = builder.error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	/**
	 * Creates builder to build {@link ResponseDTO}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link ResponseDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private String message;
		private Object data;
		private boolean error;

		private Builder() {
		}

		public Builder withMessage(String message) {
			this.message = message;
			return this;
		}

		public Builder withData(Object data) {
			this.data = data;
			return this;
		}

		public Builder withError(boolean error) {
			this.error = error;
			return this;
		}

		public ResponseDTO build() {
			return new ResponseDTO(this);
		}
	}
	
	
	
}
