package com.verifik.carteira.application.model.dto;

import java.io.Writer;

import org.apache.commons.collections4.comparators.FixedOrderComparator;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

public class RelatorioAtividadeDividaCsvDTO {
	
	@CsvBindByName(column = "DATA HORA")
	String dataHora;
	
	@CsvBindByName(column = "ATENDENTE")
	String atendente;

	@CsvBindByName(column = "Codigo")
	String codigo;

	@CsvBindByName(column = "Cliente")
	String cliente;

	@CsvBindByName(column = "Valor")
	String valor;

	@CsvBindByName(column = "Valor recebido")
	String valorRecebido;

	@CsvBindByName(column = "Vencimento")
	String vencimento;

	@CsvBindByName(column = "Pagamento")
	String pagamento;

	@CsvBindByName(column = "Pago")
	String pago;

	@CsvBindByName(column = "Observacao")
	String observacao;

	public String getDataHora() {
		return dataHora;
	}

	public void setDataHora(String dataHora) {
		this.dataHora = dataHora;
	}

	public String getAtendente() {
		return atendente;
	}

	public void setAtendente(String atendente) {
		this.atendente = atendente;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getValorRecebido() {
		return valorRecebido;
	}

	public void setValorRecebido(String valorRecebido) {
		this.valorRecebido = valorRecebido;
	}

	public String getVencimento() {
		return vencimento;
	}

	public void setVencimento(String vencimento) {
		this.vencimento = vencimento;
	}

	public String getPagamento() {
		return pagamento;
	}

	public void setPagamento(String pagamento) {
		this.pagamento = pagamento;
	}

	public String getPago() {
		return pago;
	}

	public void setPago(String pago) {
		this.pago = pago;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public static StatefulBeanToCsv<RelatorioAtividadeDividaCsvDTO> getBeanToCsvWriter(Writer writer) {
		HeaderColumnNameMappingStrategy<RelatorioAtividadeDividaCsvDTO> strategy = new HeaderColumnNameMappingStrategy<>();
		strategy.setType(RelatorioAtividadeDividaCsvDTO.class);

		String[] campos = {"DATA HORA", "ATENDENTE",  "CODIGO", "CLIENTE", "VALOR" , "VALOR RECEBIDO" , "VENCIMENTO" , "PAGAMENTO" , "PAGO" ,  "OBSERVACAO" };
		FixedOrderComparator<String> order = new FixedOrderComparator<>(campos);
		strategy.setColumnOrderOnWrite(order);

		return new StatefulBeanToCsvBuilder<RelatorioAtividadeDividaCsvDTO>(writer).withMappingStrategy(strategy)
				.withSeparator(';').build();
	}
}
