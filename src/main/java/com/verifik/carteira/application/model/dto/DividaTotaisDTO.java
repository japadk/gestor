package com.verifik.carteira.application.model.dto;

import java.math.BigDecimal;

import javax.annotation.Generated;

public class DividaTotaisDTO {

	private BigDecimal recebidos;
	private BigDecimal devidos;
	private BigDecimal pagosOriginal;
	private BigDecimal valorFaturadoInicial;

	public DividaTotaisDTO() {
		super();
	}

	@Generated("SparkTools")
	private DividaTotaisDTO(Builder builder) {
		this.recebidos = builder.recebidos;
		this.devidos = builder.devidos;
		this.pagosOriginal = builder.pagosOriginal;
		this.valorFaturadoInicial = builder.valorFaturadoInicial;
	}

	public BigDecimal getRecebidos() {
		return recebidos;
	}

	public void setRecebidos(BigDecimal recebidos) {
		this.recebidos = recebidos;
	}

	public BigDecimal getDevidos() {
		return devidos;
	}

	public void setDevidos(BigDecimal devidos) {
		this.devidos = devidos;
	}

	public BigDecimal getPagosOriginal() {
		return pagosOriginal;
	}

	public void setPagosOriginal(BigDecimal pagosOriginal) {
		this.pagosOriginal = pagosOriginal;
	}

	public BigDecimal getValorFaturadoInicial() {
		return valorFaturadoInicial;
	}

	public void setValorFaturadoInicial(BigDecimal valorFaturadoInicial) {
		this.valorFaturadoInicial = valorFaturadoInicial;
	}

	/**
	 * Creates builder to build {@link DividaTotaisDTO}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link DividaTotaisDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private BigDecimal recebidos;
		private BigDecimal devidos;
		private BigDecimal pagosOriginal;
		private BigDecimal valorFaturadoInicial;

		private Builder() {
		}

		public Builder withRecebidos(BigDecimal recebidos) {
			this.recebidos = recebidos;
			return this;
		}

		public Builder withDevidos(BigDecimal devidos) {
			this.devidos = devidos;
			return this;
		}

		public Builder withPagosOriginal(BigDecimal pagosOriginal) {
			this.pagosOriginal = pagosOriginal;
			return this;
		}

		public Builder withValorFaturadoInicial(BigDecimal valorFaturtadoInicial) {
			this.valorFaturadoInicial = valorFaturtadoInicial;
			return this;
		}

		public DividaTotaisDTO build() {
			return new DividaTotaisDTO(this);
		}
	}

}
