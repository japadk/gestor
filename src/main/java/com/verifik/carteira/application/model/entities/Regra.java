package com.verifik.carteira.application.model.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.verifik.carteira.application.model.base.entity.BaseEntity;

@Entity
public class Regra extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "regra_sequence")
    @SequenceGenerator(name = "regra_sequence", sequenceName = "regra_sequence_pk", allocationSize = 1)
	protected Long id;

	@NotNull(message = "Descricao não pode ser nulo")
	@Column(nullable = false, length = 255)
	private String descricao;

	@NotNull(message = "Chave não pode ser nula")
	@Column(nullable = false, length = 255, unique = true)
	private String chave;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "regra")
	private List<PerfilRegra> perfilRegraList;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

}
