package com.verifik.carteira.application.model.search;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.format.annotation.DateTimeFormat;

import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.entities.Atividade;
import com.verifik.carteira.application.model.entities.Usuario;

import io.swagger.annotations.ApiModelProperty;

public class AtividadeSearch extends BaseSearch {

	@ApiModelProperty(value = "Entidade")
	private String entidade;

	@ApiModelProperty(value = "Data inicio")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate inicio;

	@ApiModelProperty(value = "Data fim")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate fim;

	@ApiModelProperty(value = "Usuário da atividade")
	private Long idUsuario;

	public String getEntidade() {
		return entidade;
	}

	public void setEntidade(String entidade) {
		this.entidade = entidade;
	}

	public LocalDate getInicio() {
		return inicio;
	}

	public void setInicio(LocalDate inicio) {
		this.inicio = inicio;
	}

	public LocalDate getFim() {
		return fim;
	}

	public void setFim(LocalDate fim) {
		this.fim = fim;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	@Override
	@ApiModelProperty(hidden = true)
	public Class getEntityClass() {
		return Atividade.class;
	}

	@ApiModelProperty(hidden = true)
	public List<String> getIgnoredFields() {

		return Arrays.asList(new String[] { "idUsuario" });
	}

	@Override
	public Predicate getCustomPredicate(CriteriaBuilder builder, Root root, Optional<Predicate> previous) {
		List<Predicate> restrictions = new ArrayList<>(0);

		if (inicio != null && fim != null) {
			restrictions.add(builder.between(root.get("dataCriacao"), inicio.atTime(0, 0), fim.atTime(23,59)));
		}

		if (idUsuario != null) {

			Join<Atividade, Usuario> usuarioJoin = root.join("usuario");
			restrictions.add(builder.equal(usuarioJoin.get("id"), idUsuario));

		}

		Predicate and = builder.and(restrictions.toArray(new Predicate[restrictions.size()]));
		return !previous.isPresent() ? and : builder.and(and, previous.get());
	}

}
