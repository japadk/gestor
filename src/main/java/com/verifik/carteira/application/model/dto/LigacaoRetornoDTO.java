package com.verifik.carteira.application.model.dto;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;

public class LigacaoRetornoDTO {

	@ApiModelProperty(value = "Id da ligacao", hidden = true)
	private Long id;

	@ApiModelProperty(value = "Data marcada para retorno")
	@JsonFormat(pattern="dd/MM/yyyy", timezone = JsonFormat.DEFAULT_TIMEZONE)
	private LocalDate dataRetorno;

	@ApiModelProperty(value = "Hora marcada para retorno")
	@JsonFormat(pattern="HH:mm", timezone = JsonFormat.DEFAULT_TIMEZONE)
	private LocalTime horaRetorno;

	@ApiModelProperty(value = "Usuario que fez o retorno", accessMode = AccessMode.READ_ONLY)
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	private Long idUsuario;
	
	@ApiModelProperty(value = "Nome do Usuario que fez o retorno", accessMode = AccessMode.READ_ONLY)
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	private String nomeUsuario;

	@ApiModelProperty(value = "Ligação do retorno")
	private Long idLigacao;

	@ApiModelProperty(value = "Se o retorno ja foi retornado")
	private Boolean retornado;
	
	@ApiModelProperty(value = "Se o registro está ativo")
	private Boolean registroAtivo;
	
	@ApiModelProperty(value = "Ligacao relacionada ao retorno")
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	private LigacaoDTO ligacao;

	@Generated("SparkTools")
	private LigacaoRetornoDTO(Builder builder) {
		this.id = builder.id;
		this.dataRetorno = builder.dataRetorno;
		this.horaRetorno = builder.horaRetorno;
		this.idUsuario = builder.idUsuario;
		this.nomeUsuario = builder.nomeUsuario;
		this.idLigacao = builder.idLigacao;
		this.retornado = builder.retornado;
		this.registroAtivo = builder.registroAtivo;
		this.ligacao = builder.ligacao;
	}

	public LigacaoRetornoDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDataRetorno() {
		return dataRetorno;
	}

	public void setDataRetorno(LocalDate dataRetorno) {
		this.dataRetorno = dataRetorno;
	}

	public LocalTime getHoraRetorno() {
		return horaRetorno;
	}

	public void setHoraRetorno(LocalTime horaRetorno) {
		this.horaRetorno = horaRetorno;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public Long getIdLigacao() {
		return idLigacao;
	}

	public void setIdLigacao(Long idLigacao) {
		this.idLigacao = idLigacao;
	}

	public Boolean getRetornado() {
		return retornado;
	}

	public void setRetornado(Boolean retornado) {
		this.retornado = retornado;
	}

	public Boolean getRegistroAtivo() {
		return registroAtivo;
	}

	public void setRegistroAtivo(Boolean registroAtivo) {
		this.registroAtivo = registroAtivo;
	}

	public LigacaoDTO getLigacao() {
		return ligacao;
	}

	public void setLigacao(LigacaoDTO ligacao) {
		this.ligacao = ligacao;
	}

	/**
	 * Creates builder to build {@link LigacaoRetornoDTO}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link LigacaoRetornoDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private Long id;
		private LocalDate dataRetorno;
		private LocalTime horaRetorno;
		private Long idUsuario;
		private String nomeUsuario;
		private Long idLigacao;
		private Boolean retornado;
		private Boolean registroAtivo;
		private LigacaoDTO ligacao;

		private Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withDataRetorno(LocalDate dataRetorno) {
			this.dataRetorno = dataRetorno;
			return this;
		}

		public Builder withHoraRetorno(LocalTime horaRetorno) {
			this.horaRetorno = horaRetorno;
			return this;
		}

		public Builder withIdUsuario(Long idUsuario) {
			this.idUsuario = idUsuario;
			return this;
		}

		public Builder withNomeUsuario(String nomeUsuario) {
			this.nomeUsuario = nomeUsuario;
			return this;
		}

		public Builder withIdLigacao(Long idLigacao) {
			this.idLigacao = idLigacao;
			return this;
		}

		public Builder withRetornado(Boolean retornado) {
			this.retornado = retornado;
			return this;
		}

		public Builder withRegistroAtivo(Boolean registroAtivo) {
			this.registroAtivo = registroAtivo;
			return this;
		}

		public Builder withLigacao(LigacaoDTO ligacao) {
			this.ligacao = ligacao;
			return this;
		}

		public LigacaoRetornoDTO build() {
			return new LigacaoRetornoDTO(this);
		}
	}

	
}
