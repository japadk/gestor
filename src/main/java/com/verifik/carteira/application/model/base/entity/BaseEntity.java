package com.verifik.carteira.application.model.base.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@MappedSuperclass
public abstract class BaseEntity {
	
	@Version
	@NotNull(message = "Versão não pode ser nula")
	@Column(nullable = false)
	protected Integer versao;
	
	@CreationTimestamp
	@Column(updatable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
	protected LocalDateTime dataCriacao;

	@UpdateTimestamp
	@Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
	protected LocalDateTime dataAtualizacao;
	
	@NotNull(message = "Registro ativo não pode ser nulo")
	protected Boolean registroAtivo;
	
	@Transient
	private boolean isNew = true;

	@PostLoad
	@PostPersist
	public void setIsNew() {
		isNew = false;
	}
	
	public void setIsNew(boolean isNew) {
		this.isNew = isNew;
	}

	public boolean isNew() {
		return isNew;
	}

	public abstract Long getId();

	public abstract void setId(Long id);

	public Integer getVersao() {
		return versao;
	}

	public void setVersao(Integer versao) {
		this.versao = versao;
	}

	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDateTime dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public LocalDateTime getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(LocalDateTime dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public Boolean getRegistroAtivo() {
		return registroAtivo;
	}

	public void setRegistroAtivo(Boolean registroAtivo) {
		this.registroAtivo = registroAtivo;
	}

}
