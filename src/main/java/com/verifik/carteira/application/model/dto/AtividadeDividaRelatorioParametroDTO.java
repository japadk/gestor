package com.verifik.carteira.application.model.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.format.annotation.DateTimeFormat;

import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.entities.Atividade;
import com.verifik.carteira.application.model.entities.Usuario;

import io.swagger.annotations.ApiModelProperty;

public class AtividadeDividaRelatorioParametroDTO {

	@ApiModelProperty(value = "Data inicio")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate inicio;

	@ApiModelProperty(value = "Data fim")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate fim;

	@ApiModelProperty(value = "Usuário da atividade")
	private Long idUsuario;

	public LocalDate getInicio() {
		return inicio;
	}

	public void setInicio(LocalDate inicio) {
		this.inicio = inicio;
	}

	public LocalDate getFim() {
		return fim;
	}

	public void setFim(LocalDate fim) {
		this.fim = fim;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

}
