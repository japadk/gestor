package com.verifik.carteira.application.model.dto;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import org.apache.commons.collections4.comparators.FixedOrderComparator;

import java.io.Writer;

public class RelatorioLigacoesCsvDTO {

  @CsvBindByName(column = "ATENDENTE")
  String atendente;

  @CsvBindByName(column = "NOME")
  String nome;

  @CsvBindByName(column = "CNPJ")
  String cnpj;

  @CsvBindByName(column = "TELEFONE")
  String telefone;

  @CsvBindByName(column = "OBSERVACAO")
  String observacao;

  public String getAtendente() {
    return atendente;
  }

  public void setAtendente(final String atendente) {
    this.atendente = atendente;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(final String nome) {
    this.nome = nome;
  }

  public String getCnpj() {
    return cnpj;
  }

  public void setCnpj(final String cnpj) {
    this.cnpj = cnpj;
  }

  public String getTelefone() {
    return telefone;
  }

  public void setTelefone(final String telefone) {
    this.telefone = telefone;
  }

  public String getObservacao() {
    return observacao;
  }

  public void setObservacao(final String observacao) {
    this.observacao = observacao;
  }

  public static StatefulBeanToCsv<RelatorioLigacoesCsvDTO> getBeanToCsvWriter(Writer writer) {
    HeaderColumnNameMappingStrategy<RelatorioLigacoesCsvDTO> strategy = new HeaderColumnNameMappingStrategy<>();
    strategy.setType(RelatorioLigacoesCsvDTO.class);

    String[] campos = { "ATENDENTE", "TELEFONE", "NOME", "CNPJ", "OBSERVACAO"};
    FixedOrderComparator<String> order = new FixedOrderComparator<>(campos);
    strategy.setColumnOrderOnWrite(order);

    return new StatefulBeanToCsvBuilder<RelatorioLigacoesCsvDTO>(writer).withMappingStrategy(strategy)
        .withSeparator(';').build();
  }
}
