package com.verifik.carteira.application.model.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.verifik.carteira.application.model.base.entity.BaseEntity;

@Entity
public class Atividade extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "atividade_sequence")
    @SequenceGenerator(name = "atividade_sequence", sequenceName = "atividade_sequence_pk", allocationSize = 1)
	protected Long id;

	@NotNull(message = "Entidade não pode ser nulo")
	@Column(nullable = false, length = 100)
	private String entidade;
	
	@NotNull(message = "idEntidade não pode ser nulo")
	@Column(nullable = false)
	private long idEntidade;
	
	@NotNull(message = "Tipo não pode ser nulo")
	@Column(nullable = false, length = 20)
	private String tipo;
	
	@NotNull(message = "Usuario não pode ser nulo")
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(referencedColumnName = "id", name = "idUsuario", nullable = false)
	private Usuario usuario;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "atividade", cascade = CascadeType.PERSIST)
	private List<AtividadeAcao> atividadeAcaoList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEntidade() {
		return entidade;
	}

	public void setEntidade(String entidade) {
		this.entidade = entidade;
	}

	public long getIdEntidade() {
		return idEntidade;
	}

	public void setIdEntidade(long idEntidade) {
		this.idEntidade = idEntidade;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public List<AtividadeAcao> getAtividadeAcaoList() {
		return atividadeAcaoList;
	}

	public void setAtividadeAcaoList(List<AtividadeAcao> atividadeAcaoList) {
		this.atividadeAcaoList = atividadeAcaoList;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
