package com.verifik.carteira.application.model.dto;

import javax.annotation.Generated;

import io.swagger.annotations.ApiModelProperty;

public class PerfilDTO {

	@ApiModelProperty(value = "Id do perfil", hidden = true)
	private Long id;

	@ApiModelProperty(value = "Descricao do perfil")
	private String descricao;

	@ApiModelProperty(value = "Descricao do perfil")
	private String sigla;

	@ApiModelProperty(value = "Se o registro está ativo")
	private Boolean registroAtivo;

	@Generated("SparkTools")
	private PerfilDTO(Builder builder) {
		this.id = builder.id;
		this.descricao = builder.descricao;
		this.sigla = builder.sigla;
		this.registroAtivo = builder.registroAtivo;
	}

	public PerfilDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Boolean getRegistroAtivo() {
		return registroAtivo;
	}

	public void setRegistroAtivo(Boolean registroAtivo) {
		this.registroAtivo = registroAtivo;
	}

	/**
	 * Creates builder to build {@link PerfilDTO}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link PerfilDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private Long id;
		private String descricao;
		private String sigla;
		private Boolean registroAtivo;

		private Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withDescricao(String descricao) {
			this.descricao = descricao;
			return this;
		}

		public Builder withSigla(String sigla) {
			this.sigla = sigla;
			return this;
		}

		public Builder withRegistroAtivo(Boolean registroAtivo) {
			this.registroAtivo = registroAtivo;
			return this;
		}

		public PerfilDTO build() {
			return new PerfilDTO(this);
		}
	}
	
	

}
