package com.verifik.carteira.application.model.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;

public class DividaCsvDTO {

	@CsvBindByName(column = "NUMERO")
	String numero;

	@CsvBindByName(column = "EMISSAO")
	String dataEmissao;

	@CsvBindByName(column = "VENCIMENTO")
	@CsvDate("dd/MM/yyyy")
	LocalDate dataVencimento;

	@CsvBindByName(column = "SIT.")
	String sit;

	@CsvBindByName(column = "VALOR")
	BigDecimal valor;

	@CsvBindByName(column = "NOSSO NUMERO")
	String nossoNumero;

	@CsvBindByName(column = "CODIGO")
	String codigo;

	@CsvBindByName(column = "RAZAO SOCIAL")
	String razaoSocial;

	@CsvBindByName(column = "DATA_CAD")
	String dataCad;

	@CsvBindByName(column = "email")
	String email;

	@CsvBindByName(column = "VENDEDOR")
	String vendedor;

	@CsvBindByName(column = "VALOR_NFE")
	String valorNfe;

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public LocalDate getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(LocalDate dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public String getSit() {
		return sit;
	}

	public void setSit(String sit) {
		this.sit = sit;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getNossoNumero() {
		return nossoNumero;
	}

	public void setNossoNumero(String nossoNumero) {
		this.nossoNumero = nossoNumero;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getDataCad() {
		return dataCad;
	}

	public void setDataCad(String dataCad) {
		this.dataCad = dataCad;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getVendedor() {
		return vendedor;
	}

	public void setVendedor(String vendedor) {
		this.vendedor = vendedor;
	}

	public String getValorNfe() {
		return valorNfe;
	}

	public void setValorNfe(String valorNfe) {
		this.valorNfe = valorNfe;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DividaCsvDTO [numero=").append(numero).append(", dataEmissao=").append(dataEmissao)
				.append(", dataVencimento=").append(dataVencimento.toString()).append(", sit=").append(sit).append(", valor=")
				.append(valor).append(", nossoNumero=").append(nossoNumero).append(", codigo=").append(codigo)
				.append(", razaoSocial=").append(razaoSocial).append(", dataCad=").append(dataCad).append(", email=")
				.append(email).append(", vendedor=").append(vendedor).append(", valorNfe=").append(valorNfe)
				.append("]");
		return builder.toString();
	}
	
	

}
