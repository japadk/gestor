package com.verifik.carteira.application.model.base.entity;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author marce
 *
 */
@MappedSuperclass
public abstract class FileBaseEntity extends BaseEntity {

//	@Lob
	@NotNull(message = "Arquivo não pode ser nulo")
	@Column(nullable = false)
	@JsonIgnore
	protected byte[] arquivo;

	@NotNull(message = "Filename não pode ser nulo")
	@Column(nullable = false)
	protected String nome;

	@NotNull(message = "FileExt não pode ser nulo")
	@Column(nullable = false)
	protected String extensao;

	@NotNull(message = "Tamanho não pode ser nulo")
	@Column(nullable = false)
	protected Long tamanho;
	
	@NotNull(message = "Hash não pode ser nula")
	@Column(nullable = false)
	protected String hash;
	
	@NotNull(message = "MimeType não pode ser nula")
	@Column(nullable = false)
	protected String mimeType;

	public byte[] getArquivo() {
		return arquivo;
	}

	public void setArquivo(byte[] arquivo) {
		this.arquivo = arquivo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getExtensao() {
		return extensao;
	}

	public void setExtensao(String extensao) {
		this.extensao = extensao;
	}

	public Long getTamanho() {
		return tamanho;
	}

	public void setTamanho(Long tamanho) {
		this.tamanho = tamanho;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
}
