package com.verifik.carteira.application.model.enums;

public enum EPerfil {

	ADMIN(SIGLA.ADMIN, ERegra.values()),
	ATENDENTE(SIGLA.ATENDENTE, 
			
			//USUARIO
			ERegra.VIEW_USUARIO,
			ERegra.UPDATE_USUARIO,
			
			//CARTEIRA
			ERegra.SEARCH_CARTEIRA,
			
			//CLIENTE
			ERegra.SEARCH_CLIENTE,
			ERegra.VIEW_CLIENTE,
			ERegra.CREATE_CLIENTE,
			ERegra.UPDATE_CLIENTE,
			
			//DIVIDA
			ERegra.SEARCH_DIVIDA,
			ERegra.VIEW_DIVIDA,
			ERegra.CREATE_DIVIDA,
			ERegra.UPDATE_DIVIDA
			
	),
    SUMADMIN(SIGLA.SUBADMIN, 
			
			//USUARIO
			ERegra.VIEW_USUARIO,
			ERegra.UPDATE_USUARIO,
			
			//CARTEIRA
			ERegra.SEARCH_CARTEIRA,
			
			//CLIENTE
			ERegra.SEARCH_CLIENTE,
			ERegra.VIEW_CLIENTE,
			ERegra.CREATE_CLIENTE,
			ERegra.UPDATE_CLIENTE,
			
			//DIVIDA
			ERegra.SEARCH_DIVIDA,
			ERegra.VIEW_DIVIDA,
			ERegra.CREATE_DIVIDA,
			ERegra.UPDATE_DIVIDA
			
	),
	TELEVENDA(SIGLA.TELEVENDA, 
			
			//USUARIO
			ERegra.VIEW_USUARIO,
			ERegra.UPDATE_USUARIO,
			ERegra.SEARCH_USUARIO,

			
			//LIGACAO
			ERegra.SEARCH_LIGACAO,
			ERegra.VIEW_LIGACAO,
			ERegra.CREATE_LIGACAO,
			ERegra.UPDATE_LIGACAO
			
	)
	;

	EPerfil(String sigla, ERegra... regras) {
		this.sigla = sigla;
		this.regras = regras;
	}

	private ERegra[] regras;
	private String sigla;

	public ERegra[] getRegras() {
		return regras;
	}
	
	public String getSigla() {
		return this.sigla;
	}
	
	@Override
	public String toString() {
		return getSigla();
	}
	
	public static class SIGLA {
		public static final String ADMIN        = "ADMIN";
        public static final String SUBADMIN     = "SUBADMIN";
		public static final String ATENDENTE    = "ATENDENTE";
		public static final String TELEVENDA    = "TELEVENDA";
	}

}
