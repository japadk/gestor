package com.verifik.carteira.application.model.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.verifik.carteira.application.model.base.entity.BaseEntity;

@Entity
public class UsuarioCarteira extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usuario_carteira_sequence")
    @SequenceGenerator(name = "usuario_carteira_sequence", sequenceName = "usuario_carteira_sequence_pk", allocationSize = 1)
	protected Long id;

	@NotNull(message = "Usuario não pode ser nulo")
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(referencedColumnName = "id", name = "idUsuario", nullable = false)
	private Usuario usuario;

	@NotNull(message = "Carteira não pode ser nula")
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(referencedColumnName = "id", name = "idCarteira", nullable = false)
	private Carteira carteira;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Carteira getCarteira() {
		return carteira;
	}

	public void setCarteira(Carteira carteira) {
		this.carteira = carteira;
	}

}
