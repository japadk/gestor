package com.verifik.carteira.application.model.search;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.format.annotation.DateTimeFormat;

import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.model.entities.Cliente;
import com.verifik.carteira.application.model.entities.Divida;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.entities.UsuarioCarteira;

import io.swagger.annotations.ApiModelProperty;

public class DividaSearch extends BaseSearch {

	@ApiModelProperty(value = "Id do cliente")
	private Long idCliente;
	
	@ApiModelProperty(value = "Id da carteira")
	private Long idCarteira;
	
	@ApiModelProperty(value = "List id carteira")
	private List<Long> idCarteiraList;
	
	@ApiModelProperty(value = "Se a divida esta paga")
	private Boolean pago;
	
	@ApiModelProperty(value = "Ano de vencimento da divida")
	private Integer ano;
	
	@ApiModelProperty(value = "Mes de vencimento da divida")
	private Integer mes;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataPagamentoDe;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataPagamentoAte;
	
	@ApiModelProperty(hidden = true)
	private Long idUsuarioVinculado;

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdCarteira() {
		return idCarteira;
	}

	public void setIdCarteira(Long idCarteira) {
		this.idCarteira = idCarteira;
	}

	public Boolean getPago() {
		return pago;
	}

	public void setPago(Boolean pago) {
		this.pago = pago;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Long getIdUsuarioVinculado() {
		return idUsuarioVinculado;
	}

	public void setIdUsuarioVinculado(Long idUsuarioVinculado) {
		this.idUsuarioVinculado = idUsuarioVinculado;
	}

	public List<Long> getIdCarteiraList() {
		return idCarteiraList;
	}

	public void setIdCarteiraList(List<Long> idCarteiraList) {
		this.idCarteiraList = idCarteiraList;
	}

	public LocalDate getDataPagamentoDe() {
		return dataPagamentoDe;
	}

	public void setDataPagamentoDe(LocalDate dataPagamentoDe) {
		this.dataPagamentoDe = dataPagamentoDe;
	}

	public LocalDate getDataPagamentoAte() {
		return dataPagamentoAte;
	}

	public void setDataPagamentoAte(LocalDate dataPagamentoAte) {
		this.dataPagamentoAte = dataPagamentoAte;
	}

	@Override
	@ApiModelProperty(hidden = true)
	public Class getEntityClass() {
		return Divida.class;
	}
	
	@ApiModelProperty(hidden = true)
	public List<String> getIgnoredFields() {
		return Arrays.asList("idCliente" , "idCarteira" , "ano", "mes", "idUsuarioVinculado", "dataPagamentoDe", "dataPagamentoAte" );
	}
	
	@ApiModelProperty(hidden = true)
	public Predicate getCustomPredicate(CriteriaBuilder builder, Root root, Optional<Predicate> previous) {

		List<Predicate> restrictions = new ArrayList<>(0);

		if (idCliente != null) {

			restrictions.add(builder.equal(root.get("cliente"), idCliente));

		}
		
		if (idCarteira != null) {

			Join<Divida, Cliente> join = root.join("cliente");
			restrictions.add(builder.equal(join.get("carteira"), idCarteira));

		}
		
		if (idCarteiraList != null && !idCarteiraList.isEmpty()) {

			Join<Divida, Cliente> join =  root.join("cliente");
			restrictions.add(join.get("carteira").in(idCarteiraList));

		}
		
		if ((ano != null && ano != 0) && (mes != null && mes != 0)) {
			LocalDate dataInicio = LocalDate.of(ano, mes, 1);
			LocalDate dataFim = LocalDate.of(ano, mes, 1).plusMonths(1);
			
			restrictions.add(builder.between(root.get("dataVencimento"), dataInicio,dataFim));
		} else if (ano != null && ano != 0) {
			restrictions.add(
					builder.between(root.get("dataVencimento"), LocalDate.of(ano, 1, 1), LocalDate.of(ano + 1, 1, 1)));
		}
		
		if(dataPagamentoDe != null && dataPagamentoAte != null) {
			restrictions.add(builder.between(root.get("dataPagamento"), dataPagamentoDe, dataPagamentoAte));
		}
		
		if (idUsuarioVinculado != null) {

			Join<Divida, Cliente> clienteJoin = root.join("cliente");
			Join<Cliente, Carteira> carteiraJoin = clienteJoin.join("carteira");
			Join<Carteira, UsuarioCarteira> usuarioCarteiraJoin = carteiraJoin.join("usuarioCarteiraList");
			Join<UsuarioCarteira, Usuario> usuarioJoin = usuarioCarteiraJoin.join("usuario");
			restrictions.add(builder.equal(usuarioJoin.get("id"), idUsuarioVinculado));

		}
		
		//garante que só dividas de clientes com carteira ativa sejam contados
		restrictions.add(builder.equal(root.join("cliente").join("carteira").get("registroAtivo"), true));
		//garante que só dividas de clientes ativos sejam contados
		restrictions.add(builder.equal(root.join("cliente").get("registroAtivo"), true));
		
		Predicate and = builder.and(restrictions.toArray(new Predicate[restrictions.size()]));
		return !previous.isPresent() ? and : builder.and(and, previous.get());
	}
	
	@ApiModelProperty(hidden = true)
	@Override
	public List<Order> getCustomOrderBy(CriteriaBuilder builder, Root root){
		
		List<Order> orderList = new ArrayList<Order>();
		
		Join<Divida, Cliente> clienteJoin = root.join("cliente");
		
		orderList.add(builder.asc(clienteJoin.get("codigo")));
		
		return new ArrayList<>(0);
	}
		
}
