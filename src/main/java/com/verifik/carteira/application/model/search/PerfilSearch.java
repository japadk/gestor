package com.verifik.carteira.application.model.search;

import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.entities.Perfil;

import io.swagger.annotations.ApiModelProperty;

public class PerfilSearch extends BaseSearch {

	@ApiModelProperty(value = "Descricao do perfil")
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	@ApiModelProperty(hidden = true)
	public Class getEntityClass() {
		return Perfil.class;
	}

}
