package com.verifik.carteira.application.model.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.verifik.carteira.application.model.base.entity.BaseEntity;

@Entity
public class Perfil extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "perfil_sequence")
    @SequenceGenerator(name = "perfil_sequence", sequenceName = "perfil_sequence_pk", allocationSize = 1)
	protected Long id;

	@NotNull(message = "Descricao não pode ser nulo")
	@Column(nullable = false, length = 255)
	private String descricao;

	@NotNull(message = "Sigla não pode ser nulo")
	@Column(nullable = false, length = 20, unique = true)
	private String sigla;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "perfil")
	private List<PerfilRegra> perfilRegraList;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "perfilregra", 
		joinColumns = @JoinColumn(name = "idPerfil", referencedColumnName = "id"), 
		inverseJoinColumns = @JoinColumn(name = "idRegra", referencedColumnName = "id"))
	private List<Regra> regraList;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public List<PerfilRegra> getPerfilRegraList() {
		return perfilRegraList;
	}

	public void setPerfilRegraList(List<PerfilRegra> perfilRegraList) {
		this.perfilRegraList = perfilRegraList;
	}

	public List<Regra> getRegraList() {
		return regraList;
	}

	public void setRegraList(List<Regra> regraList) {
		this.regraList = regraList;
	}

}
