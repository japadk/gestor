package com.verifik.carteira.application.model.search;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.format.annotation.DateTimeFormat;

import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.entities.LigacaoRetorno;

import io.swagger.annotations.ApiModelProperty;

public class LigacaoRetornoSearch extends BaseSearch {

	@ApiModelProperty(value = "Id do usuario")
	private Long idUsuario;
	
	@ApiModelProperty(value = "Id do usuario", hidden = true)
	private Long idUsuarioAtual;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataDe;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataAte;
	
	private Boolean meuUsuario;

	private Boolean retornados;

	private Boolean naoRetornados;

	private Boolean pendentes;

	private Boolean passados;

	private Boolean futuros;

	@Override
	@ApiModelProperty(hidden = true)
	public Class getEntityClass() {
		return LigacaoRetorno.class;
	}

	public LocalDate getDataDe() {
		return dataDe;
	}

	public void setDataDe(LocalDate dataDe) {
		this.dataDe = dataDe;
	}

	public LocalDate getDataAte() {
		return dataAte;
	}

	public void setDataAte(LocalDate dataAte) {
		this.dataAte = dataAte;
	}

	public Boolean getRetornados() {
		return retornados;
	}

	public void setRetornados(Boolean retornados) {
		this.retornados = retornados;
	}

	public Boolean getNaoRetornados() {
		return naoRetornados;
	}

	public void setNaoRetornados(Boolean naoRetornados) {
		this.naoRetornados = naoRetornados;
	}

	public Boolean getPendentes() {
		return pendentes;
	}

	public void setPendentes(Boolean pendentes) {
		this.pendentes = pendentes;
	}

	public Boolean getPassados() {
		return passados;
	}

	public void setPassados(Boolean passados) {
		this.passados = passados;
	}

	public Boolean getFuturos() {
		return futuros;
	}

	public void setFuturos(Boolean futuros) {
		this.futuros = futuros;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Boolean getMeuUsuario() {
		return meuUsuario;
	}

	public void setMeuUsuario(Boolean meuUsuario) {
		this.meuUsuario = meuUsuario;
	}

	public Long getIdUsuarioAtual() {
		return idUsuarioAtual;
	}

	public void setIdUsuarioAtual(Long idUsuarioAtual) {
		this.idUsuarioAtual = idUsuarioAtual;
	}

	@ApiModelProperty(hidden = true)
	public List<String> getIgnoredFields() {
		return Arrays.asList(new String[] { "idUsuario", "idUsuarioAtual", "meuUsuario", "dataDe", "dataAte", "naoRetornados", "retornados",
				"pendentes", "passados", "futuros" });
	}

	@ApiModelProperty(hidden = true)
	public Predicate getCustomPredicate(CriteriaBuilder builder, Root root, Optional<Predicate> previous) {

		List<Predicate> restrictions = new ArrayList<>(0);

		if (idUsuario != null) {

			restrictions.add(builder.equal(root.get("usuario"), idUsuario));

		}
		
		if(dataDe != null && dataAte != null) {
			restrictions.add(builder.between(root.get("dataRetorno"),  dataDe, dataAte));
		}
		
		
		List<Predicate> checkRestrictions = new ArrayList<>(0);
		
		if(meuUsuario != null) {
			checkRestrictions.add(builder.equal(root.get("usuario"),  idUsuarioAtual));
		}
		
		if(naoRetornados != null && naoRetornados) {
			checkRestrictions.add(builder.equal(root.get("retornado"),  false));
		}
		
		if(retornados != null && retornados) {
			checkRestrictions.add(builder.equal(root.get("retornado"),  true));
		}
		
		if(pendentes != null && pendentes) {
			Predicate naoRetornado = builder.equal(root.get("retornado"),  false);
			Predicate hojePraTras = builder.lessThan(root.get("dataRetorno"),  LocalDate.now());
			Predicate p = builder.and(naoRetornado, hojePraTras);
			restrictions.add(p);
		}
		
		if(passados != null && passados) {
			restrictions.add(builder.lessThan(root.get("dataRetorno"),  LocalDate.now()));
		}
		
		if(futuros != null && futuros) {
			restrictions.add(builder.greaterThan(root.get("dataRetorno"),  LocalDate.now()));
		}

		Predicate and = builder.and(restrictions.toArray(new Predicate[restrictions.size()]));
		if(!checkRestrictions.isEmpty()) {
			Predicate orCheckRestrictions = builder.or(checkRestrictions.toArray(new Predicate[checkRestrictions.size()]));
			and = builder.and(and, orCheckRestrictions);
		} 
		
		return !previous.isPresent() ? and : builder.and(and, previous.get());
	}

}
