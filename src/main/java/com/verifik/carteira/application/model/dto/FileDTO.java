package com.verifik.carteira.application.model.dto;

import javax.annotation.Generated;

public class FileDTO {

	String fileName;
	String extention;
	String hash;
	String mimeType;
	byte[] data;

	@Generated("SparkTools")
	private FileDTO(Builder builder) {
		this.fileName = builder.fileName;
		this.extention = builder.extention;
		this.hash = builder.hash;
		this.mimeType = builder.mimeType;
		this.data = builder.data;
	}

	public String getFileName() {
		return fileName;
	}

	public String getExtention() {
		return extention;
	}

	public byte[] getData() {
		return data;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setExtention(String extention) {
		this.extention = extention;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public String getHash() {
		return hash;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * Creates builder to build {@link FileDTO}.
	 * 
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link FileDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private String fileName;
		private String extention;
		private String hash;
		private String mimeType;
		private byte[] data;

		private Builder() {
		}

		public Builder withFileName(String fileName) {
			this.fileName = fileName;
			return this;
		}

		public Builder withExtention(String extention) {
			this.extention = extention;
			return this;
		}

		public Builder withHash(String hash) {
			this.hash = hash;
			return this;
		}

		public Builder withMimeType(String mimeType) {
			this.mimeType = mimeType;
			return this;
		}

		public Builder withData(byte[] data) {
			this.data = data;
			return this;
		}

		public FileDTO build() {
			return new FileDTO(this);
		}
	}

}
