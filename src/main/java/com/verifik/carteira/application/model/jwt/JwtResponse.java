package com.verifik.carteira.application.model.jwt;

import java.io.Serializable;
import java.util.Date;

public class JwtResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String token;
	private final Date expirationDate;
	private final String profile;

	public JwtResponse(String jwtToken, Date expirationDate, String profile) {
		super();
		this.token = jwtToken;
		this.expirationDate = expirationDate;
		this.profile = profile;
	}

	public String getToken() {
		return token;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public String getProfile() {
		return profile;
	}

}