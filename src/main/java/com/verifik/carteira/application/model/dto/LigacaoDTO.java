package com.verifik.carteira.application.model.dto;

import java.time.LocalDateTime;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;

public class LigacaoDTO {

	@ApiModelProperty(value = "Id da ligacao", hidden = true)
	private Long id;

	@ApiModelProperty(value = "Nome do contato")
	private String nome;

	@ApiModelProperty(value = "CNPJ do contato")
	private String cnpj;

	@ApiModelProperty(value = "Telefone do contato")
	private String telefone;

	@ApiModelProperty(value = "Observacao da conversa")
	private String observacao;

	@ApiModelProperty(value = "Usuario que fez a ligacao", accessMode = AccessMode.READ_ONLY)
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	private Long idUsuario;

	@ApiModelProperty(value = "Ligacao inicial de todas as conversar e retornos")
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	private Long idLigacaoPrimaria;

	@ApiModelProperty(value = "Ligacao anterior a esta")
	private Long idLigacaoAnterior;

	@ApiModelProperty(value = "Retorno")
	private LigacaoRetornoDTO retorno;
	
	@ApiModelProperty(value = "Usuario")
	private UsuarioDTO usuario;
	
	@ApiModelProperty(value = "Data ligacao")
	@JsonFormat(pattern = "dd/MM/yyyy")
	protected LocalDateTime dataLigacao;

	@Generated("SparkTools")
	private LigacaoDTO(Builder builder) {
		this.id = builder.id;
		this.nome = builder.nome;
		this.cnpj = builder.cnpj;
		this.telefone = builder.telefone;
		this.observacao = builder.observacao;
		this.idUsuario = builder.idUsuario;
		this.idLigacaoPrimaria = builder.idLigacaoPrimaria;
		this.idLigacaoAnterior = builder.idLigacaoAnterior;
		this.retorno = builder.retorno;
		this.usuario = builder.usuario;
		this.dataLigacao = builder.dataLigacao;
	}

	public LigacaoDTO() {
		super();
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Long getIdLigacaoPrimaria() {
		return idLigacaoPrimaria;
	}

	public void setIdLigacaoPrimaria(Long idLigacaoPrimaria) {
		this.idLigacaoPrimaria = idLigacaoPrimaria;
	}

	public Long getIdLigacaoAnterior() {
		return idLigacaoAnterior;
	}

	public void setIdLigacaoAnterior(Long idLigacaoAnterior) {
		this.idLigacaoAnterior = idLigacaoAnterior;
	}

	public LigacaoRetornoDTO getRetorno() {
		return retorno;
	}

	public void setRetorno(LigacaoRetornoDTO retorno) {
		this.retorno = retorno;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}
	
	public LocalDateTime getDataLigacao() {
		return dataLigacao;
	}

	public void setDataLigacao(LocalDateTime dataLigacao) {
		this.dataLigacao = dataLigacao;
	}

	/**
	 * Creates builder to build {@link LigacaoDTO}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link LigacaoDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private Long id;
		private String nome;
		private String cnpj;
		private String telefone;
		private String observacao;
		private Long idUsuario;
		private Long idLigacaoPrimaria;
		private Long idLigacaoAnterior;
		private LigacaoRetornoDTO retorno;
		private UsuarioDTO usuario;
		private LocalDateTime dataLigacao;

		private Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withNome(String nome) {
			this.nome = nome;
			return this;
		}

		public Builder withCnpj(String cnpj) {
			this.cnpj = cnpj;
			return this;
		}

		public Builder withTelefone(String telefone) {
			this.telefone = telefone;
			return this;
		}

		public Builder withObservacao(String observacao) {
			this.observacao = observacao;
			return this;
		}

		public Builder withIdUsuario(Long idUsuario) {
			this.idUsuario = idUsuario;
			return this;
		}

		public Builder withIdLigacaoPrimaria(Long idLigacaoPrimaria) {
			this.idLigacaoPrimaria = idLigacaoPrimaria;
			return this;
		}

		public Builder withIdLigacaoAnterior(Long idLigacaoAnterior) {
			this.idLigacaoAnterior = idLigacaoAnterior;
			return this;
		}

		public Builder withRetorno(LigacaoRetornoDTO retorno) {
			this.retorno = retorno;
			return this;
		}

		public Builder withUsuario(UsuarioDTO usuario) {
			this.usuario = usuario;
			return this;
		}

		public Builder withDataLigacao(LocalDateTime dataLigacao) {
			this.dataLigacao = dataLigacao;
			return this;
		}

		public LigacaoDTO build() {
			return new LigacaoDTO(this);
		}
	}

}
