package com.verifik.carteira.application.model.dto;

import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class LigacoesRelatorioParametroDTO {

	@ApiModelProperty(value = "Data inicio")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate inicio;

	@ApiModelProperty(value = "Data fim")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate fim;

	public LocalDate getInicio() {
		return inicio;
	}

	public void setInicio(LocalDate inicio) {
		this.inicio = inicio;
	}

	public LocalDate getFim() {
		return fim;
	}

	public void setFim(LocalDate fim) {
		this.fim = fim;
	}

}
