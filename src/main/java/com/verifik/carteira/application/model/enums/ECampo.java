package com.verifik.carteira.application.model.enums;


public enum ECampo {
	

	REGISTRO_ATIVO("REGISTRO ATIVO"),
	
	USUARIO_NOME("NOME"),
	USUARIO_LOGIN("LOGIN"),
	USUARIO_PERFIL("PERFIL"),
	USUARIO_CARTEIRA("CARTEIRA"),
	USUARIO_SENHA("SENHA",
		"Definido SENHA",
		"Alterado SENHA",
		"Removido SENHA"
	),
	
	CARTEIRA_NOME("NOME"),
	
	CLIENTE_CARTEIRA("CARTEIRA"),
	CLIENTE_NOME("NOME"),
	CLIENTE_NOME_FANTASIA("NOME FANTASIA"),
	CLIENTE_CNPJ("CNPJ"),
	CLIENTE_CODIGO("CODIGO"),
	CLIENTE_CONTATO("CONTATO"),
	CLIENTE_TELEFONE("TELEFONE"),
	CLIENTE_EMAIL("EMAIL"),
	
	DIVIDA_CLIENTE("CLIENTE"),
	DIVIDA_VALOR("VALOR"),
	DIVIDA_VALOR_RECEBIDO("VALOR RECEBIDO"),
	DIVIDA_DATA_VENCIMENTO("DATA VENCIMENTO"),
	DIVIDA_DATA_PAGAMENTO("DATA PAGAMENTO"),
	DIVIDA_OBSERVACAO(
		"OBSERVACAO",
		"Definido OBSERVACAO",
		"Alterado OBSERVACAO",
		"Removido OBSERVACAO"
	),
	DIVIDA_PAGO("PAGO"),
	
	RETORNO_DATA_RETORNO("DATA RETORNO"),
	RETORNO_HORA_RETORNO("HORA RETORNO"),
	RETORNO_RETORNADO("RETORNADO"),
	
	LIGACAO_NOME("NOME"),
	LIGACAO_CNPJ("CNPJ"),
	LIGACAO_TELEFONE("TELEFONE"),
	LIGACAO_OBSERVACAO("OBSERVACAO",
			"Definido OBSERVACAO",
			"Alterado OBSERVACAO",
			"Removido OBSERVACAO"
	),
	
	;
	
	private String campo;
	private String mensagemNovo;
	private String mensagemAlterado;
	private String mensagemRemovido;
	
	ECampo(String campo){
		this.campo = campo;
	}
	
	ECampo(String campo, String novo, String alterado, String removido){
		this.campo = campo;
		this.mensagemNovo = novo;
		this.mensagemAlterado = alterado;
		this.mensagemRemovido = removido;
	}
	
	public String getMensagemNovo(String valorNovo) {
		if(mensagemNovo != null) {
			return mensagemNovo.replace(":valorNovo", valorNovo);
		}
		
		return new StringBuilder().append("Definido ").append(campo).append(" = ").append(valorNovo).toString();
	}
	
	public String getMensagemAlterado(String valorAnterior, String valorNovo) {
		if(mensagemAlterado != null) {
			return mensagemAlterado.replace(":valorAnterior", valorAnterior).replace(":valorNovo", valorNovo);
		}
		
		return new StringBuilder().append("Alterado ").append(campo).append(" de ").append(valorAnterior).append(" para ").append(valorNovo).toString();
	}
	
	public String getMensagemRemovido(String valorAnterior) {
		if(mensagemRemovido != null) {
			return mensagemRemovido.replace(":valorAnterior", valorAnterior);
		}
		
		return new StringBuilder().append("Removido valor do campo ").append(campo).append(". Valor anterior = ").append(valorAnterior).toString();
	}
	
	public static String getMensagem(String campo, String valorAnterior, String valorNovo) {
		ECampo eCampo = valueOf(campo);
		if(eCampo == null) {
			return "";
		}
		
		if(valorAnterior == null) {
			return eCampo.getMensagemNovo(valorNovo);
		}
		
		if(valorNovo == null) {
			return eCampo.getMensagemRemovido(valorAnterior);
		}
		
		return eCampo.getMensagemAlterado(valorAnterior, valorNovo);
		
	}
	
}
