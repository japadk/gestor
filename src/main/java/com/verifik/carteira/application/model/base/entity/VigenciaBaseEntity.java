package com.verifik.carteira.application.model.base.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

public abstract class VigenciaBaseEntity extends BaseEntity {

	@NotNull(message = "Data de inicio da vigencia não pode ser nula")
	@Column(nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
	protected LocalDateTime dataInicioVigencia;

	@NotNull(message = "Data de fim da vigencia não pode ser nula")
	@Column(nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
	protected LocalDateTime dataFimVigencia;

	public LocalDateTime getDataInicioVigencia() {
		return dataInicioVigencia;
	}

	public LocalDateTime getDataFimVigencia() {
		return dataFimVigencia;
	}

	public void setDataInicioVigencia(LocalDateTime dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}

	public void setDataFimVigencia(LocalDateTime dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}

}
