package com.verifik.carteira.application.model.base.validation;

public abstract class BaseValidation {

	public abstract boolean isValid();
	
}
