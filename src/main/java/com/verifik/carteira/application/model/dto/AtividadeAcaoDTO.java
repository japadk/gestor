package com.verifik.carteira.application.model.dto;

import javax.annotation.Generated;

import io.swagger.annotations.ApiModelProperty;

public class AtividadeAcaoDTO {

	@ApiModelProperty(value = "Id da atividade acao")
	private Long id;

	@ApiModelProperty(value = "Valor anterior")
	private String valorAnterior;

	@ApiModelProperty(value = "Valor novo")
	private String valorNovo;
	
	@ApiModelProperty(value = "Campo")
	private String campo;
	
	@ApiModelProperty(value = "Mensagem da tela")
	private String mensagem;

	@Generated("SparkTools")
	private AtividadeAcaoDTO(Builder builder) {
		this.id = builder.id;
		this.valorAnterior = builder.valorAnterior;
		this.valorNovo = builder.valorNovo;
		this.campo = builder.campo;
		this.mensagem = builder.mensagem;
	}

	public AtividadeAcaoDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValorAnterior() {
		return valorAnterior;
	}

	public void setValorAnterior(String valorAnterior) {
		this.valorAnterior = valorAnterior;
	}

	public String getValorNovo() {
		return valorNovo;
	}

	public void setValorNovo(String valorNovo) {
		this.valorNovo = valorNovo;
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Creates builder to build {@link AtividadeAcaoDTO}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link AtividadeAcaoDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private Long id;
		private String valorAnterior;
		private String valorNovo;
		private String campo;
		private String mensagem;

		private Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withValorAnterior(String valorAnterior) {
			this.valorAnterior = valorAnterior;
			return this;
		}

		public Builder withValorNovo(String valorNovo) {
			this.valorNovo = valorNovo;
			return this;
		}

		public Builder withCampo(String campo) {
			this.campo = campo;
			return this;
		}

		public Builder withMensagem(String mensagem) {
			this.mensagem = mensagem;
			return this;
		}

		public AtividadeAcaoDTO build() {
			return new AtividadeAcaoDTO(this);
		}
	}

	
}
