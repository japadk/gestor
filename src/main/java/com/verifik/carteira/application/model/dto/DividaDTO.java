package com.verifik.carteira.application.model.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

public class DividaDTO {

	@ApiModelProperty(value = "Id do cliente", hidden = true)
	private Long id;

	@ApiModelProperty(value = "Valor da divida")
	private BigDecimal valor;
	
	@ApiModelProperty(value = "Valor recebido da divida")
	private BigDecimal valorRecebido;

	@ApiModelProperty(value = "Vencimento da divida")
	@JsonFormat(pattern="dd/MM/yyyy", timezone = JsonFormat.DEFAULT_TIMEZONE)
	private LocalDate dataVencimento;
	
	@ApiModelProperty(value = "Vencimento de pagamento da divida")
	@JsonFormat(pattern="dd/MM/yyyy", timezone = JsonFormat.DEFAULT_TIMEZONE)
	private LocalDate dataPagamento;

	@ApiModelProperty(value = "Observacao da divida")
	private String observacao;
	
	@ApiModelProperty(value = "Se a divida foi paga")
	private Boolean pago;

	@ApiModelProperty(value = "Id do cliente")
	private Long idCliente;
	
	@ApiModelProperty(value = "Informações do cliente")
	private ClienteDTO cliente;
	
	@ApiModelProperty(value = "Informações do usuario")
	private UsuarioDTO usuarioBaixa;

	@ApiModelProperty(value = "Se o registyro está ativo")
	private Boolean registroAtivo;
	
	@ApiModelProperty(value = "Data da ultima atualizacao", hidden = true)
	@JsonFormat(pattern="dd/MM/yyyy HH:mm")
	private LocalDateTime ultimaAtualizacao;

	@Generated("SparkTools")
	private DividaDTO(Builder builder) {
		this.id = builder.id;
		this.valor = builder.valor;
		this.valorRecebido = builder.valorRecebido;
		this.dataVencimento = builder.dataVencimento;
		this.dataPagamento = builder.dataPagamento;
		this.observacao = builder.observacao;
		this.pago = builder.pago;
		this.idCliente = builder.idCliente;
		this.cliente = builder.cliente;
		this.usuarioBaixa = builder.usuarioBaixa;
		this.registroAtivo = builder.registroAtivo;
		this.ultimaAtualizacao = builder.ultimaAtualizacao;
	}

	public DividaDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public LocalDate getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(LocalDate dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Boolean getRegistroAtivo() {
		return registroAtivo;
	}

	public void setRegistroAtivo(Boolean registroAtivo) {
		this.registroAtivo = registroAtivo;
	}

	public Boolean getPago() {
		return pago;
	}

	public void setPago(Boolean pago) {
		this.pago = pago;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public LocalDateTime getUltimaAtualizacao() {
		return ultimaAtualizacao;
	}

	public void setUltimaAtualizacao(LocalDateTime ultimaAtualizacao) {
		this.ultimaAtualizacao = ultimaAtualizacao;
	}

	public UsuarioDTO getUsuarioBaixa() {
		return usuarioBaixa;
	}

	public void setUsuarioBaixa(UsuarioDTO usuarioBaixa) {
		this.usuarioBaixa = usuarioBaixa;
	}

	public BigDecimal getValorRecebido() {
		return valorRecebido;
	}

	public void setValorRecebido(BigDecimal valorRecebido) {
		this.valorRecebido = valorRecebido;
	}

	public LocalDate getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(LocalDate dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	/**
	 * Creates builder to build {@link DividaDTO}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link DividaDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private Long id;
		private BigDecimal valor;
		private BigDecimal valorRecebido;
		private LocalDate dataVencimento;
		private LocalDate dataPagamento;
		private String observacao;
		private Boolean pago;
		private Long idCliente;
		private ClienteDTO cliente;
		private UsuarioDTO usuarioBaixa;
		private Boolean registroAtivo;
		private LocalDateTime ultimaAtualizacao;

		private Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withValor(BigDecimal valor) {
			this.valor = valor;
			return this;
		}

		public Builder withValorRecebido(BigDecimal valorRecebido) {
			this.valorRecebido = valorRecebido;
			return this;
		}

		public Builder withDataVencimento(LocalDate dataVencimento) {
			this.dataVencimento = dataVencimento;
			return this;
		}

		public Builder withDataPagamento(LocalDate dataPagamento) {
			this.dataPagamento = dataPagamento;
			return this;
		}

		public Builder withObservacao(String observacao) {
			this.observacao = observacao;
			return this;
		}

		public Builder withPago(Boolean pago) {
			this.pago = pago;
			return this;
		}

		public Builder withIdCliente(Long idCliente) {
			this.idCliente = idCliente;
			return this;
		}

		public Builder withCliente(ClienteDTO cliente) {
			this.cliente = cliente;
			return this;
		}

		public Builder withUsuarioBaixa(UsuarioDTO usuarioBaixa) {
			this.usuarioBaixa = usuarioBaixa;
			return this;
		}

		public Builder withRegistroAtivo(Boolean registroAtivo) {
			this.registroAtivo = registroAtivo;
			return this;
		}

		public Builder withUltimaAtualizacao(LocalDateTime ultimaAtualizacao) {
			this.ultimaAtualizacao = ultimaAtualizacao;
			return this;
		}

		public DividaDTO build() {
			return new DividaDTO(this);
		}
	}

}
