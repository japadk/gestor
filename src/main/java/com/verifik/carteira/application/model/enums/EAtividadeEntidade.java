package com.verifik.carteira.application.model.enums;

public enum EAtividadeEntidade {

	CARTEIRA("carteiras/update/:id"), //
	CLIENTE("clientes/update/:id"), //
	DIVIDA("dividas/update/:id"), //
	USUARIO("usuarios/update/:id"), //
	LIGACAO("ligacoes/update/:id"), //
	LIGACAORETORNO("retornos/update/:id"), //

	;

	EAtividadeEntidade(String updateLink) {
		this.updateLink = updateLink;
	}

	private String updateLink;

	public static String getUpdateLink(String entidade, long idEntidade) {
		return valueOf(entidade).updateLink.replace(":id", String.valueOf(idEntidade));
	}

	public static String getMensagem(String usuario, String tipo, String entidade) {
		return entidade + " foi " + (tipo.equals("UPDATE") ? "atualizado(a)" : "criado(a)") + " por " + usuario;
	}

}
