package com.verifik.carteira.application.model.dto;

import javax.annotation.Generated;

import io.swagger.annotations.ApiModelProperty;

public class UsuarioCarteiraDTO {

	@ApiModelProperty(value = "Id Usuario Carteira", hidden = true)
	private Long id;

	@ApiModelProperty(value = "Usuario")
	private UsuarioDTO usuario;

	@ApiModelProperty(value = "Carteira")
	private CarteiraDTO carteira;

	@ApiModelProperty(value = "Se o registro está ativo")
	private Boolean registroAtivo;

	@Generated("SparkTools")
	private UsuarioCarteiraDTO(Builder builder) {
		this.id = builder.id;
		this.usuario = builder.usuario;
		this.carteira = builder.carteira;
		this.registroAtivo = builder.registroAtivo;
	}

	public UsuarioCarteiraDTO() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	public CarteiraDTO getCarteira() {
		return carteira;
	}

	public void setCarteira(CarteiraDTO carteira) {
		this.carteira = carteira;
	}

	public Boolean getRegistroAtivo() {
		return registroAtivo;
	}

	public void setRegistroAtivo(Boolean registroAtivo) {
		this.registroAtivo = registroAtivo;
	}

	/**
	 * Creates builder to build {@link UsuarioCarteiraDTO}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link UsuarioCarteiraDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private Long id;
		private UsuarioDTO usuario;
		private CarteiraDTO carteira;
		private Boolean registroAtivo;

		private Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withUsuario(UsuarioDTO usuario) {
			this.usuario = usuario;
			return this;
		}

		public Builder withCarteira(CarteiraDTO carteira) {
			this.carteira = carteira;
			return this;
		}

		public Builder withRegistroAtivo(Boolean registroAtivo) {
			this.registroAtivo = registroAtivo;
			return this;
		}

		public UsuarioCarteiraDTO build() {
			return new UsuarioCarteiraDTO(this);
		}
	}

	
}
