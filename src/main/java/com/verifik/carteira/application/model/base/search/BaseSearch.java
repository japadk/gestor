package com.verifik.carteira.application.model.base.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;

public abstract class BaseSearch {

	@ApiModelProperty(value = "Id do registro")
	private Long id;

	@ApiModelProperty(value = "Define se será feito uma busca com OR")
	protected boolean or = false;

	@ApiModelProperty(value = "Limita a quantidade retornada de registros", dataType = "int", example = "25")
	protected int limit = 25;

	@ApiModelProperty(value = "Define o indice de inicio da paginação", dataType = "int", example = "0")
	protected int offset = 0;

	@ApiModelProperty(value = "Se o registro está ou não ativo")
	private Boolean registroAtivo;
	
	@ApiModelProperty(value = "Campo que será utilizado para ordenação")
	private String orderBy;
	
	@ApiModelProperty(value = "Se a ordenação será decrecente")
	private Boolean desc;
	
	@ApiModelProperty(value = "Lista de order by")
	@ApiParam(allowMultiple = true)
	private List<String> orderByList;
	
	@ApiModelProperty(hidden = true)
	public abstract Class getEntityClass();
	
	@ApiModelProperty(hidden = true)
	public List<String> getIgnoredFields(){
		return new ArrayList<>(0);
	}
	
	@ApiModelProperty(hidden = true)
	public List<Predicate> getCustomRules(CriteriaBuilder builder, Root root){
		return new ArrayList<>(0);
	}
	
	@ApiModelProperty(hidden = true)
	public Predicate getCustomPredicate(CriteriaBuilder builder, Root root, Optional<Predicate> previous){
		return previous.isPresent() ? previous.get() : builder.isTrue(builder.literal(true));
	}
	
	@ApiModelProperty(hidden = true)
	public List<Order> getCustomOrderBy(CriteriaBuilder builder, Root root){
		return new ArrayList<>(0);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isOr() {
		return or;
	}

	public void setOr(boolean or) {
		this.or = or;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public Boolean getRegistroAtivo() {
		return registroAtivo;
	}

	public void setRegistroAtivo(Boolean registroAtivo) {
		this.registroAtivo = registroAtivo;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public Boolean getDesc() {
		return desc;
	}

	public void setDesc(Boolean desc) {
		this.desc = desc;
	}

	public List<String> getOrderByList() {
		return orderByList;
	}

	public void setOrderByList(List<String> orderByList) {
		this.orderByList = orderByList;
	}

}
