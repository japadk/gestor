package com.verifik.carteira.application.model.dto;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

public class AtividadeDTO {

	@ApiModelProperty(value = "Id da atividade")
	private Long id;
	
	@ApiModelProperty(value = "Id da entidade")
	private Long idEntidade;

	@ApiModelProperty(value = "Entidade")
	private String entidade;
	
	@ApiModelProperty(value = "Tipo")
	private String tipo;
	
	@ApiModelProperty(value = "Usuario")
	private UsuarioDTO usuario;
	
	@ApiModelProperty(value = "Mensagem da tela")
	private String mensagem;
	
	private List<AtividadeAcaoDTO> atividadeAcaoList;
	
	@JsonFormat(pattern="dd/MM/yyyy - HH:mm:ss")
	private LocalDateTime dataCriacao;
	
	private String updateLink;

	@Generated("SparkTools")
	private AtividadeDTO(Builder builder) {
		this.id = builder.id;
		this.idEntidade = builder.idEntidade;
		this.entidade = builder.entidade;
		this.tipo = builder.tipo;
		this.usuario = builder.usuario;
		this.mensagem = builder.mensagem;
		this.atividadeAcaoList = builder.atividadeAcaoList;
		this.dataCriacao = builder.dataCriacao;
		this.updateLink = builder.updateLink;
	}

	public AtividadeDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdEntidade() {
		return idEntidade;
	}

	public void setIdEntidade(Long idEntidade) {
		this.idEntidade = idEntidade;
	}

	public String getEntidade() {
		return entidade;
	}

	public void setEntidade(String entidade) {
		this.entidade = entidade;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public List<AtividadeAcaoDTO> getAtividadeAcaoList() {
		return atividadeAcaoList;
	}

	public void setAtividadeAcaoList(List<AtividadeAcaoDTO> atividadeAcaoList) {
		this.atividadeAcaoList = atividadeAcaoList;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDateTime dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public String getUpdateLink() {
		return updateLink;
	}

	public void setUpdateLink(String updateLink) {
		this.updateLink = updateLink;
	}

	/**
	 * Creates builder to build {@link AtividadeDTO}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link AtividadeDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private Long id;
		private Long idEntidade;
		private String entidade;
		private String tipo;
		private UsuarioDTO usuario;
		private String mensagem;
		private List<AtividadeAcaoDTO> atividadeAcaoList = Collections.emptyList();
		private LocalDateTime dataCriacao;
		private String updateLink;

		private Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withIdEntidade(Long idEntidade) {
			this.idEntidade = idEntidade;
			return this;
		}

		public Builder withEntidade(String entidade) {
			this.entidade = entidade;
			return this;
		}

		public Builder withTipo(String tipo) {
			this.tipo = tipo;
			return this;
		}

		public Builder withUsuario(UsuarioDTO usuario) {
			this.usuario = usuario;
			return this;
		}

		public Builder withMensagem(String mensagem) {
			this.mensagem = mensagem;
			return this;
		}

		public Builder withAtividadeAcaoList(List<AtividadeAcaoDTO> atividadeAcaoList) {
			this.atividadeAcaoList = atividadeAcaoList;
			return this;
		}

		public Builder withDataCriacao(LocalDateTime dataCriacao) {
			this.dataCriacao = dataCriacao;
			return this;
		}

		public Builder withUpdateLink(String updateLink) {
			this.updateLink = updateLink;
			return this;
		}

		public AtividadeDTO build() {
			return new AtividadeDTO(this);
		}
	}

	
}
