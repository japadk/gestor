package com.verifik.carteira.application.model.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.verifik.carteira.application.model.base.entity.BaseEntity;

@Entity
public class Carteira extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "carteira_sequence")
    @SequenceGenerator(name = "carteira_sequence", sequenceName = "carteira_sequence_pk", allocationSize = 1)
	protected Long id;

	@NotNull(message = "Nome não pode ser nulo")
	@Column(nullable = false, length = 255)
	private String nome;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "carteira")
	private List<UsuarioCarteira> usuarioCarteiraList;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "usuariocarteira", 
		joinColumns = @JoinColumn(name = "idCarteira", referencedColumnName = "id"), 
		inverseJoinColumns = @JoinColumn(name = "idUsuario", referencedColumnName = "id"))
	private List<Usuario> usuarioList;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<UsuarioCarteira> getUsuarioCarteiraList() {
		return usuarioCarteiraList;
	}

	public void setUsuarioCarteiraList(List<UsuarioCarteira> usuarioCarteiraList) {
		this.usuarioCarteiraList = usuarioCarteiraList;
	}

	public List<Usuario> getUsuarioList() {
		return usuarioList;
	}

	public void setUsuarioList(List<Usuario> usuarioList) {
		this.usuarioList = usuarioList;
	}

}
