package com.verifik.carteira.application.model.dto;

import java.io.Writer;

import org.apache.commons.collections4.comparators.FixedOrderComparator;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

public class RelatorioDividasNaoPagasCsvDTO {
	
	@CsvBindByName(column = "Codigo")
	String codigo;

	@CsvBindByName(column = "Cliente")
	String cliente;

	@CsvBindByName(column = "CNPJ")
	String cnpj;

	@CsvBindByName(column = "Contato")
	String contato;

	@CsvBindByName(column = "Telefone")
	String telefone;

	@CsvBindByName(column = "Email")
	String email;

	@CsvBindByName(column = "Carteira")
	String carteira;

	@CsvBindByName(column = "Valor")
	String valor;

	@CsvBindByName(column = "Vencimento")
	String vencimento;

	@CsvBindByName(column = "Observacao")
	String observacao;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getVencimento() {
		return vencimento;
	}

	public void setVencimento(String vencimento) {
		this.vencimento = vencimento;
	}
	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getCarteira() {
		return carteira;
	}

	public void setCarteira(String carteira) {
		this.carteira = carteira;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public static StatefulBeanToCsv<RelatorioDividasNaoPagasCsvDTO> getBeanToCsvWriter(Writer writer) {
		HeaderColumnNameMappingStrategy<RelatorioDividasNaoPagasCsvDTO> strategy = new HeaderColumnNameMappingStrategy<>();
		strategy.setType(RelatorioDividasNaoPagasCsvDTO.class);

		String[] campos = {"CODIGO", "CLIENTE", "CNPJ", "CONTATO", "TELEFONE", "EMAIL", "CARTEIRA", "VALOR" , "VENCIMENTO" , "OBSERVACAO" };
		FixedOrderComparator<String> order = new FixedOrderComparator<>(campos);
		strategy.setColumnOrderOnWrite(order);

		return new StatefulBeanToCsvBuilder<RelatorioDividasNaoPagasCsvDTO>(writer).withMappingStrategy(strategy)
				.withSeparator(';').build();
	}
}
