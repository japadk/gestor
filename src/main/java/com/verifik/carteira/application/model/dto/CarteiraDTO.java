package com.verifik.carteira.application.model.dto;

import javax.annotation.Generated;

import io.swagger.annotations.ApiModelProperty;

public class CarteiraDTO {

	@ApiModelProperty(value = "Id da carteira")
	private Long id;

	@ApiModelProperty(value = "Nome da carteira")
	private String nome;

	@ApiModelProperty(value = "Se o registro está ativo")
	private Boolean registroAtivo;

	@Generated("SparkTools")
	private CarteiraDTO(Builder builder) {
		this.id = builder.id;
		this.nome = builder.nome;
		this.registroAtivo = builder.registroAtivo;
	}

	public CarteiraDTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Boolean getRegistroAtivo() {
		return registroAtivo;
	}

	public void setRegistroAtivo(Boolean registroAtivo) {
		this.registroAtivo = registroAtivo;
	}

	/**
	 * Creates builder to build {@link CarteiraDTO}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link CarteiraDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private Long id;
		private String nome;
		private Boolean registroAtivo;

		private Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withNome(String nome) {
			this.nome = nome;
			return this;
		}

		public Builder withRegistroAtivo(Boolean registroAtivo) {
			this.registroAtivo = registroAtivo;
			return this;
		}

		public CarteiraDTO build() {
			return new CarteiraDTO(this);
		}
	}

	
}
