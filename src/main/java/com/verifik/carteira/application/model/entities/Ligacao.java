package com.verifik.carteira.application.model.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import com.verifik.carteira.application.model.base.entity.BaseEntity;

@Entity
public class Ligacao extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ligacao_sequence")
	@SequenceGenerator(name = "ligacao_sequence", sequenceName = "ligacao_sequence_pk", allocationSize = 1)
	protected Long id;

	@NotNull(message = "Nome não pode ser nulo")
	@Column(nullable = false, length = 100)
	private String nome;

	@NotNull(message = "Cnpj não pode ser nulo")
	@Column(nullable = false, length = 100)
	private String cnpj;

	@NotNull(message = "Telefone não pode ser nulo")
	@Column(nullable = false, length = 100)
	private String telefone;

	@Column(length = 10000)
	private String observacao;

	@NotNull(message = "Usuario não pode ser nulo")
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(referencedColumnName = "id", name = "idUsuario", nullable = false)
	private Usuario usuario;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(referencedColumnName = "id", name = "idLigacaoPrimaria")
	private Ligacao ligacaoPrimaria;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(referencedColumnName = "id", name = "idLigacaoAnterior")
	private Ligacao ligacaoAnterior;
	
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "ligacao", cascade = CascadeType.PERSIST)
	private LigacaoRetorno ligacaoRetorno;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Ligacao getLigacaoPrimaria() {
		return ligacaoPrimaria;
	}

	public void setLigacaoPrimaria(Ligacao ligacaoPrimaria) {
		this.ligacaoPrimaria = ligacaoPrimaria;
	}

	public Ligacao getLigacaoAnterior() {
		return ligacaoAnterior;
	}

	public void setLigacaoAnterior(Ligacao ligacaoAnterior) {
		this.ligacaoAnterior = ligacaoAnterior;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public LigacaoRetorno getLigacaoRetorno() {
		return ligacaoRetorno;
	}

	public void setLigacaoRetorno(LigacaoRetorno ligacaoRetorno) {
		this.ligacaoRetorno = ligacaoRetorno;
	}

}
