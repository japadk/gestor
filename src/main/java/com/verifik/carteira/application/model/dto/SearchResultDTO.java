package com.verifik.carteira.application.model.dto;

import java.util.List;
import javax.annotation.Generated;
import java.util.Collections;

public class SearchResultDTO {

	private long totalCount;
	private long currentCount;
	private long limit;
	private long offset;
	private List<Object> list;
	
	public SearchResultDTO() {};

	@Generated("SparkTools")
	private SearchResultDTO(Builder builder) {
		this.totalCount = builder.totalCount;
		this.currentCount = builder.currentCount;
		this.limit = builder.limit;
		this.offset = builder.offset;
		this.list = builder.list;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	public long getCurrentCount() {
		return currentCount;
	}

	public void setCurrentCount(long currentCount) {
		this.currentCount = currentCount;
	}

	public long getLimit() {
		return limit;
	}

	public void setLimit(long limit) {
		this.limit = limit;
	}

	public long getOffset() {
		return offset;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}

	public List<Object> getList() {
		return list;
	}

	public void setList(List<Object> list) {
		this.list = list;
	}

	/**
	 * Creates builder to build {@link SearchResultDTO}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link SearchResultDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private long totalCount;
		private long currentCount;
		private long limit;
		private long offset;
		private List<Object> list = Collections.emptyList();

		private Builder() {
		}

		public Builder withTotalCount(long totalCount) {
			this.totalCount = totalCount;
			return this;
		}

		public Builder withCurrentCount(long currentCount) {
			this.currentCount = currentCount;
			return this;
		}

		public Builder withLimit(long limit) {
			this.limit = limit;
			return this;
		}

		public Builder withOffset(long offset) {
			this.offset = offset;
			return this;
		}

		public Builder withList(List<Object> list) {
			this.list = list;
			return this;
		}

		public SearchResultDTO build() {
			return new SearchResultDTO(this);
		}
	}
	
	

}
