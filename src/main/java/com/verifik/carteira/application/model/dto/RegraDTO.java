package com.verifik.carteira.application.model.dto;

import javax.annotation.Generated;

import io.swagger.annotations.ApiModelProperty;

public class RegraDTO {

	@ApiModelProperty(value = "Id do perfil", hidden = true)
	private Long id;

	@ApiModelProperty(value = "Descricao da regra")
	private String descricao;

	@ApiModelProperty(value = "Chave da rera")
	private String chave;

	@ApiModelProperty(value = "Se o registro está ativo")
	private Boolean registroAtivo;

	@Generated("SparkTools")
	private RegraDTO(Builder builder) {
		this.id = builder.id;
		this.descricao = builder.descricao;
		this.chave = builder.chave;
		this.registroAtivo = builder.registroAtivo;
	}

	public RegraDTO() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public Boolean getRegistroAtivo() {
		return registroAtivo;
	}

	public void setRegistroAtivo(Boolean registroAtivo) {
		this.registroAtivo = registroAtivo;
	}

	/**
	 * Creates builder to build {@link RegraDTO}.
	 * 
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link RegraDTO}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private Long id;
		private String descricao;
		private String chave;
		private Boolean registroAtivo;

		private Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withDescricao(String descricao) {
			this.descricao = descricao;
			return this;
		}

		public Builder withChave(String chave) {
			this.chave = chave;
			return this;
		}

		public Builder withRegistroAtivo(Boolean registroAtivo) {
			this.registroAtivo = registroAtivo;
			return this;
		}

		public RegraDTO build() {
			return new RegraDTO(this);
		}
	}

}
