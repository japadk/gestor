package com.verifik.carteira.application.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.verifik.carteira.application.model.dto.DividaDTO;
import com.verifik.carteira.application.model.dto.DividaTotaisDTO;
import com.verifik.carteira.application.model.dto.FileDTO;
import com.verifik.carteira.application.model.dto.ResponseDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.entities.Divida;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.model.enums.ERegra;
import com.verifik.carteira.application.model.search.DividaSearch;
import com.verifik.carteira.application.service.divida.IDividaService;
import com.verifik.carteira.application.service.file.FileStorageService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("dividas")
@Api(tags = "dividas")
public class DividaController {

	private static final Logger logger = LoggerFactory.getLogger(DividaController.class);

	@Autowired
	IDividaService dividaService;
	
	@Autowired
	FileStorageService fileStorageService;

	@GetMapping(path = "/search")
	@ApiOperation(value = "Realiza uma busca por atributos da divida", responseContainer = "List")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.SEARCH_DIVIDA + "'))")
	public SearchResultDTO search(DividaSearch dividaSearch) {
		return dividaService.pagedSearch(dividaSearch);
	}

	@GetMapping(path = "/{id}")
	@ApiOperation(value = "Retorna a divida com o id especifico")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.VIEW_DIVIDA + "'))")
	public ResponseEntity view(@PathVariable("id") long id) {

		Divida divida = dividaService.findById(id);

		// Se não encontrou Divida, retorna Not Found
		if (divida == null) {
			return ResponseEntity.notFound().build();
		}

		// Se encontrou, retorna OK e o parceiro
		return ResponseEntity.ok().body(dividaService.toDTO(divida));
	}

	@PostMapping
	@ApiOperation(value = "Cria uma divida nova.")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.CREATE_DIVIDA + "'))")
	public ResponseEntity create(@RequestBody DividaDTO dividaDTO) {
		try {
			return ResponseEntity
					.ok(dividaService.toDTO(dividaService.save(dividaDTO)));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);

			ResponseDTO response = new ResponseDTO();
			response.setData(null);
			response.setError(true);
			response.setMessage("Erro interno do servidor. Entre em contato com o administrador.");

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
	}

	@PutMapping(path = "/{id}")
	@ApiOperation(value = "Atualiza um parceiro com id especifico. O que não for preenchido é mantido")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.UPDATE_DIVIDA + "') or hasPerfil('" + EPerfil.SIGLA.ADMIN + "'))")
	public ResponseEntity update(@PathVariable("id") long id, @RequestBody DividaDTO dividaDTO) {

		try {

			Divida dividaDB = dividaService.findById(id);

			// Se não encontrou Divida, retorna Not Found
			if (dividaDB == null) {
				return ResponseEntity.notFound().build();
			}

			dividaDB = dividaService.save(dividaDTO, id);

			return ResponseEntity.ok().body(dividaService.toDTO(dividaDB));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

			ResponseDTO response = new ResponseDTO();
			response.setData(null);
			response.setError(true);
			response.setMessage("Erro interno do servidor. Entre em contato com o administrador.");

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}

	}
	
	@PostMapping("/import")
    @PreAuthorize("hasPerfil('" + EPerfil.SIGLA.ADMIN + "')")
    public ResponseEntity importCsv(@RequestParam("file") MultipartFile file) {
    	
    	try {
    		
    		FileDTO dto = fileStorageService.toFileDTO(file);
    		
    		if(dto.getExtention() == null || !dto.getExtention().equals("csv")) {
    			ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid file extension");
    		}
    		
    		dividaService.importCsv(file);
			
			return ResponseEntity.ok().build();
			
			
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			
			ResponseDTO response = new ResponseDTO();
			response.setData(null);
			response.setError(true);
			response.setMessage("Erro interno do servidor. Entre em contato com o administrador.");

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
    }
	
	@GetMapping(path = "/count")
	@ApiOperation(value = "Retorna totais da busca", responseContainer = "List")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.SEARCH_DIVIDA + "'))")
	public DividaTotaisDTO count(DividaSearch dividaSearch) {
		return dividaService.countTotais(dividaSearch);
	}


}