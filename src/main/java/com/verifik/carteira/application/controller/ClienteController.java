package com.verifik.carteira.application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.verifik.carteira.application.model.dto.ClienteDTO;
import com.verifik.carteira.application.model.dto.ResponseDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.entities.Cliente;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.model.enums.ERegra;
import com.verifik.carteira.application.model.search.ClienteSearch;
import com.verifik.carteira.application.service.cliente.IClienteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("clientes")
@Api(tags = "clientes")
public class ClienteController {

	private static final Logger logger = LoggerFactory.getLogger(ClienteController.class);

	@Autowired
	IClienteService clienteService;

	@GetMapping(path = "/search")
	@ApiOperation(value = "Realiza uma busca por atributos do cliente", responseContainer = "List")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.SEARCH_CLIENTE + "'))")
	public SearchResultDTO search(ClienteSearch parceiroSearch) {
		return clienteService.pagedSearch(parceiroSearch);
	}

	@GetMapping(path = "/{id}")
	@ApiOperation(value = "Retorna o cliente com o id especifico")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.VIEW_CLIENTE + "'))")
	public ResponseEntity view(@PathVariable("id") long id) {

		Cliente cliente = clienteService.findById(id);

		// Se não encontrou Parceiro, retorna Not Found
		if (cliente == null) {
			return ResponseEntity.notFound().build();
		}

		// Se encontrou, retorna OK e o parceiro
		return ResponseEntity.ok().body(clienteService.toDTO(cliente));
	}

	@PostMapping
	@ApiOperation(value = "Cria um cliente novo.")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.CREATE_CLIENTE + "'))")
	public ResponseEntity create(@RequestBody ClienteDTO clienteDTO) {
		try {
			return ResponseEntity
					.ok(clienteService.toDTO(clienteService.save(clienteDTO)));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);

			ResponseDTO response = new ResponseDTO();
			response.setData(null);
			response.setError(true);
			response.setMessage("Erro interno do servidor. Entre em contato com o administrador.");

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
	}

	@PutMapping(path = "/{id}")
	@ApiOperation(value = "Atualiza um cliente com id especifico. O que não for preenchido é mantido")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.UPDATE_CLIENTE + "') or hasPerfil('" + EPerfil.SIGLA.ADMIN + "'))")
	public ResponseEntity update(@PathVariable("id") long id, @RequestBody ClienteDTO clienteDTO) {

		try {

			Cliente clienteDB = clienteService.findById(id);

			// Se não encontrou Parceiro, retorna Not Found
			if (clienteDB == null) {
				return ResponseEntity.notFound().build();
			}

			clienteDB = clienteService.save(clienteDTO, id);

			return ResponseEntity.ok().body(clienteService.toDTO(clienteDB));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

			ResponseDTO response = new ResponseDTO();
			response.setData(null);
			response.setError(true);
			response.setMessage("Erro interno do servidor. Entre em contato com o administrador.");

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}

	}

}