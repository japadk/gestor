package com.verifik.carteira.application.controller;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;

import com.verifik.carteira.application.model.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.service.relatorio.IRelatorioService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("relatorios")
@Api(tags = "relatorios")
public class RelatorioController {

  private static final Logger logger = LoggerFactory.getLogger(RelatorioController.class);

  @Autowired
  IRelatorioService relatorioService;

  @GetMapping(path = "/atividades/dividas")
  @ApiOperation(value = "Gera relatorio de atividades de dividas")
  @PreAuthorize("hasPerfil('" + EPerfil.SIGLA.ADMIN + "')")
  public ResponseEntity dividas(AtividadeDividaRelatorioParametroDTO dividaSearch)
      throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {

    List<RelatorioAtividadeDividaCsvDTO> lista = relatorioService.buscaRelatorioAtividadeDividaDTO(dividaSearch);

    Writer writer = new StringWriter();

    StatefulBeanToCsv<RelatorioAtividadeDividaCsvDTO> beanToCsv = RelatorioAtividadeDividaCsvDTO
        .getBeanToCsvWriter(writer);
    beanToCsv.write(lista);
    writer.close();

    return ResponseEntity.ok()
        .header("Content-Type", "text/csv; charset=utf-8")
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"relatorio.csv\"")
        .body(writer.toString());
  }

  @GetMapping(path = "/dividas/pagas")
  @ApiOperation(value = "Gera relatorio de dividas pagas por periodo")
  @PreAuthorize("hasPerfil('" + EPerfil.SIGLA.ADMIN + "')")
  public ResponseEntity dividasPagas(DividasPagasRelatorioParametroDTO dividaSearch)
      throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {

    List<RelatorioDividasPagasCsvDTO> lista = relatorioService.buscaRelatorioDividaPagasDTO(dividaSearch);

    Writer writer = new StringWriter();

    StatefulBeanToCsv<RelatorioDividasPagasCsvDTO> beanToCsv = RelatorioDividasPagasCsvDTO
        .getBeanToCsvWriter(writer);
    beanToCsv.write(lista);
    writer.close();

    return ResponseEntity.ok()
        .header("Content-Type", "text/csv; charset=utf-8")
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"relatorio.csv\"")
        .body(writer.toString());
  }

  @GetMapping(path = "/dividas/naopagas")
  @ApiOperation(value = "Gera relatorio de dividas nao pagas por periodo")
  @PreAuthorize("hasPerfil('" + EPerfil.SIGLA.ADMIN + "')")
  public ResponseEntity dividasNaoPagas(DividasNaoPagasRelatorioParametroDTO dividaSearch)
      throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {

    List<RelatorioDividasNaoPagasCsvDTO> lista = relatorioService.buscaRelatorioDividaNaoPagasDTO(dividaSearch);

    Writer writer = new StringWriter();

    StatefulBeanToCsv<RelatorioDividasNaoPagasCsvDTO> beanToCsv = RelatorioDividasNaoPagasCsvDTO
        .getBeanToCsvWriter(writer);
    beanToCsv.write(lista);
    writer.close();

    return ResponseEntity.ok()
        .header("Content-Type", "text/csv; charset=utf-8")
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"relatorio.csv\"")
        .body(writer.toString());
  }

  @GetMapping(path = "/dividas/todas")
  @ApiOperation(value = "Gera relatorio de dividas periodo")
  @PreAuthorize("hasPerfil('" + EPerfil.SIGLA.ADMIN + "')")
  public ResponseEntity dividasTodas(DividasRelatorioParametroDTO dividaSearch)
          throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {

    List<RelatorioDividasCsvDTO> lista = relatorioService.buscaRelatorioDividaDTO(dividaSearch);

    Writer writer = new StringWriter();

    StatefulBeanToCsv<RelatorioDividasCsvDTO> beanToCsv = RelatorioDividasCsvDTO
            .getBeanToCsvWriter(writer);
    beanToCsv.write(lista);
    writer.close();

    return ResponseEntity.ok()
            .header("Content-Type", "text/csv; charset=utf-8")
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"relatorio.csv\"")
            .body(writer.toString());
  }

  @GetMapping(path = "/clientes/ativos")
  @ApiOperation(value = "Gera relatorio de clientes ativos")
  @PreAuthorize("hasPerfil('" + EPerfil.SIGLA.ADMIN + "')")
  public ResponseEntity clientesAtivos()
          throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {

    List<RelatorioClienteCsvDTO> lista = relatorioService.buscaClientesAtivos();

    Writer writer = new StringWriter();

    StatefulBeanToCsv<RelatorioClienteCsvDTO> beanToCsv = RelatorioClienteCsvDTO
            .getBeanToCsvWriter(writer);
    beanToCsv.write(lista);
    writer.close();

    return ResponseEntity.ok()
            .header("Content-Type", "text/csv; charset=utf-8")
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"relatorio.csv\"")
            .body(writer.toString());
  }


  @GetMapping(path = "/ligacoes/todas")
  @ApiOperation(value = "Gera relatorio de dividas periodo")
  @PreAuthorize("hasPerfil('" + EPerfil.SIGLA.ADMIN + "')")
  public ResponseEntity ligacoesTodas(LigacoesRelatorioParametroDTO ligacoesRelatorioParametroDTO)
      throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {

    List<RelatorioLigacoesCsvDTO> lista = relatorioService.buscaRelatorioLigacoesDTO(ligacoesRelatorioParametroDTO);

    Writer writer = new StringWriter();

    StatefulBeanToCsv<RelatorioLigacoesCsvDTO> beanToCsv = RelatorioLigacoesCsvDTO
        .getBeanToCsvWriter(writer);
    beanToCsv.write(lista);
    writer.close();

    return ResponseEntity.ok()
        .header("Content-Type", "text/csv; charset=utf-8")
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"relatorio.csv\"")
        .body(writer.toString());
  }

};

