package com.verifik.carteira.application.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.verifik.carteira.application.configuration.jwt.JwtTokenUtil;
import com.verifik.carteira.application.model.dto.ResponseDTO;
import com.verifik.carteira.application.model.jwt.JwtRequest;
import com.verifik.carteira.application.model.jwt.JwtResponse;
import com.verifik.carteira.application.model.security.CustomUserDetails;
import com.verifik.carteira.application.service.jwt.JwtUserDetailsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin
@Api(tags = "auth")
public class AuthController {

	@Autowired

	private AuthenticationManager authenticationManager;

	@Autowired

	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;
	

	@PostMapping(value = "authenticate")
	@ApiOperation(value = "Faz uma operação de login")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
		
		//Procura o user no banco
		final UserDetails userDetails;
		try {
			userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsuario());
		} catch (UsernameNotFoundException e) {
			ResponseDTO response = new ResponseDTO();
			response.setData(null);
			response.setError(true);
			response.setMessage("Login ou senha inválidos");

			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}
		
		//Valida se a senha do request é valida com a senha do banco
		boolean isValid = userDetailsService.matches(authenticationRequest.getSenha(), userDetails.getPassword());

		if(!isValid) {
			ResponseDTO response = new ResponseDTO();
			response.setData(null);
			response.setError(true);
			response.setMessage("Login ou senha inválidos");

			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}
		
		//Autentica de fato o usuário
		authenticate(authenticationRequest.getUsuario(), authenticationRequest.getSenha());

		//Gera a token
		final String token = jwtTokenUtil.generateToken(userDetails);
		final Date expirationDate = jwtTokenUtil.getExpirationDateFromToken(token);

		return ResponseEntity.ok(new JwtResponse(token, expirationDate, ((CustomUserDetails) userDetails).getPerfilSigla()));

	}

	private void authenticate(String username, String password) throws Exception {

		try {

			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

		} catch (DisabledException e) {

			throw new Exception("USER_DISABLED", e);

		} catch (BadCredentialsException e) {

			throw new Exception("INVALID_CREDENTIALS", e);

		}

	}

}
