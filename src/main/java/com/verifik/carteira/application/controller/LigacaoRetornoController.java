package com.verifik.carteira.application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.verifik.carteira.application.model.dto.LigacaoRetornoDTO;
import com.verifik.carteira.application.model.dto.ResponseDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.entities.LigacaoRetorno;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.model.enums.ERegra;
import com.verifik.carteira.application.model.search.LigacaoRetornoSearch;
import com.verifik.carteira.application.service.ligacaoretorno.ILigacaoRetornoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("retornos")
@Api(tags = "retornos")
public class LigacaoRetornoController {

	private static final Logger logger = LoggerFactory.getLogger(LigacaoRetornoController.class);

	@Autowired
	ILigacaoRetornoService ligacaoRetornoService;
	

	@GetMapping(path = "/search")
	@ApiOperation(value = "Realiza uma busca por atributos da ligacao retorno", responseContainer = "List")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.SEARCH_LIGACAO + "'))")
	public SearchResultDTO search(LigacaoRetornoSearch ligacaoSearch) {
		return ligacaoRetornoService.pagedSearch(ligacaoSearch);
	}

	@GetMapping(path = "/{id}")
	@ApiOperation(value = "Retorna a ligacao retorno com o id especifico")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.VIEW_LIGACAO + "'))")
	public ResponseEntity view(@PathVariable("id") long id) {

		LigacaoRetorno entity = ligacaoRetornoService.findById(id);

		// Se não encontrou Ligacao, retorna Not Found
		if (entity == null) {
			return ResponseEntity.notFound().build();
		}

		// Se encontrou, retorna OK e o parceiro
		return ResponseEntity.ok().body(ligacaoRetornoService.toDTO(entity));
	}
	
	@PutMapping(path = "/{id}")
	@ApiOperation(value = "Atualiza um retorno com id especifico. O que não for preenchido é mantido")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.UPDATE_LIGACAO_RETORNO + "') or hasPerfil('" + EPerfil.SIGLA.ADMIN + "'))")
	public ResponseEntity update(@PathVariable("id") long id, @RequestBody LigacaoRetornoDTO ligacaoDTO) {

		try {

			LigacaoRetorno ligacaoDB = ligacaoRetornoService.findById(id);

			// Se não encontrou Ligacao, retorna Not Found
			if (ligacaoDB == null) {
				return ResponseEntity.notFound().build();
			}

			ligacaoDB = ligacaoRetornoService.save(ligacaoDTO, id);

			return ResponseEntity.ok().body(ligacaoRetornoService.toDTO(ligacaoDB));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

			ResponseDTO response = new ResponseDTO();
			response.setData(null);
			response.setError(true);
			response.setMessage("Erro interno do servidor. Entre em contato com o administrador.");

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}

	}
	

}