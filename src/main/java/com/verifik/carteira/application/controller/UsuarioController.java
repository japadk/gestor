package com.verifik.carteira.application.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.verifik.carteira.application.exceptions.service.usuario.UsuarioLoginDuplicadoException;
import com.verifik.carteira.application.model.dto.ResponseDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.dto.UsuarioDTO;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.model.enums.ERegra;
import com.verifik.carteira.application.model.search.UsuarioSearch;
import com.verifik.carteira.application.service.usuario.IUsuarioService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("usuarios")
@Api(tags = {"usuario"})
public class UsuarioController {

	private static final Logger logger = LoggerFactory.getLogger(UsuarioController.class);
	
	@Autowired
	IUsuarioService usuarioService;
	
    @GetMapping(path = "/search")
    @ApiOperation(value = "Realiza uma busca por atributos do usuário", responseContainer = "List")
    @PreAuthorize("hasRegra('" + ERegra.CHAVE.SEARCH_USUARIO + "')")
    public SearchResultDTO search(UsuarioSearch usuarioSearch) {
    	return usuarioService.pagedSearch(usuarioSearch);
    }
    
    @GetMapping(path = "/{id}")
    @ApiOperation(value = "Retorna o usuário com o id especifico")
    @PreAuthorize("(hasRegra('" + ERegra.CHAVE.VIEW_USUARIO + "') and isCurrentUser(#id)) or hasPerfil('" + EPerfil.SIGLA.ADMIN + "')")
    public ResponseEntity view(@PathVariable("id") long id) {
    	
    	Usuario usuario = usuarioService.findById(id);
    	
    	//Se não encontrou User, retorna Not Found
    	if(usuario == null) {
    		return ResponseEntity.notFound().build();
    	}
    	
    	//Se encontrou, retorna OK e o usuario
    	return ResponseEntity.ok().body(usuarioService.toDTO(usuario));
    }
    
    @PutMapping(path = "/{id}")
    @ApiOperation(value = "Atualiza um usuário com id especifico. O que não for preenchido é mantido")
    @PreAuthorize("(hasRegra('" + ERegra.CHAVE.UPDATE_USUARIO + "') and isCurrentUser(#id)) or hasPerfil('" + EPerfil.SIGLA.ADMIN + "')")
    public ResponseEntity update(@PathVariable("id") long id, @RequestBody UsuarioDTO usuarioDTO) {
    	
    	try {
    		Usuario usuarioDB = usuarioService.findById(id);
    		
    		//Se não encontrou User, retorna Not Found
    		if(usuarioDB == null) {
    			return ResponseEntity.notFound().build();
    		}
    		
    		usuarioDB = usuarioService.save(usuarioDTO, id);
    		
    		return ResponseEntity.ok().body(usuarioService.toDTO(usuarioDB));
    		
    	} catch (Exception e) {
    		logger.error(e.getMessage(), e);
			
			ResponseDTO response = new ResponseDTO();
			response.setData(null);
			response.setError(true);
			response.setMessage("Erro interno do servidor. Entre em contato com o administrador.");

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		} 
    	
    }
	
	@PostMapping
	@ApiOperation(value = "Cria um usuário novo. A senha é encriptada automaticamente")
	@PreAuthorize("hasPerfil('" + EPerfil.SIGLA.ADMIN + "')")
	public ResponseEntity create(@RequestBody UsuarioDTO usuarioDTO) {

		try {
			Usuario entity = usuarioService.save(usuarioDTO);
			return ResponseEntity.ok(usuarioService.toDTO(entity));
		} catch (UsuarioLoginDuplicadoException e) {

			ResponseDTO response = new ResponseDTO();
			response.setData(null);
			response.setError(true);
			response.setMessage("Login ja foi utilizado");

			return ResponseEntity.badRequest().body(response);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			
			ResponseDTO response = new ResponseDTO();
			response.setData(null);
			response.setError(true);
			response.setMessage("Erro interno do servidor. Entre em contato com o administrador.");

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}

	}

	@GetMapping("/check/login/{login}")
	@ApiOperation(value = "Verifica se um login ja foi utilizado")
	public ResponseEntity checkLogin(@PathVariable("login") String login) {
		if(login == null || login.trim().isEmpty()) {
			return ResponseEntity.ok(false);
		}

		Usuario usuario = usuarioService.getUsuarioByLogin(login);

		if (usuario != null) {
			return ResponseEntity.ok(true);
		}

		return ResponseEntity.ok(false);

	}

}