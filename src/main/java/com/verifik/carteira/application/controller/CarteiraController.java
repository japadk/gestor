package com.verifik.carteira.application.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.verifik.carteira.application.model.dto.CarteiraDTO;
import com.verifik.carteira.application.model.dto.ResponseDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.model.enums.ERegra;
import com.verifik.carteira.application.model.search.CarteiraSearch;
import com.verifik.carteira.application.service.carteira.ICarteiraService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("carteiras")
@Api(tags = {"carteiras"})
public class CarteiraController {

	private static final Logger logger = LoggerFactory.getLogger(CarteiraController.class);
	
	@Autowired
	ICarteiraService carteiraService;
	
    
    @GetMapping(path = "/search")
    @ApiOperation(value = "Realiza uma busca por atributos da carteira", responseContainer = "List")
    @PreAuthorize("hasRegra('" + ERegra.CHAVE.SEARCH_CARTEIRA + "')")
    public SearchResultDTO search(CarteiraSearch carteiraSearch) {
    	return carteiraService.pagedSearch(carteiraSearch);
    }
    
    @GetMapping(path = "/{id}")
    @ApiOperation(value = "Retorna a carteira com o id especifico")
    @PreAuthorize("(hasRegra('" + ERegra.CHAVE.VIEW_CARTEIRA + "')) or hasPerfil('" + EPerfil.SIGLA.ADMIN + "')")
    public ResponseEntity view(@PathVariable("id") long id) {
    	
    	Carteira carteira = carteiraService.findById(id);
    	
    	//Se não encontrou Carteira, retorna Not Found
    	if(carteira == null) {
    		return ResponseEntity.notFound().build();
    	}
    	
    	//Se encontrou, retorna OK e o usuario
    	return ResponseEntity.ok().body(carteiraService.toDTO(carteira));
    }
    
    @PutMapping(path = "/{id}")
    @ApiOperation(value = "Atualiza uma carteira com id especifico. O que não for preenchido é mantido")
    @PreAuthorize("(hasRegra('" + ERegra.CHAVE.UPDATE_CARTEIRA + "')) or hasPerfil('" + EPerfil.SIGLA.ADMIN + "')")
    public ResponseEntity update(@PathVariable("id") long id, @RequestBody CarteiraDTO carteiraDTO) {
    	
    	try {
    		Carteira carteiraDB = carteiraService.findById(id);
    		
    		//Se não encontrou Carteira, retorna Not Found
    		if(carteiraDB == null) {
    			return ResponseEntity.notFound().build();
    		}
    		
    		carteiraDB = carteiraService.save(carteiraDTO, id);
    		
    		return ResponseEntity.ok().body(carteiraService.toDTO(carteiraDB));
    		
    	} catch (Exception e) {
    		logger.error(e.getMessage(), e);
			
			ResponseDTO response = new ResponseDTO();
			response.setData(null);
			response.setError(true);
			response.setMessage("Erro interno do servidor. Entre em contato com o administrador.");

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		} 
    	
    }
	
	@PostMapping
	@ApiOperation(value = "Cria uma nova carteira")
	public ResponseEntity create(@RequestBody CarteiraDTO carteiraDTO) {

		try {
			return ResponseEntity.ok(carteiraService.toDTO(carteiraService.save(carteiraDTO)));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			
			ResponseDTO response = new ResponseDTO();
			response.setData(null);
			response.setError(true);
			response.setMessage("Erro interno do servidor. Entre em contato com o administrador.");

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}

	}

}