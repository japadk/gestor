package com.verifik.carteira.application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.verifik.carteira.application.model.dto.LigacaoDTO;
import com.verifik.carteira.application.model.dto.ResponseDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.entities.Ligacao;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.model.enums.ERegra;
import com.verifik.carteira.application.model.search.LigacaoSearch;
import com.verifik.carteira.application.service.file.FileStorageService;
import com.verifik.carteira.application.service.ligacao.ILigacaoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("ligacoes")
@Api(tags = "ligacoes")
public class LigacaoController {

	private static final Logger logger = LoggerFactory.getLogger(LigacaoController.class);

	@Autowired
	ILigacaoService ligacaoService;
	
	@Autowired
	FileStorageService fileStorageService;

	@GetMapping(path = "/search")
	@ApiOperation(value = "Realiza uma busca por atributos da ligacao", responseContainer = "List")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.SEARCH_LIGACAO + "'))")
	public SearchResultDTO search(LigacaoSearch LigacaoSearch) {
		return ligacaoService.pagedSearch(LigacaoSearch);
	}

	@GetMapping(path = "/{id}")
	@ApiOperation(value = "Retorna a ligacao com o id especifico")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.VIEW_LIGACAO + "'))")
	public ResponseEntity view(@PathVariable("id") long id) {

		Ligacao entity = ligacaoService.findById(id);

		// Se não encontrou Ligacao, retorna Not Found
		if (entity == null) {
			return ResponseEntity.notFound().build();
		}

		// Se encontrou, retorna OK e o parceiro
		return ResponseEntity.ok().body(ligacaoService.toDTO(entity));
	}

	@PostMapping
	@ApiOperation(value = "Cria uma ligacao nova.")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.CREATE_LIGACAO + "'))")
	public ResponseEntity create(@RequestBody LigacaoDTO ligacaoDTO) {
		try {
			return ResponseEntity
					.ok(ligacaoService.toDTO(ligacaoService.save(ligacaoDTO)));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);

			ResponseDTO response = new ResponseDTO();
			response.setData(null);
			response.setError(true);
			response.setMessage("Erro interno do servidor. Entre em contato com o administrador.");

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
	}

	@PutMapping(path = "/{id}")
	@ApiOperation(value = "Atualiza uma ligacao com id especifico. O que não for preenchido é mantido")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.UPDATE_LIGACAO + "') or hasPerfil('" + EPerfil.SIGLA.ADMIN + "'))")
	public ResponseEntity update(@PathVariable("id") long id, @RequestBody LigacaoDTO ligacaoDTO) {

		try {

			Ligacao ligacaoDB = ligacaoService.findById(id);

			// Se não encontrou Ligacao, retorna Not Found
			if (ligacaoDB == null) {
				return ResponseEntity.notFound().build();
			}

			ligacaoDB = ligacaoService.save(ligacaoDTO, id);

			return ResponseEntity.ok().body(ligacaoService.toDTO(ligacaoDB));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

			ResponseDTO response = new ResponseDTO();
			response.setData(null);
			response.setError(true);
			response.setMessage("Erro interno do servidor. Entre em contato com o administrador.");

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}

	}
	

}