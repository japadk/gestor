package com.verifik.carteira.application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.model.search.AtividadeSearch;
import com.verifik.carteira.application.service.atividade.IAtividadeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("atividades")
@Api(tags = "atividades")
public class AtividadeController {

	private static final Logger logger = LoggerFactory.getLogger(AtividadeController.class);

	@Autowired
	IAtividadeService atividadeService;
	

	@GetMapping(path = "/search")
	@ApiOperation(value = "Realiza uma busca por atributos da atividade", responseContainer = "List")
	@PreAuthorize("hasPerfil('" + EPerfil.SIGLA.ADMIN + "')")
	public SearchResultDTO search(AtividadeSearch dividaSearch) {
		return atividadeService.pagedSearch(dividaSearch);
	}

}