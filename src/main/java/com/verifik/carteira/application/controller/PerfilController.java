package com.verifik.carteira.application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.entities.Perfil;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.model.enums.ERegra;
import com.verifik.carteira.application.model.search.PerfilSearch;
import com.verifik.carteira.application.service.perfil.IPerfilService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("perfis")
@Api(tags = "perfis")
public class PerfilController {

	private static final Logger logger = LoggerFactory.getLogger(PerfilController.class);

	@Autowired
	IPerfilService perfilService;

	@GetMapping(path = "/search")
	@ApiOperation(value = "Realiza uma busca por atributos do cliente", responseContainer = "List")
	@PreAuthorize("(hasPerfil('" + EPerfil.SIGLA.ADMIN + "'))")
	public SearchResultDTO search(PerfilSearch search) {
		return perfilService.pagedSearch(search);
	}

	@GetMapping(path = "/{id}")
	@ApiOperation(value = "Retorna o cliente com o id especifico")
	@PreAuthorize("(hasRegra('" + ERegra.CHAVE.VIEW_CLIENTE + "'))")
	public ResponseEntity view(@PathVariable("id") long id) {

		Perfil entity = perfilService.findById(id);

		// Se não encontrou, retorna Not Found
		if (entity == null) {
			return ResponseEntity.notFound().build();
		}

		// Se encontrou, retorna OK e o parceiro
		return ResponseEntity.ok().body(perfilService.toDTO(entity));
	}

}