package com.verifik.carteira.application.exceptions.service.regra;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;

public class RegraChaveDuplicadoException extends BaseServiceException {

	private static final long serialVersionUID = 1L;

	public RegraChaveDuplicadoException() {
		super("Perfil com sigla duplicada");
	}

	public RegraChaveDuplicadoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public RegraChaveDuplicadoException(String message, Throwable cause) {
		super(message, cause);
	}

	public RegraChaveDuplicadoException(String message) {
		super(message);
	}

	public RegraChaveDuplicadoException(Throwable cause) {
		super(cause);
	}

	
}
