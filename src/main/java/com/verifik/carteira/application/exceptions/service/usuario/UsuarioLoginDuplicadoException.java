package com.verifik.carteira.application.exceptions.service.usuario;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;

public class UsuarioLoginDuplicadoException extends BaseServiceException {

	private static final long serialVersionUID = 1L;

	public UsuarioLoginDuplicadoException() {
		super("Usuário com login duplicado");
	}

	public UsuarioLoginDuplicadoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UsuarioLoginDuplicadoException(String message, Throwable cause) {
		super(message, cause);
	}

	public UsuarioLoginDuplicadoException(String message) {
		super(message);
	}

	public UsuarioLoginDuplicadoException(Throwable cause) {
		super(cause);
	}

	
}
