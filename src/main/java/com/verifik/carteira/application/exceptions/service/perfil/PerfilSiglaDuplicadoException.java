package com.verifik.carteira.application.exceptions.service.perfil;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;

public class PerfilSiglaDuplicadoException extends BaseServiceException {

	private static final long serialVersionUID = 1L;

	public PerfilSiglaDuplicadoException() {
		super("Perfil com sigla duplicada");
	}

	public PerfilSiglaDuplicadoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PerfilSiglaDuplicadoException(String message, Throwable cause) {
		super(message, cause);
	}

	public PerfilSiglaDuplicadoException(String message) {
		super(message);
	}

	public PerfilSiglaDuplicadoException(Throwable cause) {
		super(cause);
	}

	
}
