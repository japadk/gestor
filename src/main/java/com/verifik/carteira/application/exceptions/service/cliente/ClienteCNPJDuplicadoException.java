package com.verifik.carteira.application.exceptions.service.cliente;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;

public class ClienteCNPJDuplicadoException extends BaseServiceException {

	private static final long serialVersionUID = 1L;

	public ClienteCNPJDuplicadoException() {
		super("CLiente com CNPJ duplicado");
	}

	public ClienteCNPJDuplicadoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ClienteCNPJDuplicadoException(String message, Throwable cause) {
		super(message, cause);
	}

	public ClienteCNPJDuplicadoException(String message) {
		super(message);
	}

	public ClienteCNPJDuplicadoException(Throwable cause) {
		super(cause);
	}

	
}
