package com.verifik.carteira.application.exceptions.base;

public class BaseServiceException extends Exception {
	private static final long serialVersionUID = 1L;

	public BaseServiceException() {
		super();
	}

	public BaseServiceException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BaseServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public BaseServiceException(String message) {
		super(message);
	}

	public BaseServiceException(Throwable cause) {
		super(cause);
	}
}
