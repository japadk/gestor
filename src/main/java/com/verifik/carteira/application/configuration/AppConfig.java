package com.verifik.carteira.application.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.Nullable;

@Configuration
public class AppConfig {

	@Value("${app.admin.user:#{null}}")
	@Nullable
	private String adminUser;

	@Value("${app.admin.password:#{null}}")
	@Nullable
	private String adminPassword;

	public String getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(String adminUser) {
		this.adminUser = adminUser;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}
	
}
