package com.verifik.carteira.application.configuration;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.model.dto.PerfilDTO;
import com.verifik.carteira.application.model.dto.RegraDTO;
import com.verifik.carteira.application.model.entities.Perfil;
import com.verifik.carteira.application.model.entities.PerfilRegra;
import com.verifik.carteira.application.model.entities.Regra;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.model.enums.ERegra;
import com.verifik.carteira.application.service.perfil.PerfilService;
import com.verifik.carteira.application.service.perfilregra.PerfilRegraService;
import com.verifik.carteira.application.service.regra.RegraService;
import com.verifik.carteira.application.service.usuario.UsuarioService;
import com.verifik.carteira.application.utils.TestUtils;

@Configuration
@Profile({"!test"})
public class StartupConfig {

	private static Logger log = LoggerFactory.getLogger(StartupConfig.class);
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private PerfilService perfilService;
	
	@Autowired
	private RegraService regraService;

	@Autowired
	private PerfilRegraService perfilRegraService;

	@Autowired
	private AppConfig appConfig;

	public void run() throws BaseServiceException {
		createPerfis();
		createRegras();
		createPerfilRegraVinculo();
		createAdminUser();
	}
	
	/**
	 * Cria os perfis de acesso automaticamente com base no enum EPerfil
	 * @throws BaseServiceException 
	 */
	private void createPerfis() throws BaseServiceException {
		for( EPerfil perfilAtual : EPerfil.values()) {
			
			String sigualAtual = perfilAtual.toString();
			
			Perfil novoPerfil = perfilService.getPerfilBySigla(sigualAtual);
			
			if(novoPerfil != null) {
				continue;
			}
			
			novoPerfil = perfilService.toEntity(PerfilDTO.builder()
						.withDescricao(sigualAtual)
						.withSigla(sigualAtual)
						.withRegistroAtivo(true)
						.build());
			
			log.debug("Gerando perfil {}", perfilAtual);
			
			perfilService.save(novoPerfil);
		}
	}
	
	/**
	 * Cria as regras automaticamente com base no enum ERegra
	 * @throws BaseServiceException 
	 */
	private void createRegras() throws BaseServiceException {
		for( ERegra regraAtual : ERegra.values()) {
			
			String descricaoAtual = regraAtual.getDescricao();
			
			String chaveAtual = regraAtual.toString();
			
			Regra novaRegra = regraService.getRegraByChave(chaveAtual);
			
			if(novaRegra != null) {
				continue;
			}
			
			novaRegra = regraService.toEntity(RegraDTO.builder()
						.withDescricao(descricaoAtual)
						.withChave(chaveAtual)
						.withRegistroAtivo(true)
						.build());
			
			log.debug("Gerando regra {}", regraAtual);
			
			regraService.save(novaRegra);
		}
	}
	
	/**
	 * Cria os vinculos de Perfil e Regra com base nos Enums EPerfil e ERegra
	 */
	private void createPerfilRegraVinculo() {
		
		Map<String, Perfil> perfilPorSigla = new HashMap<>();
		Map<String, Regra> regraPorChave = new HashMap<>();
		
		for( EPerfil perfilAtual : EPerfil.values()) {
			
			List<Regra> regraList = perfilRegraService.getRegrasPorPerfilSigla(perfilAtual.toString());
			
			Set<String> chavesDoPerfilDB = new HashSet<>(perfilAtual.getRegras().length);
			for(Regra regraDB : regraList) {
				chavesDoPerfilDB.add(regraDB.getChave());
			}
			
			for(ERegra regra : perfilAtual.getRegras()) {
				
				String chave = regra.getChave();
				
				if(chavesDoPerfilDB.contains(chave)) {
					continue;
				}
				
				Perfil perfilParaVincular = perfilPorSigla.get(perfilAtual.toString());
				if(perfilParaVincular == null) {
					perfilParaVincular = perfilService.getPerfilBySigla(perfilAtual.toString());
					perfilPorSigla.put(perfilAtual.toString(), perfilParaVincular);
				}
				
				Regra regraParaVincular = regraPorChave.get(chave);
				if(regraParaVincular == null) {
					regraParaVincular = regraService.getRegraByChave(chave);
					regraPorChave.put(chave, regraParaVincular);
				}
				
				PerfilRegra perfilRegra = new PerfilRegra();
				
				perfilRegra.setPerfil(perfilParaVincular);
				perfilRegra.setRegra(regraParaVincular);
				
				perfilRegraService.save(perfilRegra);
				
			}
			
		}
		
	}

	/**
	 * Cria um usuário admin com base nas configurações
	 * @throws BaseServiceException 
	 */
	private void createAdminUser() throws BaseServiceException {
		
		Usuario usuario = usuarioService.getUsuarioByLogin(appConfig.getAdminUser());
		Perfil perfil = perfilService.getPerfilBySigla(EPerfil.ADMIN.toString());

		if (usuario != null) {
			return;
		}

		String adminLogin = appConfig.getAdminUser();
		String adminPassword = appConfig.getAdminPassword();

		if (adminLogin == null || adminPassword == null) {
			return;
		}

		log.debug("Gerando usuario admin");

		usuario = TestUtils.buildMockUser();
		usuario.setLogin(adminLogin);
		usuario.setSenha(adminPassword);
		usuario.setNome("Admin");
		usuario.setPerfil(perfil);

		usuarioService.save(usuarioService.encodePassword(usuario));
	}

}
