package com.verifik.carteira.application.configuration;

import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.security.CustomUserDetails;

import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;

/**
 * 
 * Classe que implementa os métodos utilizados no @PreAuthorize e @PostAuthorize
 *
 */
public class WebSecurityMethodConfigCustomMethodSecurityExpressionRoot extends SecurityExpressionRoot
		implements MethodSecurityExpressionOperations {

	private Object filterObject;
	private Object returnObject;

	public WebSecurityMethodConfigCustomMethodSecurityExpressionRoot(Authentication authentication) {
		super(authentication);
	}

	@Override
	public Object getFilterObject() {
		return filterObject;
	}

	@Override
	public Object getReturnObject() {
		return returnObject;
	}

	@Override
	public Object getThis() {
		return this;
	}

	@Override
	public void setFilterObject(Object arg0) {
		this.filterObject = arg0;
	}

	@Override
	public void setReturnObject(Object arg0) {
		this.returnObject = arg0;
	}
	
	public boolean hasRegra(String regra) {
		CustomUserDetails details = ((CustomUserDetails) this.getPrincipal());
		return details.containsRegra(regra);
	}
	
	public boolean hasOneRegra(String... regras) {
		for(String regra : regras) {
			if(hasRegra(regra)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasAllRegra(String... regras) {
		for(String regra : regras) {
			if(!hasRegra(regra)) {
				return false;
			}
		}
		return true;
	}
	
	public boolean isCurrentUser(Long idUsuario) {
		Usuario user = ((CustomUserDetails) this.getPrincipal()).getUsuario();
		return user.getId().longValue() == idUsuario.longValue();
	}
	
	public boolean hasPerfil(String perfilSigla) {
		String perfil = ((CustomUserDetails) this.getPrincipal()).getPerfilSigla();
		return perfil.trim().compareTo(perfilSigla.trim()) == 0;
	}
	
	public boolean hasOnePerfil(String... siglas) {
		for(String sigla : siglas) {
			if(hasPerfil(sigla)) {
				return true;
			}
		}
		return false;
	}

}
