package com.verifik.carteira.application.service.divida;

import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.bean.CsvToBeanBuilder;
import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.dto.AtividadeAcaoDTO;
import com.verifik.carteira.application.model.dto.AtividadeDTO;
import com.verifik.carteira.application.model.dto.CarteiraDTO;
import com.verifik.carteira.application.model.dto.ClienteDTO;
import com.verifik.carteira.application.model.dto.DividaCsvDTO;
import com.verifik.carteira.application.model.dto.DividaDTO;
import com.verifik.carteira.application.model.dto.DividaTotaisDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.model.entities.Cliente;
import com.verifik.carteira.application.model.entities.Divida;
import com.verifik.carteira.application.model.entities.Perfil;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.enums.ECampo;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.model.search.DividaSearch;
import com.verifik.carteira.application.repository.CarteiraRepository;
import com.verifik.carteira.application.repository.ClienteRepository;
import com.verifik.carteira.application.repository.DividaRepository;
import com.verifik.carteira.application.service.atividade.IAtividadeService;
import com.verifik.carteira.application.service.carteira.CarteiraService;
import com.verifik.carteira.application.service.cliente.ClienteService;
import com.verifik.carteira.application.service.usuario.IUsuarioService;
import com.verifik.carteira.application.utils.CompareUtils;
import com.verifik.carteira.application.utils.SearchUtils;

@Service
public class DividaService implements IDividaService {

  @Autowired
  DividaRepository dividaRepository;

  @Autowired
  CarteiraRepository carteiraRepository;

  @Autowired
  CarteiraService carteiraService;

  @Autowired
  ClienteRepository clienteRepository;

  @Autowired
  IAtividadeService atividadeService;

  @Autowired
  ClienteService clienteService;

  @Autowired
  IUsuarioService usuarioService;

  @Override
  public List<Divida> findAll() {
    return dividaRepository.findAll();
  }

  @Override
  public Divida findById(long id) {

    Optional<Divida> entity = dividaRepository.findById(id);

    Divida d = entity.orElseGet(() -> null);
    if(d == null){
        return null;
    }

    Usuario user  = usuarioService.getCurrentUser();
    Carteira c = d.getCliente().getCarteira();
    if(c == null){
        return null;
    }

    if(!usuarioService.hasCarteira(c, user)){
        return null;
    }

    return d;

  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Divida save(Divida entity) {

    if (entity.getRegistroAtivo() == null) {
      entity.setRegistroAtivo(true);
    }

    return dividaRepository.save(entity);

  }

  @Override
  public void delete(long id) {
    dividaRepository.deleteById(id);
  }

  @Override
  public List<Divida> search(BaseSearch baseSearch) {
    return dividaRepository.findBySearch(baseSearch);
  }

  @Override
  public long count(BaseSearch baseSearch) {
    return dividaRepository.countBySearch(baseSearch);
  }

  @Override
  public SearchResultDTO pagedSearch(BaseSearch baseSearch) {
    return SearchUtils.runPagedSearch(this, baseSearch);
  }

  @Override
  public DividaDTO toDTO(Divida entity) {
    return DividaDTO.builder()
        .withId(entity.getId())
        .withDataVencimento(entity.getDataVencimento())
        .withDataPagamento(entity.getDataPagamento())
        .withValor(entity.getValor())
        .withValorRecebido(entity.getValorRecebido())
        .withObservacao(entity.getObservacao())
        .withPago(entity.isPago())
        .withIdCliente(entity.getCliente() != null ? entity.getCliente().getId() : null)
        .withCliente(entity.getCliente() != null ? clienteService.toDTO(entity.getCliente()) : new ClienteDTO())
        .withUsuarioBaixa(entity.getUsuarioBaixa() != null ? usuarioService.toDTO(entity.getUsuarioBaixa()) : null)
        .withRegistroAtivo(entity.getRegistroAtivo())
        .withUltimaAtualizacao(entity.getDataAtualizacao())
        .build();
  }

  @Override
  public List<DividaDTO> toDTO(List<Divida> entityList) {
    List<DividaDTO> result = new ArrayList<>(entityList.size());
    for (Divida entity : entityList) {
      result.add(toDTO(entity));
    }

    return result;
  }

  public Divida toEntity(DividaDTO dto) {
    return toEntity(new Divida(), dto);
  }

  public Divida toEntity(Divida entity, DividaDTO dto) {


    Usuario usuario = usuarioService.getCurrentUser();
    Perfil perfil = usuario.getPerfil();
    boolean isAdmin = perfil.getSigla().equals(EPerfil.SIGLA.ADMIN);
    boolean isSubadmin = perfil.getSigla().equals(EPerfil.SIGLA.SUBADMIN);

    if (dto.getId() != null && (entity.getId() == null || entity.getId().longValue() == 0)) {
      entity.setId(dto.getId());
    }

    // não permite editar valor se não for admin, mas se a divida for nova então permite setar o valor    
    if(isAdmin || isSubadmin || entity.isNew()) {
      entity.setValor(dto.getValor());
    }

    if (dto.getValorRecebido() != null) {
      entity.setValorRecebido(dto.getValorRecebido());
    }

    if (dto.getDataVencimento() != null && entity.getDataVencimento() == null) {
      entity.setDataVencimento(dto.getDataVencimento());
    }

    if (dto.getObservacao() != null) {
      entity.setObservacao(dto.getObservacao());
    }

    // não permite editar valor se não for admin, mas se a divida for nova então permite setar o valor
    if (isAdmin || isSubadmin | entity.isNew()) {
      if (dto.getPago() != null) {
        entity.setPago(dto.getPago());
        if (entity.isPago()) {
          entity.setDataPagamento(dto.getDataPagamento());
          if (entity.getUsuarioBaixa() == null) {
            entity.setUsuarioBaixa(usuarioService.getCurrentUser());
          }
        } else if (!entity.isPago()) {
          entity.setUsuarioBaixa(null);
          entity.setDataPagamento(null);
        }
      }
    }

    if (dto.getIdCliente() != null) {
      Cliente cliente = clienteService.findById(dto.getIdCliente());
      if (cliente != null) {
        entity.setCliente(cliente);
      }
    }

    if (dto.getRegistroAtivo() != null) {
      entity.setRegistroAtivo(dto.getRegistroAtivo());
    }

    return entity;
  }

  @Override
  public BaseSearch applyRules(BaseSearch baseSearch) {

    DividaSearch search = (DividaSearch) baseSearch;

    Usuario usuario = usuarioService.getCurrentUser();

    Perfil perfil = usuario.getPerfil();
    if (!perfil.getSigla().equals(EPerfil.SIGLA.ADMIN)) {
      search.setIdUsuarioVinculado(usuario.getId());
    }

    return baseSearch;
  }

  @Override
  @Transactional
  public void importCsv(MultipartFile file) throws IllegalStateException, IOException, BaseServiceException {

    List<DividaCsvDTO> beans = new CsvToBeanBuilder(new InputStreamReader(file.getInputStream()))
        .withType(DividaCsvDTO.class).withSeparator(';').build().parse();

    for (DividaCsvDTO dividaCsv : beans) {

      Cliente cliente = clienteRepository.findByCodigo(dividaCsv.getCodigo().trim());

      // Cria cliente se não existe
      if (cliente == null) {
        Carteira carteira = carteiraRepository.findByNome(dividaCsv.getVendedor().trim());

        // Caso carteira não existe
        if (carteira == null) {
          CarteiraDTO carteiraDTO = CarteiraDTO.builder()
              .withNome(dividaCsv.getVendedor().trim())
              .build();
          Carteira newCarteira = carteiraService.toEntity(carteiraDTO);
          carteira = carteiraService.save(newCarteira);
        }

        ClienteDTO clienteDTO = ClienteDTO.builder()
            .withCodigo(dividaCsv.getCodigo())
            .withNome(dividaCsv.getRazaoSocial())
            .withEmail(dividaCsv.getEmail())
            .withIdCarteira(carteira.getId())
            .build();
        Cliente novoCliente = clienteService.toEntity(clienteDTO);
        cliente = clienteService.save(novoCliente);
      }

      DividaDTO dividaDTO = DividaDTO.builder()
          .withValor(dividaCsv.getValor())
          .withDataVencimento(dividaCsv.getDataVencimento())
          .withIdCliente(cliente.getId())
          .withPago(false)
          .build();

      Divida divida = this.toEntity(dividaDTO);
      this.save(divida);

    }
  }

  @Override
  public DividaTotaisDTO countTotais(DividaSearch searchCount) {

    Long idCliente = searchCount.getIdCliente();
    List<Long> idCarteiraList = searchCount.getIdCarteiraList() == null ? new ArrayList<Long>()
        : searchCount.getIdCarteiraList();
    Integer vencimentoMes = searchCount.getMes();
    Integer vencimentoAno = searchCount.getAno();
    LocalDate dataPagamentoInicio = searchCount.getDataPagamentoDe();
    LocalDate dataPagamentoFim = searchCount.getDataPagamentoAte();

    LocalDate vencimentoInicio = null;
    LocalDate vencimentoFim = null;

    if ((vencimentoAno != null && vencimentoAno != 0) && (vencimentoMes != null && vencimentoMes != 0)) {
      vencimentoInicio = LocalDate.of(vencimentoAno, vencimentoMes, 1);
      vencimentoFim = LocalDate.of(vencimentoAno, vencimentoMes, 1).plusMonths(1);
    } else if (vencimentoAno != null && vencimentoAno != 0) {
      vencimentoInicio = LocalDate.of(vencimentoAno, 1, 1);
      vencimentoFim = LocalDate.of(vencimentoAno + 1, 1, 1);
    }

    BigDecimal valorFaturadoInicial = null;
    BigDecimal pagoOriginal = null;
    BigDecimal recebidos = null;
    BigDecimal devidos = null;

    if (idCarteiraList.isEmpty()) {
      Usuario usuarioLogado = usuarioService.getCurrentUser();
      List<Carteira> carteiraList = usuarioLogado.getCarteiraList();

      if (usuarioLogado.getPerfil().getSigla().equals(EPerfil.SIGLA.ADMIN)) {
        carteiraList = carteiraRepository.findAllActive();
      }

      carteiraList.forEach(carteira -> idCarteiraList.add(carteira.getId()));
    }

    valorFaturadoInicial = dividaRepository.sumValor(idCliente, idCarteiraList, vencimentoInicio, vencimentoFim,
        dataPagamentoInicio, dataPagamentoFim);
    devidos = dividaRepository.sumNaoPagos(idCliente, idCarteiraList, vencimentoInicio, vencimentoFim,
        dataPagamentoInicio, dataPagamentoFim);
    pagoOriginal = dividaRepository.sumPagos(idCliente, idCarteiraList, vencimentoInicio, vencimentoFim,
        dataPagamentoInicio, dataPagamentoFim);
    recebidos = dividaRepository.sumPagosRecebidos(idCliente, idCarteiraList, vencimentoInicio, vencimentoFim,
        dataPagamentoInicio, dataPagamentoFim);

    return DividaTotaisDTO.builder()
        .withDevidos(devidos == null ? BigDecimal.ZERO : devidos)
        .withPagosOriginal(pagoOriginal == null ? BigDecimal.ZERO : pagoOriginal)
        .withRecebidos(recebidos == null ? BigDecimal.ZERO : recebidos)
        .withValorFaturadoInicial(valorFaturadoInicial == null ? BigDecimal.ZERO : valorFaturadoInicial)
        .build();
  }

  @Override
  public AtividadeDTO createAtividade(DividaDTO newEntity, DividaDTO oldEntity, boolean novo) {
    AtividadeDTO atividade = atividadeService.createAtividadeDTO(Divida.class.getSimpleName(), newEntity.getId(), novo);

    List<AtividadeAcaoDTO> acaoList = new ArrayList<>();

    if (CompareUtils.changed(newEntity.getCliente().getNome(), oldEntity.getCliente().getNome())) {
      acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getCliente().getNome(),
          oldEntity.getCliente().getNome(), ECampo.DIVIDA_CLIENTE.name()));
    }

    if (CompareUtils.changed(newEntity.getValor(), oldEntity.getValor())) {
      acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getValor(), oldEntity.getValor(),
          ECampo.DIVIDA_VALOR.name()));
    }

    if (CompareUtils.changed(newEntity.getValorRecebido(), oldEntity.getValorRecebido())) {
      acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getValorRecebido(), oldEntity.getValorRecebido(),
          ECampo.DIVIDA_VALOR_RECEBIDO.name()));
    }

    if (CompareUtils.changed(newEntity.getDataVencimento(), oldEntity.getDataVencimento())) {
      acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getDataVencimento(), oldEntity.getDataVencimento(),
          ECampo.DIVIDA_DATA_VENCIMENTO.name()));
    }

    if (CompareUtils.changed(newEntity.getDataVencimento(), oldEntity.getDataVencimento())) {
      acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getDataPagamento(), oldEntity.getDataPagamento(),
          ECampo.DIVIDA_DATA_PAGAMENTO.name()));
    }

    if (CompareUtils.changed(newEntity.getObservacao(), oldEntity.getObservacao())) {
      acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getObservacao(), oldEntity.getObservacao(),
          ECampo.DIVIDA_OBSERVACAO.name()));
    }

    if (CompareUtils.changed(newEntity.getPago(), oldEntity.getPago())) {
      acaoList.add(
          atividadeService.createAtividadeAcaoDTO(newEntity.getPago(), oldEntity.getPago(), ECampo.DIVIDA_PAGO.name()));
    }

    atividade.setAtividadeAcaoList(acaoList);

    return atividade;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Divida save(DividaDTO dto, Long id) throws BaseServiceException {
    Divida entity = id != null ? findById(id) : new Divida();

    DividaDTO oldDTO = toDTO(entity);

    entity = toEntity(entity, dto);

    Divida novo = save(entity);
    AtividadeDTO atividadeDTO = createAtividade(toDTO(novo), oldDTO, id == null);
    atividadeService.save(atividadeDTO);
    return novo;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Divida save(DividaDTO dto) throws BaseServiceException {
    return save(dto, null);
  }

  @Override
  public List<Divida> findDividasPagasPorPeriodo(LocalDate dataInicio, LocalDate dataFim) {
    return dividaRepository.findDividasPagasPorPeriodo(dataInicio, dataFim);
  }

  @Override
  public List<Divida> findDividasNaoPagasPorPeriodo(LocalDate dataInicio, LocalDate dataFim) {
    return dividaRepository.findDividasNaoPagasPorPeriodo(dataInicio, dataFim);
  }

  @Override
  public List<Divida> findDividasPorPeriodo(LocalDate dataInicio, LocalDate dataFim) {
    return dividaRepository.findDividasPorPeriodo(dataInicio, dataFim);
  }

}
