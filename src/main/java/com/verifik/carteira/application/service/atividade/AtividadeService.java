package com.verifik.carteira.application.service.atividade;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.dto.AtividadeAcaoDTO;
import com.verifik.carteira.application.model.dto.AtividadeDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.entities.Atividade;
import com.verifik.carteira.application.model.entities.AtividadeAcao;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.enums.EAtividadeEntidade;
import com.verifik.carteira.application.model.enums.ECampo;
import com.verifik.carteira.application.model.enums.ETipoAtividade;
import com.verifik.carteira.application.repository.AtividadeAcaoRepository;
import com.verifik.carteira.application.repository.AtividadeRepository;
import com.verifik.carteira.application.service.usuario.IUsuarioService;
import com.verifik.carteira.application.utils.SearchUtils;
import com.verifik.carteira.application.utils.StringUtils;

@Service
public class AtividadeService implements IAtividadeService {

	@Autowired 
	AtividadeRepository atividadeRepository;
	
	@Autowired 
	AtividadeAcaoRepository atividadeAcaoRepository;
	
	@Autowired
	IUsuarioService usuarioService;
		
	@Override
	public List<Atividade> findAll() {
		return atividadeRepository.findAll();
	}
	
	@Override
	public Atividade findById(long id) {
		
		Optional<Atividade> entity = atividadeRepository.findById(id);
		
		return entity.orElseGet(() -> null);
		
	}
	
	@Override
	public Atividade save(AtividadeDTO dto) throws BaseServiceException {
		if(dto.getAtividadeAcaoList() == null || dto.getAtividadeAcaoList().isEmpty()) {
			return new Atividade();
		}
		return save(toEntity(dto));
	}
	
	@Override
	public Atividade save(Atividade entity) throws BaseServiceException {
				
		if(entity.getRegistroAtivo() == null) {
			entity.setRegistroAtivo(true);
		}
		
		for(AtividadeAcao acao : entity.getAtividadeAcaoList()) {
			acao.setRegistroAtivo(true);			
		}
		
		return atividadeRepository.save(entity);
		
	}
	
	@Override
	public void delete(long id) {
		atividadeRepository.deleteById(id);
	}
	
	@Override
	public BaseSearch applyRules(BaseSearch baseSearch) {
		return baseSearch;
	}

	@Override
	public List<Atividade> search(BaseSearch baseSearch) {
		return atividadeRepository.findBySearch(baseSearch);
	}
	
	@Override
	public long count(BaseSearch baseSearch) {
		return atividadeRepository.countBySearch(baseSearch);
	}
	
	@Override
	public SearchResultDTO pagedSearch(BaseSearch baseSearch) {
		return SearchUtils.runPagedSearch(this,baseSearch);
	}
	
	@Override
	public AtividadeDTO toDTO(Atividade entity) { 
		
		return AtividadeDTO.builder()
				.withId(entity.getId())
				.withEntidade(entity.getEntidade())
				.withIdEntidade(entity.getId())
				.withTipo(entity.getTipo())
				.withUsuario(usuarioService.toDTO(entity.getUsuario()))
				.withMensagem(EAtividadeEntidade.getMensagem(entity.getUsuario().getNome(), entity.getTipo(), entity.getEntidade()))
				.withUpdateLink(EAtividadeEntidade.getUpdateLink(entity.getEntidade(), entity.getIdEntidade()))
				.withDataCriacao(entity.getDataCriacao())
				.withAtividadeAcaoList(toAcaoDTO(entity.getAtividadeAcaoList()))
				.build();
		
	}
	
	@Override
	public AtividadeAcaoDTO toAcaoDTO(AtividadeAcao entity) { 
		
		return AtividadeAcaoDTO.builder()
				.withCampo(entity.getCampo())
				.withId(entity.getId())
				.withValorAnterior(entity.getValorAnterior())
				.withValorNovo(entity.getValorNovo())
				.withMensagem(ECampo.getMensagem(entity.getCampo(), entity.getValorAnterior(), entity.getValorNovo()))
				.build();
		
	}
	
	@Override
	public List<AtividadeAcaoDTO> toAcaoDTO(List<AtividadeAcao> entityList) {
		List<AtividadeAcaoDTO> result = new ArrayList<>(entityList.size());
		for(AtividadeAcao atividade : entityList) {
			result.add(toAcaoDTO(atividade));
		}
		
		return result;
	}
	
	@Override
	public List<AtividadeDTO> toDTO(List<Atividade> entityList) {
		List<AtividadeDTO> result = new ArrayList<>(entityList.size());
		for(Atividade atividade : entityList) {
			result.add(toDTO(atividade));
		}
		
		return result;
	}

	@Override
	public Atividade toEntity(AtividadeDTO dto) {
		return toEntity(new Atividade(), dto);
	}
	
	@Override
	public Atividade toEntity(Atividade entity, AtividadeDTO dto) {
				
		entity.setIdEntidade(dto.getIdEntidade());
		entity.setEntidade(dto.getEntidade());
		entity.setTipo(dto.getTipo());
		
		Usuario usuario = usuarioService.findById(dto.getUsuario().getId());
		
		entity.setUsuario(usuario);
		
		List<AtividadeAcao> listAcao = new ArrayList<>(dto.getAtividadeAcaoList().size());
		for(AtividadeAcaoDTO acaoDTO : dto.getAtividadeAcaoList()) {
			AtividadeAcao acao = new AtividadeAcao();
			acao.setCampo(acaoDTO.getCampo());
			acao.setValorAnterior(acaoDTO.getValorAnterior());
			acao.setValorNovo(acaoDTO.getValorNovo());
			acao.setAtividade(entity);
			listAcao.add(acao);
		}
		
		entity.setAtividadeAcaoList(listAcao);
		
		return entity;
	}
	
	@Override
	public AtividadeDTO createAtividadeDTO(String entity, Long id, boolean isNew) {
		return AtividadeDTO.builder()
				.withEntidade(EAtividadeEntidade.valueOf(entity.toUpperCase()).name())
				.withTipo(isNew ? ETipoAtividade.CREATE.name() : ETipoAtividade.UPDATE.name())
				.withIdEntidade(id)
				.withUsuario(usuarioService.toDTO(usuarioService.getCurrentUser()))
				.build();
	}
	
	@Override
	public AtividadeAcaoDTO createAtividadeAcaoDTO(Object valorNovo, Object valorAnterior, String campo) {
		return AtividadeAcaoDTO.builder()
				.withCampo(campo)
				.withValorNovo(StringUtils.toString(valorNovo))
				.withValorAnterior(StringUtils.toString(valorAnterior))
				.build();
	}

}
