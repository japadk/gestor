package com.verifik.carteira.application.service.usuariocarteira;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.dto.UsuarioCarteiraDTO;
import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.entities.UsuarioCarteira;
import com.verifik.carteira.application.repository.CarteiraRepository;
import com.verifik.carteira.application.repository.UsuarioCarteiraRepository;
import com.verifik.carteira.application.repository.UsuarioRepository;
import com.verifik.carteira.application.service.carteira.CarteiraService;
import com.verifik.carteira.application.service.usuario.UsuarioService;
import com.verifik.carteira.application.utils.SearchUtils;

@Service
public class UsuarioCarteiraService implements IUsuarioCarteiraService {

	@Autowired 
	UsuarioCarteiraRepository usuarioCarteiraRepository;
	
	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	CarteiraService carteiraService;
	
	@Autowired
	CarteiraRepository carteiraRepository;
	
	@Override
	public List<UsuarioCarteira> findAll() {
		return usuarioCarteiraRepository.findAll();
	}
	
	@Override
	public UsuarioCarteira findById(long id) {
		
		Optional<UsuarioCarteira> entity = usuarioCarteiraRepository.findById(id);
		
		return entity.orElseGet(() -> null);
		
	}
	
	@Override
	public UsuarioCarteira save(UsuarioCarteira entity) {
		
		if(entity.getRegistroAtivo() == null) {
			entity.setRegistroAtivo(true);
		}
		
		
		return usuarioCarteiraRepository.save(entity);
		
	}
	
	@Override
	public void delete(long id) {
		usuarioCarteiraRepository.deleteById(id);
	}

	@Override
	public List<UsuarioCarteira> search(BaseSearch baseSearch) {
		return usuarioCarteiraRepository.findBySearch(baseSearch);
	}
	
	@Override
	public long count(BaseSearch baseSearch) {
		return usuarioCarteiraRepository.countBySearch(baseSearch);
	}
	
	@Override
	public SearchResultDTO pagedSearch(BaseSearch baseSearch) {
		return SearchUtils.runPagedSearch(this, baseSearch);
	}

	@Override
	public UsuarioCarteiraDTO toDTO(UsuarioCarteira entity) {
		return UsuarioCarteiraDTO.builder()
			.withId(entity.getId())
			.withUsuario(usuarioService.toDTO(entity.getUsuario()))
			.withCarteira(carteiraService.toDTO(entity.getCarteira()))
			.withRegistroAtivo(entity.getRegistroAtivo())
			.build();
	}

	@Override
	public List<UsuarioCarteiraDTO> toDTO(List<UsuarioCarteira> entityList) {
		List<UsuarioCarteiraDTO> result = new ArrayList<>(entityList.size());
		for(UsuarioCarteira entity : entityList) {
			result.add(toDTO(entity));
		}
		
		return result;
	}

	@Override
	public UsuarioCarteira toEntity(UsuarioCarteiraDTO dto) {
		return toEntity(new UsuarioCarteira(), dto);
	}
	
	@Override
	public UsuarioCarteira toEntity(UsuarioCarteira entity, UsuarioCarteiraDTO dto) {
		
		throw new NotImplementedException("Ainda não implementado");
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateUsuarioCarteira(Usuario entity, List<Long> idCarteiras) {
		
		List<Carteira> carteiras = usuarioCarteiraRepository.findCarteirasByIdUsuario(entity.getId());
		
		Set<Long> setCarteiras = new HashSet<>(idCarteiras);
		
		List<Carteira> toRemove = new ArrayList<>();
		
		//Filtra as carteiras que precisam ser removidas
		for(Carteira carteira : carteiras) {
			if(!setCarteiras.contains(carteira.getId())) {
				toRemove.add(carteira);
			} else {
				setCarteiras.remove(carteira.getId());
			}
		}
		
		Iterable<Carteira> toAdd = carteiraRepository.findAllById(setCarteiras);
		
		for(Carteira carteira: toRemove) {
			UsuarioCarteira usuarioCarteira = usuarioCarteiraRepository.findByIdCateiraAndIdUsuario(carteira.getId(), entity.getId());
			if(usuarioCarteira != null) {
				usuarioCarteiraRepository.delete(usuarioCarteira);
			}
		}
		
		for(Carteira carteira : toAdd) {
			UsuarioCarteira usuarioCarteira = new UsuarioCarteira();
			
			usuarioCarteira.setUsuario(entity);
			usuarioCarteira.setCarteira(carteira);
			usuarioCarteira.setRegistroAtivo(true);
			
			usuarioCarteiraRepository.save(usuarioCarteira);
		}
		
		usuarioRepository.refresh(entity);
		
		
	}

	@Override
	public BaseSearch applyRules(BaseSearch baseSearch) {
		return baseSearch;
	}

}
