package com.verifik.carteira.application.service.usuario;

import java.util.List;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.model.dto.UsuarioDTO;
import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.service.IDefaultAtividadeService;

public interface IUsuarioService extends IDefaultAtividadeService<Usuario, UsuarioDTO> {

	public Usuario getUsuarioByLogin(String login);
	
	public Usuario encodePassword(Usuario usuario);

	UsuarioDTO toDTO(Usuario entity, boolean hidePassword);

	List<UsuarioDTO> toDTO(List<Usuario> entityList, boolean hidePassword);
	
	public Usuario createUsuario(UsuarioDTO dto) throws BaseServiceException;
	
	public Usuario saveWithCarteira(Usuario entity, UsuarioDTO dto) throws BaseServiceException;

	Usuario getCurrentUser();

    public boolean hasCarteira(Carteira carteira, Usuario usuario);
}
