package com.verifik.carteira.application.service.ligacaoretorno;

import com.verifik.carteira.application.model.dto.LigacaoRetornoDTO;
import com.verifik.carteira.application.model.entities.LigacaoRetorno;
import com.verifik.carteira.application.service.IDefaultAtividadeService;

public interface ILigacaoRetornoService extends IDefaultAtividadeService<LigacaoRetorno, LigacaoRetornoDTO> {

}
