package com.verifik.carteira.application.service.file;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.verifik.carteira.application.exceptions.FileStorageException;
import com.verifik.carteira.application.exceptions.MyFileNotFoundException;
import com.verifik.carteira.application.model.dto.FileDTO;
import com.verifik.carteira.application.utils.HashUtils;

@Service
public class FileStorageService {

    private final Path fileStorageLocation;

    @Autowired
    public FileStorageService() {
        this.fileStorageLocation = Paths.get(System.getProperty("java.io.tmpdir"))
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }
    
    public FileDTO toFileDTO(MultipartFile file) throws IOException {
    	
    	String fileName = StringUtils.cleanPath(file.getOriginalFilename());
    	byte[] bytes = file.getBytes();
    	return FileDTO.builder()
    			.withData(bytes)
    			.withFileName(fileName)
    			.withExtention(getExtention(fileName))
    			.withHash(HashUtils.md5(bytes, String.valueOf(System.currentTimeMillis())))
    			.withMimeType(getMimeType(bytes))
    			.build();
    }
    
    public String getMimeType(byte[] data) throws IOException {
    	InputStream is = new BufferedInputStream(new ByteArrayInputStream(data));
    	return URLConnection.guessContentTypeFromStream(is);
    }
    
    public String getExtention(String fileName) {
    
		String[] split = fileName.split(".");
		if(split.length == 0) {
			return "";
		}
		
		return split[split.length-1];
    	
    }
    

    public String storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }
            
            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public UrlResource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            UrlResource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }
}