package com.verifik.carteira.application.service.relatorio;

import com.verifik.carteira.application.model.dto.*;

import java.util.List;

public interface IRelatorioService {

    List<RelatorioAtividadeDividaCsvDTO> buscaRelatorioAtividadeDividaDTO(AtividadeDividaRelatorioParametroDTO search);

    List<RelatorioDividasPagasCsvDTO> buscaRelatorioDividaPagasDTO(DividasPagasRelatorioParametroDTO dto);

    List<RelatorioDividasNaoPagasCsvDTO> buscaRelatorioDividaNaoPagasDTO(DividasNaoPagasRelatorioParametroDTO dto);

    List<RelatorioDividasCsvDTO> buscaRelatorioDividaDTO(DividasRelatorioParametroDTO dto);

    List<RelatorioLigacoesCsvDTO> buscaRelatorioLigacoesDTO(LigacoesRelatorioParametroDTO dto);

    List<RelatorioClienteCsvDTO> buscaClientesAtivos();
}
