package com.verifik.carteira.application.service.carteira;

import com.verifik.carteira.application.model.dto.CarteiraDTO;
import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.service.IDefaultAtividadeService;

public interface ICarteiraService extends IDefaultAtividadeService<Carteira, CarteiraDTO> {
}
