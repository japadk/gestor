package com.verifik.carteira.application.service.perfil;

import com.verifik.carteira.application.model.dto.PerfilDTO;
import com.verifik.carteira.application.model.entities.Perfil;
import com.verifik.carteira.application.service.IDefaultService;

public interface IPerfilService extends IDefaultService<Perfil, PerfilDTO> {
	
	public Perfil getPerfilBySigla(String sigla);

}
