package com.verifik.carteira.application.service.usuariocarteira;

import java.util.List;

import com.verifik.carteira.application.model.dto.UsuarioCarteiraDTO;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.entities.UsuarioCarteira;
import com.verifik.carteira.application.service.IDefaultService;

public interface IUsuarioCarteiraService extends IDefaultService<UsuarioCarteira, UsuarioCarteiraDTO> {
	
	void updateUsuarioCarteira(Usuario usuario, List<Long> idCarteiras);

}
