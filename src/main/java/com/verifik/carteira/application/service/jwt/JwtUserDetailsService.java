package com.verifik.carteira.application.service.jwt;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.verifik.carteira.application.model.entities.Regra;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.security.CustomUserDetails;
import com.verifik.carteira.application.service.usuario.IUsuarioService;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private IUsuarioService userService;

	@Autowired
	@Lazy
	private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	Usuario usuario = userService.getUsuarioByLogin(username);
    	
    	if (usuario == null || !usuario.getRegistroAtivo()) {
            throw new UsernameNotFoundException(username);
        }

		List<Regra> regraList = usuario.getPerfil().getRegraList();
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(regraList.size());
		Set<String> regras = new HashSet<>(regraList.size());
		for (Regra regra : regraList) {
            authorities.add(new SimpleGrantedAuthority(regra.getChave()));
            regras.add(regra.getChave());
        }
    	
        return new CustomUserDetails(usuario, usuario.getPerfil().getSigla(),  authorities, regras);
    }
    
    public boolean matches(String rawPassword, String dbPassword) {
    	return passwordEncoder.matches(rawPassword, dbPassword);
    }
}
