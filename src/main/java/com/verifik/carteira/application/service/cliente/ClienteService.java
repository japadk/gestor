package com.verifik.carteira.application.service.cliente;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.dto.AtividadeAcaoDTO;
import com.verifik.carteira.application.model.dto.AtividadeDTO;
import com.verifik.carteira.application.model.dto.ClienteDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.model.entities.Cliente;
import com.verifik.carteira.application.model.entities.Perfil;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.enums.ECampo;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.model.search.ClienteSearch;
import com.verifik.carteira.application.repository.CarteiraRepository;
import com.verifik.carteira.application.repository.ClienteRepository;
import com.verifik.carteira.application.service.atividade.IAtividadeService;
import com.verifik.carteira.application.service.usuario.IUsuarioService;
import com.verifik.carteira.application.utils.CompareUtils;
import com.verifik.carteira.application.utils.SearchUtils;

@Service
public class ClienteService implements IClienteService {

	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	CarteiraRepository carteiraRepository;
	
	@Autowired
	IUsuarioService usuarioService;
	
	@Autowired
	IAtividadeService atividadeService;

	@Override
	public List<Cliente> findAll() {
		return clienteRepository.findAll();
	}

	@Override
	public Cliente findById(long id) {

		Optional<Cliente> cliente = clienteRepository.findById(id);

		return cliente.orElseGet(() -> null);

	}
	
	@Override
	public Cliente findByNome(String nome) {

		return clienteRepository.findByNome(nome);

	}
			
			

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Cliente save(Cliente entity) {
		
		if (entity.getRegistroAtivo() == null) {
			entity.setRegistroAtivo(true);
		}

		return clienteRepository.save(entity);

	}

	@Override
	public void delete(long id) {
		clienteRepository.deleteById(id);
	}

	@Override
	public List<Cliente> search(BaseSearch baseSearch) {
		return clienteRepository.findBySearch(baseSearch);
	}
	
	@Override
	public long count(BaseSearch baseSearch) {
		return clienteRepository.countBySearch(baseSearch);
	}
	
	@Override
	public SearchResultDTO pagedSearch(BaseSearch baseSearch) {
		return SearchUtils.runPagedSearch(this, baseSearch);
	}

	@Override
	public ClienteDTO toDTO(Cliente entity) {
		return ClienteDTO.builder()
				.withId(entity.getId())
				.withNome(entity.getNome())
				.withNomeFantasia(entity.getNomeFantasia())
				.withRegistroAtivo(entity.getRegistroAtivo())
				.withCnpj(entity.getCnpj())
				.withCodigo(entity.getCodigo())
				.withContato(entity.getContato())
				.withEmail(entity.getEmail())
				.withTelefone(entity.getTelefone())
				.withIdCarteira(entity.getCarteira() != null ? entity.getCarteira().getId() : null)
				.withNomeCarteira(entity.getCarteira() != null ? entity.getCarteira().getNome() : null)
				.build();
	}

	@Override
	public List<ClienteDTO> toDTO(List<Cliente> entityList) {
		List<ClienteDTO> result = new ArrayList<>(entityList.size());
		for (Cliente entity : entityList) {
			result.add(toDTO(entity));
		}

		return result;
	}

	public Cliente toEntity(ClienteDTO dto) {
		return toEntity(new Cliente(), dto);
	}

	public Cliente toEntity(Cliente entity, ClienteDTO dto) {

		if (dto.getId() != null && (entity.getId() == null || entity.getId().longValue() == 0)) {
			entity.setId(dto.getId());
		}

		if (dto.getNome() != null) {
			entity.setNome(dto.getNome().trim());
		}

		if (dto.getNomeFantasia() != null) {
			entity.setNomeFantasia(dto.getNomeFantasia().trim());
		}

		if (dto.getCnpj() != null) {
			entity.setCnpj(dto.getCnpj().trim());
		}
		
		if(dto.getCodigo() != null) {
			entity.setCodigo(dto.getCodigo().trim());
		}
		
		if(dto.getContato() != null) {
			entity.setContato(dto.getContato().trim());
		}
		
		if(dto.getTelefone() != null) {
			entity.setTelefone(dto.getTelefone().trim());
		}
		
		if(dto.getEmail() != null) {
			entity.setEmail(dto.getEmail().trim());
		}
		
		if(dto.getIdCarteira() != null) {
			Optional<Carteira> carteira = carteiraRepository.findById(dto.getIdCarteira());
			if(carteira.isPresent()) {
				entity.setCarteira(carteira.get());
			}
		}

		if (dto.getRegistroAtivo() != null) {
			entity.setRegistroAtivo(dto.getRegistroAtivo());
		}

		return entity;
	}
	
	@Override
	public BaseSearch applyRules(BaseSearch baseSearch) {
		
		ClienteSearch search = (ClienteSearch) baseSearch;
		
		Usuario usuario = usuarioService.getCurrentUser();
		
		Perfil perfil = usuario.getPerfil();
		if(!perfil.getSigla().equals(EPerfil.SIGLA.ADMIN)) {
			search.setIdUsuarioVinculado(usuario.getId());
		}
		
		return baseSearch;
	}

	@Override
	public AtividadeDTO createAtividade(ClienteDTO newEntity, ClienteDTO oldEntity, boolean novo) {
		AtividadeDTO atividade = atividadeService.createAtividadeDTO(Cliente.class.getSimpleName(), newEntity.getId(), novo);
		
		List<AtividadeAcaoDTO> acaoList = new ArrayList<>();
						
		if(CompareUtils.changed(newEntity.getNome(), oldEntity.getNome())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getNome(), oldEntity.getNome(), ECampo.CLIENTE_NOME.name()));
		}
		
		if(CompareUtils.changed(newEntity.getNomeFantasia(), oldEntity.getNomeFantasia())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getNomeFantasia(), oldEntity.getNomeFantasia(), ECampo.CLIENTE_NOME_FANTASIA.name()));
		}
		
		if(CompareUtils.changed(newEntity.getCodigo(), oldEntity.getCodigo())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getCodigo(), oldEntity.getCodigo(), ECampo.CLIENTE_CODIGO.name()));
		}
		
		if(CompareUtils.changed(newEntity.getCnpj(), oldEntity.getCnpj())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getCnpj(), oldEntity.getCnpj(), ECampo.CLIENTE_CNPJ.name()));
		}
		
		if(CompareUtils.changed(newEntity.getContato(), oldEntity.getContato())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getContato(), oldEntity.getContato(), ECampo.CLIENTE_CONTATO.name()));
		}
		
		if(CompareUtils.changed(newEntity.getTelefone(), oldEntity.getTelefone())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getTelefone(), oldEntity.getTelefone(), ECampo.CLIENTE_TELEFONE.name()));
		}
		
		if(CompareUtils.changed(newEntity.getEmail(), oldEntity.getEmail())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getEmail(), oldEntity.getEmail(), ECampo.CLIENTE_EMAIL.name()));
		}
		
		if(CompareUtils.changed(newEntity.getNomeCarteira(), oldEntity.getNomeCarteira())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getNomeCarteira(), oldEntity.getNomeCarteira(), ECampo.CLIENTE_CARTEIRA.name()));
		}
		
		if(CompareUtils.changed(newEntity.getRegistroAtivo(), oldEntity.getRegistroAtivo())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getRegistroAtivo(), oldEntity.getRegistroAtivo(), ECampo.REGISTRO_ATIVO.name()));
		}
		
		atividade.setAtividadeAcaoList(acaoList);
		
		return atividade;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Cliente save(ClienteDTO dto, Long id) throws BaseServiceException {
		Cliente entity = id != null ? findById(id) : new Cliente();
		
		ClienteDTO oldDTO = toDTO(entity);
		
		entity = toEntity(entity, dto);
						
		Cliente novo = save(entity);
		AtividadeDTO atividadeDTO = createAtividade(toDTO(novo), oldDTO, id == null);
		atividadeService.save(atividadeDTO);
		return novo;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Cliente save(ClienteDTO dto) throws BaseServiceException {
		return save(dto, null);
	}

	@Override
	public List<Cliente> findAllActive(){
		return clienteRepository.findAllActive();
	}

}
