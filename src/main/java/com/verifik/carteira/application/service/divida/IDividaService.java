package com.verifik.carteira.application.service.divida;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.model.dto.DividaDTO;
import com.verifik.carteira.application.model.dto.DividaTotaisDTO;
import com.verifik.carteira.application.model.entities.Divida;
import com.verifik.carteira.application.model.search.DividaSearch;
import com.verifik.carteira.application.service.IDefaultAtividadeService;

public interface IDividaService extends IDefaultAtividadeService<Divida, DividaDTO> {

  void importCsv(MultipartFile file) throws IllegalStateException, IOException, BaseServiceException;

  DividaTotaisDTO countTotais(DividaSearch searchCount);

  List<Divida> findDividasPagasPorPeriodo(LocalDate dataInicio, LocalDate dataFim);

  List<Divida> findDividasNaoPagasPorPeriodo(LocalDate dataInicio, LocalDate dataFim);

  public List<Divida> findDividasPorPeriodo(LocalDate dataInicio, LocalDate dataFim);

}
