package com.verifik.carteira.application.service.atividade;

import java.util.List;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.model.dto.AtividadeAcaoDTO;
import com.verifik.carteira.application.model.dto.AtividadeDTO;
import com.verifik.carteira.application.model.entities.Atividade;
import com.verifik.carteira.application.model.entities.AtividadeAcao;
import com.verifik.carteira.application.service.IDefaultService;

public interface IAtividadeService extends IDefaultService<Atividade, AtividadeDTO> {

	AtividadeAcaoDTO createAtividadeAcaoDTO(Object valorAnterior, Object valorNovo, String campo);

	AtividadeDTO createAtividadeDTO(String entity, Long id, boolean isNew);

	Atividade save(AtividadeDTO dto) throws BaseServiceException;

	List<AtividadeAcaoDTO> toAcaoDTO(List<AtividadeAcao> entityList);

	AtividadeAcaoDTO toAcaoDTO(AtividadeAcao entity);
}
