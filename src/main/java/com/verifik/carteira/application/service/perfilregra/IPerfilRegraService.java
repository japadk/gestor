package com.verifik.carteira.application.service.perfilregra;

import java.util.List;

import com.verifik.carteira.application.model.dto.PerfilRegraDTO;
import com.verifik.carteira.application.model.entities.PerfilRegra;
import com.verifik.carteira.application.model.entities.Regra;
import com.verifik.carteira.application.service.IDefaultService;

public interface IPerfilRegraService extends IDefaultService<PerfilRegra, PerfilRegraDTO> {
	
	List<Regra> getRegrasPorPerfilSigla(String sigla);

}
