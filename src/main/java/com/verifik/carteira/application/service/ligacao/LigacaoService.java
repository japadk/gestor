package com.verifik.carteira.application.service.ligacao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.verifik.carteira.application.model.entities.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.dto.AtividadeAcaoDTO;
import com.verifik.carteira.application.model.dto.AtividadeDTO;
import com.verifik.carteira.application.model.dto.LigacaoDTO;
import com.verifik.carteira.application.model.dto.LigacaoRetornoDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.enums.ECampo;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.model.search.LigacaoSearch;
import com.verifik.carteira.application.repository.CarteiraRepository;
import com.verifik.carteira.application.repository.ClienteRepository;
import com.verifik.carteira.application.repository.LigacaoRepository;
import com.verifik.carteira.application.repository.LigacaoRetornoRepository;
import com.verifik.carteira.application.service.atividade.IAtividadeService;
import com.verifik.carteira.application.service.carteira.CarteiraService;
import com.verifik.carteira.application.service.cliente.ClienteService;
import com.verifik.carteira.application.service.ligacaoretorno.LigacaoRetornoService;
import com.verifik.carteira.application.service.usuario.IUsuarioService;
import com.verifik.carteira.application.utils.CompareUtils;
import com.verifik.carteira.application.utils.SearchUtils;

@Service
public class LigacaoService implements ILigacaoService {

	@Autowired
	LigacaoRepository ligacaoRepository;
	
	@Autowired
	CarteiraRepository carteiraRepository;
	
	@Autowired
	LigacaoRetornoRepository ligacaoRetornoRepository;
	
	@Autowired
	CarteiraService carteiraService;
	
	@Autowired
	ClienteRepository clienteRepository;
	
	@Autowired
	IAtividadeService atividadeService;
		
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	IUsuarioService usuarioService;
	
	@Autowired
	LigacaoRetornoService ligacaoRetornoService;

	@Override
	public List<Ligacao> findAll() {
		return ligacaoRepository.findAll();
	}

	@Override
	public Ligacao findById(long id) {

		Optional<Ligacao> entity = ligacaoRepository.findById(id);

		return entity.orElseGet(() -> null);

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Ligacao save(Ligacao entity) {
		
		if (entity.getRegistroAtivo() == null) {
			entity.setRegistroAtivo(true);
		}

		return ligacaoRepository.save(entity);

	}

	@Override
	public void delete(long id) {
		ligacaoRepository.deleteById(id);
	}

	@Override
	public List<Ligacao> search(BaseSearch baseSearch) {
		return ligacaoRepository.findBySearch(baseSearch);
	}
	
	@Override
	public long count(BaseSearch baseSearch) {
		return ligacaoRepository.countBySearch(baseSearch);
	}
	
	@Override
	public SearchResultDTO pagedSearch(BaseSearch baseSearch) {
		return SearchUtils.runPagedSearch(this, baseSearch);
	}
	
	@Override
	public LigacaoDTO toDTO(Ligacao entity) {
		return toDTO(entity, false);
	}

	@Override
	public List<LigacaoDTO> toDTO(List<Ligacao> entityList) {
		return toDTO(entityList, false);
	}

	@Override
	public LigacaoDTO toDTO(Ligacao entity, boolean comRetorno) {
		LigacaoDTO.Builder builder = LigacaoDTO.builder()
				.withId(entity.getId())
				.withNome(entity.getNome())
				.withCnpj(entity.getCnpj())
				.withTelefone(entity.getTelefone())
				.withObservacao(entity.getObservacao())
				.withIdUsuario(entity.getUsuario() != null ? entity.getUsuario().getId() : null)
				.withIdLigacaoPrimaria(entity.getLigacaoPrimaria() != null ? entity.getLigacaoPrimaria().getId() : null)
				.withIdLigacaoAnterior(entity.getLigacaoAnterior() != null ? entity.getLigacaoAnterior().getId() : null)
				.withUsuario(entity.getUsuario() != null ? usuarioService.toDTO(entity.getUsuario()) : null)
				.withDataLigacao(entity.getDataCriacao())
			;
		
		
		if(comRetorno && entity.getLigacaoRetorno() != null) {
			builder.withRetorno(ligacaoRetornoService.toDTO(entity.getLigacaoRetorno()));
		}
		
		return builder.build();
	}

	@Override
	public List<LigacaoDTO> toDTO(List<Ligacao> entityList, boolean semRetorno) {
		List<LigacaoDTO> result = new ArrayList<>(entityList.size());
		for (Ligacao entity : entityList) {
			result.add(toDTO(entity, semRetorno));
		}

		return result;
	}

	public Ligacao toEntity(LigacaoDTO dto) {
		return toEntity(new Ligacao(), dto);
	}

	public Ligacao toEntity(Ligacao entity, LigacaoDTO dto) {

		if (dto.getId() != null && (entity.getId() == null || entity.getId().longValue() == 0)) {
			entity.setId(dto.getId());
		}
		
		if (dto.getNome() != null) {
			entity.setNome(dto.getNome());
		}
		
		if (dto.getCnpj() != null) {
			entity.setCnpj(dto.getCnpj());
		}

		if (dto.getTelefone() != null) {
			entity.setTelefone(dto.getTelefone());
		}

		if (dto.getObservacao()!= null) {
			entity.setObservacao(dto.getObservacao());
		}
		
		if(entity.isNew()) {
			entity.setUsuario(usuarioService.getCurrentUser());
		}
		
		if(dto.getIdLigacaoAnterior() != null) {
			Ligacao ligacao = findById(dto.getIdLigacaoAnterior());
			if(ligacao != null) {
				entity.setLigacaoAnterior(ligacao);
				entity.setNome(ligacao.getNome());
				entity.setCnpj(ligacao.getCnpj());
				entity.setTelefone(ligacao.getTelefone());
			}
		}
		
		if(dto.getIdLigacaoPrimaria() != null) {
			Ligacao ligacao = findById(dto.getIdLigacaoPrimaria());
			if(ligacao != null) {
				entity.setLigacaoPrimaria(ligacao);
			}
		}
		
		if(dto.getRetorno() != null && entity.isNew()) {
			LigacaoRetorno retorno = ligacaoRetornoService.toEntity(dto.getRetorno());
			entity.setLigacaoRetorno(retorno);
			retorno.setLigacao(entity);
			retorno.setUsuario(entity.getUsuario());
		}

		return entity;
	}
	
	@Override
	public BaseSearch applyRules(BaseSearch baseSearch) {
		
		LigacaoSearch search = (LigacaoSearch) baseSearch;
		
		Usuario usuario = usuarioService.getCurrentUser();
		
		Perfil perfil = usuario.getPerfil();
		if(!perfil.getSigla().equals(EPerfil.SIGLA.ADMIN)) {
			search.setIdUsuario(usuario.getId());
		}
		
		return baseSearch;
	}
	

	@Override
	public AtividadeDTO createAtividade(LigacaoDTO newEntity, LigacaoDTO oldEntity, boolean novo) {
		AtividadeDTO atividade = atividadeService.createAtividadeDTO(Ligacao.class.getSimpleName(), newEntity.getId(), novo);
		
		List<AtividadeAcaoDTO> acaoList = new ArrayList<>();
										
		if(CompareUtils.changed(newEntity.getNome(), oldEntity.getNome())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getNome(), oldEntity.getNome(), ECampo.LIGACAO_NOME.name()));
		}
		
		if(CompareUtils.changed(newEntity.getCnpj(), oldEntity.getCnpj())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getCnpj(), oldEntity.getCnpj(), ECampo.LIGACAO_CNPJ.name()));
		}
		
		if(CompareUtils.changed(newEntity.getTelefone(), oldEntity.getTelefone())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getTelefone(), oldEntity.getTelefone(), ECampo.LIGACAO_TELEFONE.name()));
		}
		
		if(CompareUtils.changed(newEntity.getObservacao(), oldEntity.getObservacao())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getObservacao(), oldEntity.getObservacao(), ECampo.LIGACAO_OBSERVACAO.name()));
		}
		
		atividade.setAtividadeAcaoList(acaoList);
		
		return atividade;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Ligacao save(LigacaoDTO dto, Long id) throws BaseServiceException {
		Ligacao entity = id != null ? findById(id) : new Ligacao();
		
		LigacaoDTO oldDTO = toDTO(entity);
		
		entity = toEntity(entity, dto);
		
		if(dto.getIdLigacaoAnterior() != null) {			
			Ligacao ligacao = findById(dto.getIdLigacaoAnterior());
			if(ligacao != null) {	
				LigacaoRetorno lr = ligacao.getLigacaoRetorno();
				lr.setRetornado(true);
				ligacaoRetornoService.save(lr);
			}
		}
		
						
		Ligacao novo = save(entity);
		AtividadeDTO atividadeDTO = createAtividade(toDTO(novo), oldDTO, id == null);
		atividadeService.save(atividadeDTO);
		
		//cria retorno
		if(dto.getRetorno() != null && id == null) {
			AtividadeDTO atividadeRetornoDTO = ligacaoRetornoService.createAtividade(ligacaoRetornoService.toDTO(novo.getLigacaoRetorno()), new LigacaoRetornoDTO(), id == null);
			atividadeService.save(atividadeRetornoDTO);
		}
		return novo;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Ligacao save(LigacaoDTO dto) throws BaseServiceException {
		return save(dto, null);
	}

	@Override
	public List<Ligacao> findLigacoesPorPeriodo(LocalDate dataInicio, LocalDate dataFim) {
		return ligacaoRepository.findLigacoesPorPeriodo(dataInicio.atTime(0,0, 0), dataFim.atTime(23,59, 59));
	}
}
