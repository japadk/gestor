package com.verifik.carteira.application.service.regra;

import com.verifik.carteira.application.model.dto.RegraDTO;
import com.verifik.carteira.application.model.entities.Regra;
import com.verifik.carteira.application.service.IDefaultService;

public interface IRegraService extends IDefaultService<Regra, RegraDTO> {
	
	public Regra getRegraByChave(String sigla);

}
