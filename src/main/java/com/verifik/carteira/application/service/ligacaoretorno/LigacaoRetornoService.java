package com.verifik.carteira.application.service.ligacaoretorno;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.dto.AtividadeAcaoDTO;
import com.verifik.carteira.application.model.dto.AtividadeDTO;
import com.verifik.carteira.application.model.dto.LigacaoRetornoDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.entities.LigacaoRetorno;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.enums.ECampo;
import com.verifik.carteira.application.model.search.LigacaoRetornoSearch;
import com.verifik.carteira.application.repository.LigacaoRetornoRepository;
import com.verifik.carteira.application.service.atividade.IAtividadeService;
import com.verifik.carteira.application.service.cliente.ClienteService;
import com.verifik.carteira.application.service.ligacao.ILigacaoService;
import com.verifik.carteira.application.service.usuario.IUsuarioService;
import com.verifik.carteira.application.utils.CompareUtils;
import com.verifik.carteira.application.utils.SearchUtils;

@Service
public class LigacaoRetornoService implements ILigacaoRetornoService {

	@Autowired
	ILigacaoService ligacaoService;
		
	@Autowired
	LigacaoRetornoRepository ligacaoRetornoRepository;
	
	@Autowired
	IAtividadeService atividadeService;
		
	@Autowired
	ClienteService clienteService;
	
	@Autowired
	IUsuarioService usuarioService;

	@Override
	public List<LigacaoRetorno> findAll() {
		return ligacaoRetornoRepository.findAll();
	}

	@Override
	public LigacaoRetorno findById(long id) {

		Optional<LigacaoRetorno> entity = ligacaoRetornoRepository.findById(id);

		return entity.orElseGet(() -> null);

	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public LigacaoRetorno save(LigacaoRetorno entity) {
		
		if (entity.getRegistroAtivo() == null) {
			entity.setRegistroAtivo(true);
		}

		return ligacaoRetornoRepository.save(entity);

	}

	@Override
	public void delete(long id) {
		ligacaoRetornoRepository.deleteById(id);
	}

	@Override
	public List<LigacaoRetorno> search(BaseSearch baseSearch) {
		return ligacaoRetornoRepository.findBySearch(baseSearch);
	}
	
	@Override
	public long count(BaseSearch baseSearch) {
		return ligacaoRetornoRepository.countBySearch(baseSearch);
	}
	
	@Override
	public SearchResultDTO pagedSearch(BaseSearch baseSearch) {
		return SearchUtils.runPagedSearch(this, baseSearch);
	}

	@Override
	public LigacaoRetornoDTO toDTO(LigacaoRetorno entity) {
		
		return LigacaoRetornoDTO.builder()
				.withId(entity.getId())
				.withDataRetorno(entity.getDataRetorno())
				.withHoraRetorno(entity.getHoraRetorno())
				.withIdUsuario(entity.getUsuario().getId())
				.withNomeUsuario(entity.getUsuario().getNome())
				.withIdLigacao(entity.getLigacao().getId())
				.withRetornado(entity.getRetornado())
				.withLigacao(ligacaoService.toDTO(entity.getLigacao(), false))
				.withRegistroAtivo(entity.getRegistroAtivo())
				.build();
	}

	@Override
	public List<LigacaoRetornoDTO> toDTO(List<LigacaoRetorno> entityList) {
		List<LigacaoRetornoDTO> result = new ArrayList<>(entityList.size());
		for (LigacaoRetorno entity : entityList) {
			result.add(toDTO(entity));
		}

		return result;
	}

	public LigacaoRetorno toEntity(LigacaoRetornoDTO dto) {
		return toEntity(new LigacaoRetorno(), dto);
	}

	public LigacaoRetorno toEntity(LigacaoRetorno entity, LigacaoRetornoDTO dto) {
		
		if(entity.isNew()) {
			entity.setRegistroAtivo(true);
			entity.setRetornado(false);
		}

		if (dto.getId() != null && (entity.getId() == null || entity.getId().longValue() == 0)) {
			entity.setId(dto.getId());
		}
		
		if (dto.getDataRetorno() != null) {
			entity.setDataRetorno(dto.getDataRetorno());
		}
		
		if (dto.getHoraRetorno() != null) {
			entity.setHoraRetorno(dto.getHoraRetorno());
		}

		if (dto.getIdUsuario() != null) {
			Usuario usuario = usuarioService.findById(dto.getIdUsuario());
			if(usuario != null) {
				entity.setUsuario(usuario);
			}
		}

		if (dto.getRetornado() != null) {
			entity.setRetornado(dto.getRetornado());
		}

		if (dto.getRegistroAtivo() != null) {
			entity.setRegistroAtivo(dto.getRegistroAtivo());
		}

		return entity;
	}
	
	@Override
	public BaseSearch applyRules(BaseSearch baseSearch) {
		
		LigacaoRetornoSearch search = (LigacaoRetornoSearch) baseSearch;
		
		if(search.getMeuUsuario() != null && search.getMeuUsuario()) {
			search.setIdUsuarioAtual(usuarioService.getCurrentUser().getId());
		}
				
		return baseSearch;
	}
	

	@Override
	public AtividadeDTO createAtividade(LigacaoRetornoDTO newEntity, LigacaoRetornoDTO oldEntity, boolean novo) {
		AtividadeDTO atividade = atividadeService.createAtividadeDTO(LigacaoRetorno.class.getSimpleName(), newEntity.getId(), novo);
		
		List<AtividadeAcaoDTO> acaoList = new ArrayList<>();
								
		if(CompareUtils.changed(newEntity.getDataRetorno(), oldEntity.getDataRetorno())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getDataRetorno(), oldEntity.getDataRetorno(), ECampo.RETORNO_DATA_RETORNO.name()));
		}
		
		if(CompareUtils.changed(newEntity.getHoraRetorno(), oldEntity.getHoraRetorno())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getHoraRetorno(), oldEntity.getHoraRetorno(), ECampo.RETORNO_HORA_RETORNO.name()));
		}
		
		if(CompareUtils.changed(newEntity.getRetornado(), oldEntity.getRetornado())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getRetornado(), oldEntity.getRetornado(), ECampo.RETORNO_RETORNADO.name()));
		}
		
		atividade.setAtividadeAcaoList(acaoList);
		
		return atividade;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public LigacaoRetorno save(LigacaoRetornoDTO dto, Long id) throws BaseServiceException {
		LigacaoRetorno entity = id != null ? findById(id) : new LigacaoRetorno();
		
		LigacaoRetornoDTO oldDTO = toDTO(entity);
		
		entity = toEntity(entity, dto);
						
		LigacaoRetorno novo = save(entity);
		AtividadeDTO atividadeDTO = createAtividade(toDTO(novo), oldDTO, id == null);
		atividadeService.save(atividadeDTO);
		return novo;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public LigacaoRetorno save(LigacaoRetornoDTO dto) throws BaseServiceException {
		return save(dto, null);
	}
}
