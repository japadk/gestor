package com.verifik.carteira.application.service.relatorio;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.verifik.carteira.application.model.dto.*;
import com.verifik.carteira.application.model.entities.*;
import com.verifik.carteira.application.service.cliente.ClienteService;
import com.verifik.carteira.application.service.ligacao.LigacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.verifik.carteira.application.model.enums.EAtividadeEntidade;
import com.verifik.carteira.application.model.enums.ECampo;
import com.verifik.carteira.application.repository.AtividadeRepository;
import com.verifik.carteira.application.service.divida.DividaService;
import com.verifik.carteira.application.utils.StringUtils;

@Service
public class RelatorioService implements IRelatorioService {

  @Autowired
  private AtividadeRepository atividadeRepository;

  @Autowired
  private DividaService dividaService;

  @Autowired
  private ClienteService clienteService;

  @Autowired
  private LigacaoService ligacaoService;


  @Override
  public List<RelatorioAtividadeDividaCsvDTO> buscaRelatorioAtividadeDividaDTO(
      AtividadeDividaRelatorioParametroDTO search) {

    LocalDateTime inicio = search.getInicio() != null ? search.getInicio().atStartOfDay()
        : LocalDate.now().atStartOfDay();
    LocalDateTime fim = search.getFim() != null ? search.getFim().atTime(23, 59) : LocalDate.now().atTime(23, 59);
    String entidade = EAtividadeEntidade.DIVIDA.name();

    List<Atividade> atividades;
    if (search.getIdUsuario() != null) {
      atividades = atividadeRepository.findBy(entidade, search.getIdUsuario(), inicio, fim);
    } else {
      atividades = atividadeRepository.findBy(entidade, inicio, fim);
    }

    List<RelatorioAtividadeDividaCsvDTO> result = new ArrayList<>(atividades.size());
    DateTimeFormatter stf = DateTimeFormatter.ofPattern("dd/MM/YYYY - hh:mm");
    for (Atividade atividade : atividades) {

      List<AtividadeAcao> acaoList = atividade.getAtividadeAcaoList();
      RelatorioAtividadeDividaCsvDTO linha = new RelatorioAtividadeDividaCsvDTO();
      Divida divida = dividaService.findById(atividade.getIdEntidade());
      Usuario usuario = atividade.getUsuario();

      linha.setDataHora(atividade.getDataCriacao().format(stf));
      linha.setAtendente(usuario.getNome());

      for (AtividadeAcao acao : acaoList) {

        ECampo campo = ECampo.valueOf(acao.getCampo());

        switch (campo) {
          case DIVIDA_OBSERVACAO:
            linha.setObservacao(acao.getValorNovo());
            break;
          case DIVIDA_PAGO:
            linha.setPago(acao.getValorNovo());
            break;
          case DIVIDA_DATA_PAGAMENTO:
            linha.setPagamento(acao.getValorNovo());
            break;
          case DIVIDA_DATA_VENCIMENTO:
            linha.setVencimento(acao.getValorNovo());
            break;
          case DIVIDA_VALOR_RECEBIDO:
            linha.setValorRecebido(acao.getValorNovo());
            break;
          case DIVIDA_VALOR:
            linha.setValor(acao.getValorNovo());
            break;
          default:
            break;
        }
      }

      Cliente cliente = divida.getCliente();
      linha.setCliente(cliente.getNome());
      linha.setCodigo(cliente.getCodigo());

      result.add(linha);

    }

    return result;
  }

  @Override
  public List<RelatorioDividasPagasCsvDTO> buscaRelatorioDividaPagasDTO(DividasPagasRelatorioParametroDTO dto) {

    List<Divida> dividasPagasNoPeriodo = dividaService.findDividasPagasPorPeriodo(dto.getInicio(), dto.getFim());
    List<RelatorioDividasPagasCsvDTO> retorno = new ArrayList<>(dividasPagasNoPeriodo.size());
    for (Divida d : dividasPagasNoPeriodo) {
      Cliente cliente = d.getCliente();
      Carteira carteira = cliente.getCarteira();
      Usuario atendente = d.getUsuarioBaixa();

      RelatorioDividasPagasCsvDTO linha = new RelatorioDividasPagasCsvDTO();

      linha.setAtendente(atendente.getNome());
      linha.setCarteira(carteira.getNome());
      linha.setCliente(cliente.getNome());
      linha.setCodigo(cliente.getCodigo());
      linha.setObservacao(d.getObservacao());
      linha.setPagamento(StringUtils.toString(d.getDataPagamento()));
      linha.setVencimento(StringUtils.toString(d.getDataVencimento()));
      linha.setValor(StringUtils.toString(d.getValor()));
      linha.setValorRecebido(StringUtils.toString(d.getValorRecebido()));
      linha.setCnpj(cliente.getCnpj());
      linha.setEmail(cliente.getEmail());
      linha.setTelefone(cliente.getTelefone());
      linha.setContato(cliente.getContato());

      retorno.add(linha);

    }

    return retorno;

  }

  @Override
  public List<RelatorioDividasNaoPagasCsvDTO> buscaRelatorioDividaNaoPagasDTO(
      DividasNaoPagasRelatorioParametroDTO dto) {

    List<Divida> dividasNaoPagasNoPeriodo = dividaService.findDividasNaoPagasPorPeriodo(dto.getInicio(), dto.getFim());
    List<RelatorioDividasNaoPagasCsvDTO> retorno = new ArrayList<>(dividasNaoPagasNoPeriodo.size());
    for (Divida d : dividasNaoPagasNoPeriodo) {
      Cliente cliente = d.getCliente();
      Carteira carteira = cliente.getCarteira();

      RelatorioDividasNaoPagasCsvDTO linha = new RelatorioDividasNaoPagasCsvDTO();

      linha.setCarteira(carteira.getNome());
      linha.setCliente(cliente.getNome());
      linha.setCodigo(cliente.getCodigo());
      linha.setObservacao(d.getObservacao());
      linha.setVencimento(StringUtils.toString(d.getDataVencimento()));
      linha.setValor(StringUtils.toString(d.getValor()));
      linha.setCnpj(cliente.getCnpj());
      linha.setEmail(cliente.getEmail());
      linha.setTelefone(cliente.getTelefone());
      linha.setContato(cliente.getContato());

      retorno.add(linha);

    }

    return retorno;

  }

  @Override
  public List<RelatorioDividasCsvDTO> buscaRelatorioDividaDTO(DividasRelatorioParametroDTO dto) {

    List<Divida> dividasNoPeriodo = dividaService.findDividasPorPeriodo(dto.getInicio(), dto.getFim());
    List<RelatorioDividasCsvDTO> retorno = new ArrayList<>(dividasNoPeriodo.size());
    for (Divida d : dividasNoPeriodo) {
      Cliente cliente = d.getCliente();
      Carteira carteira = cliente.getCarteira();

      RelatorioDividasCsvDTO linha = new RelatorioDividasCsvDTO();

      linha.setCarteira(carteira.getNome());
      linha.setCliente(cliente.getNome());
      linha.setCodigo(cliente.getCodigo());
      linha.setObservacao(d.getObservacao());
      linha.setPagamento(StringUtils.toString(d.getDataPagamento()));
      linha.setVencimento(StringUtils.toString(d.getDataVencimento()));
      linha.setValor(StringUtils.toString(d.getValor()));
      linha.setValorRecebido(StringUtils.toString(d.getValorRecebido()));
      linha.setCnpj(cliente.getCnpj());
      linha.setEmail(cliente.getEmail());
      linha.setTelefone(cliente.getTelefone());
      linha.setContato(cliente.getContato());

      retorno.add(linha);
    }

    return retorno;

  }

  @Override
  public List<RelatorioClienteCsvDTO> buscaClientesAtivos(){
    List<Cliente> clientesAtivos = clienteService.findAllActive();
    List<RelatorioClienteCsvDTO> retorno = new ArrayList<>(clientesAtivos.size());
    for (Cliente c : clientesAtivos) {
      Carteira carteira = c.getCarteira();

      RelatorioClienteCsvDTO linha = new RelatorioClienteCsvDTO();
      linha.setCarteira(carteira.getNome());
      linha.setCliente(c.getNome());
      linha.setCodigo(c.getCodigo());
      linha.setCnpj(c.getCnpj());
      linha.setEmail(c.getEmail());
      linha.setTelefone(c.getTelefone());
      linha.setContato(c.getContato());

      retorno.add(linha);
    }
    return retorno;
  }

  @Override
  public List<RelatorioLigacoesCsvDTO> buscaRelatorioLigacoesDTO(LigacoesRelatorioParametroDTO dto) {

    List<Ligacao> ligacoesNoPeriodo = ligacaoService.findLigacoesPorPeriodo(dto.getInicio(), dto.getFim());
    List<RelatorioLigacoesCsvDTO> retorno = new ArrayList<>(ligacoesNoPeriodo.size());
    for (Ligacao ligacao : ligacoesNoPeriodo) {
      RelatorioLigacoesCsvDTO linha = new RelatorioLigacoesCsvDTO();

      linha.setAtendente(ligacao.getUsuario().getNome());
      linha.setNome(ligacao.getNome());
      linha.setObservacao(ligacao.getObservacao());
      linha.setCnpj(ligacao.getCnpj());
      linha.setTelefone(ligacao.getTelefone());

      retorno.add(linha);
    }

    return retorno;

  }
}
