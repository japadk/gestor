package com.verifik.carteira.application.service.perfil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.exceptions.service.perfil.PerfilSiglaDuplicadoException;
import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.dto.PerfilDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.entities.Perfil;
import com.verifik.carteira.application.repository.PerfilRepository;
import com.verifik.carteira.application.utils.SearchUtils;

@Service
public class PerfilService implements IPerfilService {

	@Autowired 
	PerfilRepository perfilRepository;
	
	@Override
	public Perfil getPerfilBySigla(String sigla) {
		return perfilRepository.findBySiglaIgnoreCase(sigla);
	}
	
	@Override
	public List<Perfil> findAll() {
		return perfilRepository.findAll();
	}
	
	@Override
	public Perfil findById(long id) {
		
		Optional<Perfil> entity = perfilRepository.findById(id);
		
		return entity.orElseGet(() -> null);
		
	}
	
	@Override
	public Perfil save(Perfil entity) throws BaseServiceException {
		
		Perfil perfilSigla = perfilRepository.findBySiglaIgnoreCase(entity.getSigla());
		
		if(perfilSigla != null) {
			throw new PerfilSiglaDuplicadoException();
		}
		
		if(entity.getRegistroAtivo() == null) {
			entity.setRegistroAtivo(true);
		}
		
		
		return perfilRepository.save(entity);
		
	}
	
	@Override
	public void delete(long id) {
		perfilRepository.deleteById(id);
	}

	@Override
	public List<Perfil> search(BaseSearch baseSearch) {
		return perfilRepository.findBySearch(baseSearch);
	}
	
	@Override
	public long count(BaseSearch baseSearch) {
		return perfilRepository.countBySearch(baseSearch);
	}
	
	@Override
	public SearchResultDTO pagedSearch(BaseSearch baseSearch) {
		return SearchUtils.runPagedSearch(this, baseSearch);
	}

	@Override
	public PerfilDTO toDTO(Perfil entity) {
		return PerfilDTO.builder()
			.withId(entity.getId())
			.withSigla(entity.getSigla())
			.withDescricao(entity.getDescricao())
			.withRegistroAtivo(entity.getRegistroAtivo())
			.build();
	}

	@Override
	public List<PerfilDTO> toDTO(List<Perfil> entityList) {
		List<PerfilDTO> result = new ArrayList<>(entityList.size());
		for(Perfil entity : entityList) {
			result.add(toDTO(entity));
		}
		
		return result;
	}

	@Override
	public Perfil toEntity(PerfilDTO dto) {
		return toEntity(new Perfil(), dto);
	}
	
	@Override
	public Perfil toEntity(Perfil entity, PerfilDTO dto) {
		
		if(dto.getId() != null && (entity.getId() == null || entity.getId().longValue() == 0)) {
			entity.setId(dto.getId());			
		}
		
		if(dto.getSigla() != null) {
			entity.setSigla(dto.getSigla());
		}
		
		if(dto.getDescricao() != null) {
			entity.setDescricao(dto.getDescricao());
		}
		
		if(dto.getRegistroAtivo() != null) {
			entity.setRegistroAtivo(dto.getRegistroAtivo());
		}
		
		return entity;
	}
	
	@Override
	public BaseSearch applyRules(BaseSearch baseSearch) {
		return baseSearch;
	}

}
