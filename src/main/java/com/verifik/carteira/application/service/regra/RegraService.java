package com.verifik.carteira.application.service.regra;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.exceptions.service.regra.RegraChaveDuplicadoException;
import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.dto.RegraDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.entities.Regra;
import com.verifik.carteira.application.repository.RegraRepository;
import com.verifik.carteira.application.utils.SearchUtils;

@Service
public class RegraService implements IRegraService {

	@Autowired 
	RegraRepository regraRepository;
	
	@Override
	public Regra getRegraByChave(String chave) {
		return regraRepository.findByChaveIgnoreCase(chave);
	}
	
	@Override
	public List<Regra> findAll() {
		return regraRepository.findAll();
	}
	
	@Override
	public Regra findById(long id) {
		
		Optional<Regra> entity = regraRepository.findById(id);
		
		return entity.orElseGet(() -> null);
		
	}
	
	@Override
	public Regra save(Regra entity) throws BaseServiceException {
		
		Regra regraChave = regraRepository.findByChaveIgnoreCase(entity.getChave());
		
		if(regraChave != null) {
			throw new RegraChaveDuplicadoException();
		}
		
		if(entity.getRegistroAtivo() == null) {
			entity.setRegistroAtivo(true);
		}
		
		
		return regraRepository.save(entity);
		
	}
	
	@Override
	public void delete(long id) {
		regraRepository.deleteById(id);
	}

	@Override
	public List<Regra> search(BaseSearch baseSearch) {
		return regraRepository.findBySearch(baseSearch);
	}
	
	@Override
	public long count(BaseSearch baseSearch) {
		return regraRepository.countBySearch(baseSearch);
	}
	
	@Override
	public SearchResultDTO pagedSearch(BaseSearch baseSearch) {
		return SearchUtils.runPagedSearch(this, baseSearch);
	}

	@Override
	public RegraDTO toDTO(Regra entity) {
		return RegraDTO.builder()
			.withId(entity.getId())
			.withChave(entity.getChave())
			.withDescricao(entity.getDescricao())
			.withRegistroAtivo(entity.getRegistroAtivo())
			.build();
	}

	@Override
	public List<RegraDTO> toDTO(List<Regra> entityList) {
		List<RegraDTO> result = new ArrayList<>(entityList.size());
		for(Regra entity : entityList) {
			result.add(toDTO(entity));
		}
		
		return result;
	}

	@Override
	public Regra toEntity(RegraDTO dto) {
		return toEntity(new Regra(), dto);
	}
	
	@Override
	public Regra toEntity(Regra entity, RegraDTO dto) {
		
		if(dto.getId() != null && (entity.getId() == null || entity.getId().longValue() == 0)) {
			entity.setId(dto.getId());			
		}
		
		if(dto.getChave() != null) {
			entity.setChave(dto.getChave());
		}
		
		if(dto.getDescricao() != null) {
			entity.setDescricao(dto.getDescricao());
		}
		
		if(dto.getRegistroAtivo() != null) {
			entity.setRegistroAtivo(dto.getRegistroAtivo());
		}
		
		return entity;
	}
	
	@Override
	public BaseSearch applyRules(BaseSearch baseSearch) {
		return baseSearch;
	}

}
