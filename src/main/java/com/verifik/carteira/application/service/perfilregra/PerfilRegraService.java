package com.verifik.carteira.application.service.perfilregra;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.dto.PerfilRegraDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.entities.PerfilRegra;
import com.verifik.carteira.application.model.entities.Regra;
import com.verifik.carteira.application.repository.PerfilRegraRepository;
import com.verifik.carteira.application.service.perfil.PerfilService;
import com.verifik.carteira.application.service.regra.RegraService;
import com.verifik.carteira.application.utils.SearchUtils;

@Service
public class PerfilRegraService implements IPerfilRegraService {

	@Autowired 
	PerfilRegraRepository perfilRegraRepository;
	
	@Autowired
	PerfilService perfilService;
	
	@Autowired
	RegraService regraService;
	
	@Override
	public List<PerfilRegra> findAll() {
		return perfilRegraRepository.findAll();
	}
	
	@Override
	public PerfilRegra findById(long id) {
		
		Optional<PerfilRegra> entity = perfilRegraRepository.findById(id);
		
		return entity.orElseGet(() -> null);
		
	}
	
	@Override
	public PerfilRegra save(PerfilRegra regra) {
		
		if(regra.getRegistroAtivo() == null) {
			regra.setRegistroAtivo(true);
		}
		
		
		return perfilRegraRepository.save(regra);
		
	}
	
	@Override
	public void delete(long id) {
		perfilRegraRepository.deleteById(id);
	}

	@Override
	public List<PerfilRegra> search(BaseSearch baseSearch) {
		return perfilRegraRepository.findBySearch(baseSearch);
	}
	
	@Override
	public long count(BaseSearch baseSearch) {
		return perfilRegraRepository.countBySearch(baseSearch);
	}
	
	@Override
	public SearchResultDTO pagedSearch(BaseSearch baseSearch) {
		return SearchUtils.runPagedSearch(this, baseSearch);
	}

	@Override
	public PerfilRegraDTO toDTO(PerfilRegra entity) {
		return PerfilRegraDTO.builder()
			.withId(entity.getId())
			.withPerfil(perfilService.toDTO(entity.getPerfil()))
			.withRegra(regraService.toDTO(entity.getRegra()))
			.withRegistroAtivo(entity.getRegistroAtivo())
			.build();
	}

	@Override
	public List<PerfilRegraDTO> toDTO(List<PerfilRegra> entityList) {
		List<PerfilRegraDTO> result = new ArrayList<>(entityList.size());
		for(PerfilRegra entity : entityList) {
			result.add(toDTO(entity));
		}
		
		return result;
	}

	@Override
	public PerfilRegra toEntity(PerfilRegraDTO dto) {
		return toEntity(new PerfilRegra(), dto);
	}
	
	@Override
	public PerfilRegra toEntity(PerfilRegra entity, PerfilRegraDTO dto) {
		
		throw new NotImplementedException("Ainda não implementado");
	}

	@Override
	public List<Regra> getRegrasPorPerfilSigla(String sigla) {
		return perfilRegraRepository.findRegrasByPerfilSigla(sigla);
	}
	
	@Override
	public BaseSearch applyRules(BaseSearch baseSearch) {
		return baseSearch;
	}

}
