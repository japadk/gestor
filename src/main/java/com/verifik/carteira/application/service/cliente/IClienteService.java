package com.verifik.carteira.application.service.cliente;

import com.verifik.carteira.application.model.dto.ClienteDTO;
import com.verifik.carteira.application.model.entities.Cliente;
import com.verifik.carteira.application.service.IDefaultAtividadeService;

import java.util.List;

public interface IClienteService extends IDefaultAtividadeService<Cliente, ClienteDTO> {

	Cliente findByNome(String nome);

    List<Cliente> findAllActive();
}
