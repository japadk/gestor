package com.verifik.carteira.application.service.usuario;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.verifik.carteira.application.component.IAuthenticationFacade;
import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.exceptions.service.usuario.UsuarioLoginDuplicadoException;
import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.dto.AtividadeAcaoDTO;
import com.verifik.carteira.application.model.dto.AtividadeDTO;
import com.verifik.carteira.application.model.dto.CarteiraDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.dto.UsuarioDTO;
import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.model.entities.Perfil;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.entities.UsuarioCarteira;
import com.verifik.carteira.application.model.enums.ECampo;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.repository.CarteiraRepository;
import com.verifik.carteira.application.repository.PerfilRepository;
import com.verifik.carteira.application.repository.UsuarioCarteiraRepository;
import com.verifik.carteira.application.repository.UsuarioRepository;
import com.verifik.carteira.application.service.atividade.IAtividadeService;
import com.verifik.carteira.application.service.carteira.ICarteiraService;
import com.verifik.carteira.application.service.usuariocarteira.IUsuarioCarteiraService;
import com.verifik.carteira.application.utils.CompareUtils;
import com.verifik.carteira.application.utils.SearchUtils;

@Service
public class UsuarioService implements IUsuarioService {

	@Autowired 
	UsuarioRepository usuarioRepository;
	
	@Autowired
	PerfilRepository perfilRepository;
	
	@Autowired
	CarteiraRepository carteiraRepository;

    @Autowired
    UsuarioCarteiraRepository usuarioCarteiraRepository;
	
	@Autowired
	@Lazy
	PasswordEncoder passwordEncoder;
	
	@Autowired
	ICarteiraService carteiraService;
	
	@Autowired
	IAtividadeService atividadeService;
	
	@Autowired
	IUsuarioCarteiraService usuarioCarteiraService;
	
	@Autowired
	IAuthenticationFacade authFacade;
	
	@Override
	public Usuario getUsuarioByLogin(String login) {
		return usuarioRepository.findByLoginIgnoreCase(login);
	}

	@Override
	public List<Usuario> findAll() {
		return usuarioRepository.findAll();
	}
	
	@Override
	public Usuario findById(long id) {
		
		Optional<Usuario> user = usuarioRepository.findById(id);
		
		return user.orElseGet(() -> null);
		
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Usuario save(Usuario entity) throws BaseServiceException {
		
		Usuario usuarioLogin = usuarioRepository.findByLoginIgnoreCase(entity.getLogin());
		
		if(usuarioLogin != null) {
			//Se usuarioLogin não esta nulo, achou um login duplicado
			if(entity.getId() == null) {
				//Se o id a ser salvo é nulo, é porque vai criar um novo usuário e deve bloquear
				throw new UsuarioLoginDuplicadoException();
			}
			
			//Se os ids forem diferentes, é porque quer mudar o login pra outro que já existe, então bloqueia
			if(entity.getId().longValue() != usuarioLogin.getId().longValue()) {
				throw new UsuarioLoginDuplicadoException();
			}
			
			//Se passou até aqui, é porque o usuário esta apenas sendo atualizado 
		}
		
		if(entity.getRegistroAtivo() == null) {
			entity.setRegistroAtivo(true);
		}
		
		
		return usuarioRepository.save(entity);
		
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Usuario saveWithCarteira(Usuario entity, UsuarioDTO dto) throws BaseServiceException {
		
		Usuario saved = save(entity);
		List<CarteiraDTO> carteiras = dto.getCarteiraList() != null && !dto.getCarteiraList().isEmpty() ? dto.getCarteiraList() : new ArrayList<>(0); 
		List<Long> idCarteiras = carteiras.stream()
				.mapToLong( carteira -> carteira.getId())
				.boxed()
				.collect(Collectors.toList());
		usuarioCarteiraService.updateUsuarioCarteira(entity, idCarteiras);		
		
		return saved;
	}
	
	@Override
	public void delete(long id) {
		usuarioRepository.deleteById(id);
	}

	@Override
	public List<Usuario> search(BaseSearch baseSearch) {
		return usuarioRepository.findBySearch(baseSearch);
	}
	
	@Override
	public long count(BaseSearch baseSearch) {
		return usuarioRepository.countBySearch(baseSearch);
	}
	
	@Override
	public SearchResultDTO pagedSearch(BaseSearch baseSearch) {
		return SearchUtils.runPagedSearch(this, baseSearch);
	}
	
	@Override
	public UsuarioDTO toDTO(Usuario entity, boolean hidePassword) {
		return UsuarioDTO.builder()
				.withId(entity.getId())
				.withNome(entity.getNome())
				.withLogin(entity.getLogin())
				.withSenha(hidePassword ? null : entity.getSenha())
				.withSiglaPerfil(entity.getPerfil() != null ? entity.getPerfil().getSigla() : null)
				.withRegistroAtivo(entity.getRegistroAtivo())
				.withCarteiraList(carteiraService.toDTO(entity.getCarteiraList()))
				.build();
		
	}

	@Override
	public UsuarioDTO toDTO(Usuario entity) {
		return toDTO(entity, true);
	}
	
	@Override
	public List<UsuarioDTO> toDTO(List<Usuario> entityList, boolean hidePassword) {
		List<UsuarioDTO> result = new ArrayList<>(entityList.size());
		for(Usuario entity : entityList) {
			result.add(toDTO(entity));
		}
		
		return result;
	}

	@Override
	public List<UsuarioDTO> toDTO(List<Usuario> entityList) {
		return toDTO(entityList, true);
	}

	@Override
	public Usuario encodePassword(Usuario entity) {
		String encodedPassword = passwordEncoder.encode(entity.getSenha());
		
		entity.setSenha(encodedPassword);
		
		return entity;
	}

	@Override
	public Usuario toEntity(UsuarioDTO dto) {
		return toEntity(new Usuario(), dto);
	}
	
	@Override
	public Usuario toEntity(Usuario entity, UsuarioDTO dto) {
		
		if(dto.getId() != null && (entity.getId() == null || entity.getId().longValue() == 0)) {
			entity.setId(dto.getId());			
		}
		
		if(dto.getLogin() != null) {
			entity.setLogin(dto.getLogin());
		}
		
		if(dto.getSenha() != null && !dto.getSenha().trim().isEmpty()) {
			entity.setSenha(dto.getSenha());
		}
		
		if(dto.getNome() != null) {
			entity.setNome(dto.getNome());
		}
		
		if(dto.getSiglaPerfil() != null) {
			Perfil perfil = perfilRepository.findBySiglaIgnoreCase(dto.getSiglaPerfil());
			entity.setPerfil(perfil);
		}
		
		if(dto.getRegistroAtivo() != null) {
			entity.setRegistroAtivo(dto.getRegistroAtivo());
		}
		
		return entity;
	}

	@Override
	@Transactional
	public Usuario createUsuario(UsuarioDTO dto) throws BaseServiceException {
		return save(dto);
	}

	@Override
	public Usuario getCurrentUser() {
		Authentication auth =  authFacade.getAuthentication();
		return usuarioRepository.findByLoginIgnoreCase(auth.getName());
	}

    public boolean hasCarteira(Carteira carteira, Usuario usuario){

		Perfil perfil = usuario.getPerfil();
		if(perfil.getSigla().equals(EPerfil.SIGLA.ADMIN)) {
            return true;
        }

		UsuarioCarteira usuarioCarteira = usuarioCarteiraRepository.findByIdCateiraAndIdUsuario(carteira.getId(), usuario.getId());
        if(usuarioCarteira != null) {
            return true;
        }

        return false;

    }

	@Override
	public BaseSearch applyRules(BaseSearch baseSearch) {
		return baseSearch;
	}

	@Override
	public AtividadeDTO createAtividade(UsuarioDTO newEntity, UsuarioDTO oldEntity, boolean novo) {
		AtividadeDTO atividade = atividadeService.createAtividadeDTO(Usuario.class.getSimpleName(), newEntity.getId(), novo);
		
		List<AtividadeAcaoDTO> acaoList = new ArrayList<>();
				
		if(CompareUtils.changed(newEntity.getNome(), oldEntity.getNome())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getNome(), oldEntity.getNome(), ECampo.USUARIO_NOME.name()));
		}
		
		if(CompareUtils.changed(newEntity.getLogin(), oldEntity.getLogin())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getLogin(), oldEntity.getLogin(), ECampo.USUARIO_LOGIN.name()));
		}
		
		if(CompareUtils.changed(newEntity.getSenha(), oldEntity.getSenha())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getSenha(), oldEntity.getSenha(), ECampo.USUARIO_SENHA.name()));
		}
		
		if(CompareUtils.changed(newEntity.getSiglaPerfil(), oldEntity.getSiglaPerfil())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getSiglaPerfil(), oldEntity.getSiglaPerfil(), ECampo.USUARIO_PERFIL.name()));
		}
		
		if(CompareUtils.changed(newEntity.getRegistroAtivo(), oldEntity.getRegistroAtivo())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getRegistroAtivo(), oldEntity.getRegistroAtivo(), ECampo.REGISTRO_ATIVO.name()));
		}
		
		Set<String> carteiraNew = newEntity.getCarteiraList().stream().map( c -> c.getNome()).collect(Collectors.toSet());
		Set<String> carteiraOld = oldEntity.getCarteiraList().stream().map( c -> c.getNome()).collect(Collectors.toSet());
		
		if(!carteiraOld.equals(carteiraNew)) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(carteiraNew, carteiraOld, ECampo.USUARIO_CARTEIRA.name()));
		}
		
		atividade.setAtividadeAcaoList(acaoList);
		
		return atividade;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Usuario save(UsuarioDTO dto, Long id) throws BaseServiceException {
		Usuario entity = id != null ? findById(id) : new Usuario();
		
		UsuarioDTO oldDTO = toDTO(entity);
		
		entity = toEntity(entity, dto);
		if(dto.getSenha() != null && !dto.getSenha().trim().isEmpty()) {
			entity = encodePassword(entity);
		}
						
		Usuario novo = saveWithCarteira(entity, dto);
		
		List<Long> idCarteiras = dto.getCarteiraList().stream()
				.mapToLong( carteira -> carteira.getId())
				.boxed()
				.collect(Collectors.toList());
		
		Iterable<Carteira> carteirasIt = carteiraRepository.findAllById(idCarteiras);
		List<Carteira> carteiraList = new ArrayList<>();
	    carteirasIt.forEach(carteiraList::add);
	    
	    UsuarioDTO newDTO = toDTO(novo);
	    newDTO.setCarteiraList(carteiraService.toDTO(carteiraList));

		
		AtividadeDTO atividadeDTO = createAtividade(newDTO, oldDTO, id == null);
		atividadeService.save(atividadeDTO);
		
		return novo;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Usuario save(UsuarioDTO dto) throws BaseServiceException {
		return save(dto, null);
	}

}
