package com.verifik.carteira.application.service;

import java.util.List;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.model.base.entity.BaseEntity;
import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.dto.SearchResultDTO;

public interface IDefaultService<T extends BaseEntity, R> {
	
	public List<T> findAll();

	T findById(long id);

	T save(T entity) throws BaseServiceException;

	void delete(long id);
	
	public List<T> search(BaseSearch baseSearch);
	
	public long count(BaseSearch baseSearch);
	
	public SearchResultDTO pagedSearch(BaseSearch baseSearch);
	
	public R toDTO(T entity);
	
	public List<R> toDTO(List<T> entityList);
	
	public T toEntity(R dto);
	
	public T toEntity(T entity, R dto);
	
	public BaseSearch applyRules(BaseSearch baseSearch);
	
	
}
