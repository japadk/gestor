package com.verifik.carteira.application.service;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.model.base.entity.BaseEntity;
import com.verifik.carteira.application.model.dto.AtividadeDTO;

public interface IDefaultAtividadeService<T extends BaseEntity, R> extends IDefaultService<T, R> {
		
	AtividadeDTO createAtividade(R newEntity, R oldEntity, boolean novo);

	T save(R dto, Long id) throws BaseServiceException;

	T save(R dto) throws BaseServiceException;
}
