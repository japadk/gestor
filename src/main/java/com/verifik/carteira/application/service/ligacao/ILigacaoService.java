package com.verifik.carteira.application.service.ligacao;

import com.verifik.carteira.application.model.dto.LigacaoDTO;
import com.verifik.carteira.application.model.entities.Ligacao;
import com.verifik.carteira.application.service.IDefaultAtividadeService;

import java.time.LocalDate;
import java.util.List;

public interface ILigacaoService extends IDefaultAtividadeService<Ligacao, LigacaoDTO> {

	LigacaoDTO toDTO(Ligacao entity, boolean semRetorno);

	List<LigacaoDTO> toDTO(List<Ligacao> entityList, boolean semRetorno);

    List<Ligacao> findLigacoesPorPeriodo(LocalDate dataInicio, LocalDate dataFim);
}
