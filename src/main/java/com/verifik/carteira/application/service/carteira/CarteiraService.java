package com.verifik.carteira.application.service.carteira;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.verifik.carteira.application.exceptions.base.BaseServiceException;
import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.dto.AtividadeAcaoDTO;
import com.verifik.carteira.application.model.dto.AtividadeDTO;
import com.verifik.carteira.application.model.dto.CarteiraDTO;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.model.entities.Perfil;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.enums.ECampo;
import com.verifik.carteira.application.model.enums.EPerfil;
import com.verifik.carteira.application.model.search.CarteiraSearch;
import com.verifik.carteira.application.repository.CarteiraRepository;
import com.verifik.carteira.application.repository.PerfilRepository;
import com.verifik.carteira.application.service.atividade.IAtividadeService;
import com.verifik.carteira.application.service.usuario.IUsuarioService;
import com.verifik.carteira.application.utils.CompareUtils;
import com.verifik.carteira.application.utils.SearchUtils;

@Service
public class CarteiraService implements ICarteiraService {

	@Autowired 
	CarteiraRepository carteiraRepository;
	
	@Autowired
	PerfilRepository perfilRepository;

	@Autowired
	IUsuarioService usuarioService;
	
	@Autowired
	IAtividadeService atividadeService;
		
	@Override
	public List<Carteira> findAll() {
		return carteiraRepository.findAll();
	}
	
	@Override
	public Carteira findById(long id) {
		
		Optional<Carteira> carteira = carteiraRepository.findById(id);

        Usuario user  = usuarioService.getCurrentUser();
        Carteira c = carteira.orElseGet(() -> null);
        if(c == null){
            return null;
        }

        if(!usuarioService.hasCarteira(c, user)){
            return null;
        }
		
		return c;
		
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Carteira save(Carteira entity) throws BaseServiceException {
		if(entity.getRegistroAtivo() == null) {
			entity.setRegistroAtivo(true);
		}
		
		return  carteiraRepository.save(entity);
		
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Carteira save(CarteiraDTO dto) throws BaseServiceException {
		return save(dto, null);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Carteira save(CarteiraDTO dto, Long id) throws BaseServiceException {
		
		Carteira entity = id != null ? findById(id) : new Carteira();
		
		CarteiraDTO oldDTO = toDTO(entity);
		
		entity = toEntity(entity, dto);
						
		Carteira novo = save(entity);
		AtividadeDTO atividadeDTO = createAtividade(toDTO(novo), oldDTO, id == null);
		atividadeService.save(atividadeDTO);
		
		return novo;
		
	}
	
	@Override
	public void delete(long id) {
		carteiraRepository.deleteById(id);
	}
	
	@Override
	public BaseSearch applyRules(BaseSearch baseSearch) {
		
		CarteiraSearch search = (CarteiraSearch) baseSearch;
		
		Usuario usuario = usuarioService.getCurrentUser();
		
		Perfil perfil = usuario.getPerfil();
		if(!perfil.getSigla().equals(EPerfil.SIGLA.ADMIN)) {
			search.setIdUsuarioVinculado(usuario.getId());
		}
		
		return baseSearch;
	}

	@Override
	public List<Carteira> search(BaseSearch baseSearch) {
		return carteiraRepository.findBySearch(baseSearch);
	}
	
	@Override
	public long count(BaseSearch baseSearch) {
		return carteiraRepository.countBySearch(baseSearch);
	}
	
	@Override
	public SearchResultDTO pagedSearch(BaseSearch baseSearch) {
		return SearchUtils.runPagedSearch(this,baseSearch);
	}
	
	@Override
	public CarteiraDTO toDTO(Carteira entity) { 
		
		return CarteiraDTO.builder()
				.withId(entity.getId())
				.withNome(entity.getNome())
				.withRegistroAtivo(entity.getRegistroAtivo())
				.build();
		
	}
	
	@Override
	public List<CarteiraDTO> toDTO(List<Carteira> entityList) {
		if(entityList == null) {
			return new ArrayList<>();
		}
		
		List<CarteiraDTO> result = new ArrayList<>(entityList.size());
		for(Carteira entity : entityList) {
			result.add(toDTO(entity));
		}
		
		return result;
	}

	@Override
	public Carteira toEntity(CarteiraDTO dto) {
		return toEntity(new Carteira(), dto);
	}
	
	@Override
	public Carteira toEntity(Carteira entity, CarteiraDTO dto) {
		
		if(dto.getId() != null && (entity.getId() == null || entity.getId().longValue() == 0)) {
			entity.setId(dto.getId());			
		}
		
		if(dto.getNome() != null) {
			entity.setNome(dto.getNome().trim());
		}
		
		if(dto.getRegistroAtivo() != null) {
			entity.setRegistroAtivo(dto.getRegistroAtivo());
		}
		
		return entity;
	}

	@Override
	public AtividadeDTO createAtividade(CarteiraDTO newEntity, CarteiraDTO oldEntity, boolean novo) {
		
		AtividadeDTO atividade = atividadeService.createAtividadeDTO(Carteira.class.getSimpleName(), newEntity.getId(), novo);
		
		List<AtividadeAcaoDTO> acaoList = new ArrayList<>();
				
		if(CompareUtils.changed(newEntity.getNome(), oldEntity.getNome())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getNome(), oldEntity.getNome(), ECampo.CARTEIRA_NOME.name()));
		}
		
		if(CompareUtils.changed(newEntity.getRegistroAtivo(), oldEntity.getRegistroAtivo())) {
			acaoList.add(atividadeService.createAtividadeAcaoDTO(newEntity.getRegistroAtivo(), oldEntity.getRegistroAtivo(), ECampo.REGISTRO_ATIVO.name()));
		}
		
		atividade.setAtividadeAcaoList(acaoList);
		
		return atividade;
	}

}
