package com.verifik.carteira.application.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FileUtils {
    
    private static final String tempExtention = ".temp";
    
    private static final String[] tempExtensionLists = {
            tempExtention
    };
    
    public static String getUserDir() {
        return System.getProperty("user.dir");
    }

    public static Path writeToNewTempFile(String content) throws IOException {
        return writeToNewFile(System.getProperty("java.io.tmpdir") + "\\" + System.currentTimeMillis() + tempExtention, content);
    }

    public static Path writeToNewFile(String filename, String content) throws IOException {
        Path path = Paths.get(filename);
        return Files.write(path, content.getBytes(),StandardOpenOption.CREATE);
    }
    
    public static Path writeToBeginningOfFile(String filename, String content) throws IOException {
        Path path = Paths.get(filename);
        
        List<String> arquivo = new ArrayList<>();
        arquivo.add(content);
        if(path.toFile().exists()){
            arquivo.addAll(Files.readAllLines(path));
        }
        
        Files.write(path, arquivo,StandardOpenOption.CREATE);
        return path;
    }
    
    public static Path writeToEndOfFile(String filename, String content) throws IOException {
        Path path = Paths.get(filename);
        
        List<String> arquivo = new ArrayList<>();
        if(path.toFile().exists()){
            arquivo.addAll(Files.readAllLines(path));
        }
        arquivo.add(content);
        
        Files.write(path, arquivo,StandardOpenOption.CREATE);
        return path;
    }
    
    public static boolean isJar() throws URISyntaxException {
        URI resource = FileUtils.class.getClassLoader().getResource("db").toURI();
        return resource.getScheme().contains("jar");
    }
    
    public static List<File> findFiles(String dir, String ext) {
        File folder = new File(dir);
        if(!folder.exists()) {
            return new ArrayList<>(0);
        }
        List<File> lista = new ArrayList<>();

        Arrays.stream(folder.listFiles((f, p) -> {
            return ext == null ? true : p.endsWith(ext);
        })).forEach((f) ->{
            lista.add(f);
        });
        
        Collections.sort(lista, (o1,o2) ->{
            return o1.getName().compareToIgnoreCase(o2.getName());
        });
        return lista;
        
    }
    
    public static void clearTempFiles() {
        File folder = new File(System.getProperty("java.io.tmpdir"));
        Arrays.stream(folder.listFiles((f, p) -> {
            for(String ext : tempExtensionLists) {
                if(p.endsWith(ext)) {
                    return true;
                }
            }
            return false;
        })).forEach(File::delete);
    }
    
    public static void deleteFile(String filename) throws IOException {
        Path path = Paths.get(filename);
        Files.delete(path);
    }
    
    public static String readResourceFile(String file) throws IOException, URISyntaxException {
        URL resource = FileUtils.class.getClassLoader().getResource(file);
        
        if(resource == null) {
            return ""; 
        }
        
        if(isJar()) {
            InputStream in = FileUtils.class.getResourceAsStream(file.startsWith("/") ? file : "/" +file); 
            return new BufferedReader(new InputStreamReader(in))
                    .lines().collect(Collectors.joining("\n"));
        }else {
            File f = new File(resource.getFile());
            return new String (Files.readAllBytes(f.toPath()),Charset.forName("UTF-8"));            
        }
        
    }
    
}
