package com.verifik.carteira.application.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class StringUtils {

	public static String toString(Object object) {
		
		if(object == null) {
			return null;
		}
		
		if(object instanceof String) {
			return (String) object;
		}
		
		if(object instanceof Boolean) {
			return (((Boolean) object).booleanValue()) ? "SIM" : "NÃO";
		}
		
		if(object instanceof Date) {
			return new SimpleDateFormat("dd/MM/yyyy").format(object);
		}
		
		if(object instanceof LocalDate) {
			return ((LocalDate)object).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		}
		
		if(object instanceof LocalTime) {
			return ((LocalTime)object).format(DateTimeFormatter.ofPattern("HH:mm:ss"));
		}
		
		if(object instanceof BigDecimal) {
			return ((BigDecimal) object).setScale(2, RoundingMode.HALF_EVEN).toPlainString();
		}
		
		return object.toString();
		
	}

	

}
