package com.verifik.carteira.application.utils;

import com.verifik.carteira.application.model.entities.Carteira;
import com.verifik.carteira.application.model.entities.Cliente;
import com.verifik.carteira.application.model.entities.Perfil;
import com.verifik.carteira.application.model.entities.Usuario;
import com.verifik.carteira.application.model.enums.EPerfil;

public class TestUtils {

	public static Usuario buildMockUser() {
		Usuario usuario = new Usuario();
		usuario.setNome(RandomingUtils.randomAlphanumeric(10));
		usuario.setSenha(RandomingUtils.randomAlphanumeric(10));
		usuario.setLogin(RandomingUtils.randomAlphanumeric(10));
		usuario.setRegistroAtivo(true);
		return usuario;
	}

	public static Cliente buildMockCliente() {
		Cliente cliente = new Cliente();
		cliente.setNome(RandomingUtils.randomAlphanumeric(10));
		cliente.setCnpj(RandomingUtils.cnpj(false));
		cliente.setRegistroAtivo(true);
		
		return cliente;
	}

	public static Perfil buildMockPerfil() {
		Perfil perfil = new Perfil();
		EPerfil sigla = EPerfil.values()[RandomingUtils.getRandomNumberInRange(0, EPerfil.values().length - 1)];

		perfil.setDescricao(sigla.toString());
		perfil.setSigla(sigla.toString());
		perfil.setRegistroAtivo(true);
		return perfil;
	}
	
	public static Carteira buildMockCarteira() {
		
		Carteira carteira = new Carteira();
		carteira.setNome(RandomingUtils.randomAlphanumeric(10));
		carteira.setRegistroAtivo(true);
		
		return carteira;
	}

}
