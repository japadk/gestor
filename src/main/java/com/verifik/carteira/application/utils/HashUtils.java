package com.verifik.carteira.application.utils;

import java.security.MessageDigest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HashUtils {
	

	private static final Logger logger = LoggerFactory.getLogger(HashUtils.class);
	
	public static String md5(byte[] data) {
		return md5(data, null);
	}
	
	public static String md5(byte[] data, String salt) {
		try {    
		    MessageDigest md = MessageDigest.getInstance("MD5");
		    md.update(data);
		    
		    if (salt != null) {
		        md.update(salt.getBytes());
		    }
		    
		    byte[] byteData = md.digest();
		    
		    StringBuilder sb = new StringBuilder();
		    for (int i = 0; i < byteData.length; i++) {
		        sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
		                .substring(1));
		    }
		    
		    return sb.toString().toUpperCase();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return null;
		}
		
	}

}
