package com.verifik.carteira.application.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.verifik.carteira.application.model.base.search.BaseSearch;
import com.verifik.carteira.application.model.dto.SearchResultDTO;
import com.verifik.carteira.application.service.IDefaultService;

public class SearchUtils {
	
	private static final Set<String> ignoredFields;
	
	private static Logger logger = LoggerFactory.getLogger(SearchUtils.class);
	
	static {
		ignoredFields = new HashSet<String>();
		ignoredFields.add("or");
		ignoredFields.add("limit");
		ignoredFields.add("offset");
		ignoredFields.add("orderBy");
		ignoredFields.add("orderByList");
		ignoredFields.add("desc");
		ignoredFields.add("registroAtivo");
	}
	
	
	public static Map<String, Object> mapValues(BaseSearch search){
		
		Map<String,Object> fieldMap = new HashMap<>();

		fillMapValues(search.getClass(), search, fieldMap);
		fillMapValues(search.getClass().getSuperclass(), search, fieldMap);
		
		return fieldMap;
		
	}
	
	private static void fillMapValues(Class clazz, Object object, Map<String, Object> fieldMap) {
		Arrays.asList(clazz.getDeclaredFields()).forEach(field -> {
			field.setAccessible(true);
			try {
				fieldMap.put(field.getName(),field.get(object));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				logger.error("Erro ao buscar campo " + field.getName() + " da classe " + object.getClass(), e);
			}
		});
	}
	
	public static List<Predicate> buildPredicateList(CriteriaBuilder builder, Root root, BaseSearch baseSearch){
		
		List<Predicate> restrictions = new LinkedList<>();
		
		Map<String,Object> fields = mapValues(baseSearch);
		
		//Remove os campos ignorados pela baseSearch
		baseSearch.getIgnoredFields().forEach( ignoredField -> {
			fields.remove(ignoredField);
		});

		
		for(Entry<String, Object> entry : fields.entrySet()) {
			
			String name = entry.getKey();
			Object value = entry.getValue();
			
			if(value == null || ignoredFields.contains(name)) {
				continue;
			}
			
			if(value instanceof String) {
				restrictions.add(builder.like(builder.lower(root.get(name)), ((String) value).toLowerCase() ));
			} else if(value instanceof Integer || value instanceof Long || value instanceof Float || value instanceof Double || value instanceof Boolean) {
				restrictions.add(builder.equal(root.get(name), value ));
			}
			
		}
		
		// Adiciona as regras customizadas
		restrictions.addAll(baseSearch.getCustomRules(builder, root));
		
		return restrictions;
		
	}
	
	public static SearchResultDTO runPagedSearch(IDefaultService service, BaseSearch baseSearch) {
		BaseSearch search = service.applyRules(baseSearch);
		
		long totalCount = service.count(search);
		List found = service.search(search);
		
		return SearchResultDTO.builder()
				.withTotalCount(totalCount)
				.withCurrentCount(found.size())
				.withList(service.toDTO(found))
				.withLimit(baseSearch.getLimit())
				.withOffset(baseSearch.getOffset())
				.build();
		
	}
	
	public static Expression getJoinExpression(String forJoin, Root root) {
		String[] dotSplit = forJoin.split("\\.");
		Join join = null;
		Expression exp = null;
		for(int i = 0; i < dotSplit.length; i++) {
			if(i != dotSplit.length - 1) {        				
				join = root.join(dotSplit[i].trim());
			} else {
				if(join == null) {
					exp = root.get(forJoin.trim());
				} else {        					
					exp = join.get(dotSplit[i].trim());
				}
			}
		}
		return exp;
	}

}
