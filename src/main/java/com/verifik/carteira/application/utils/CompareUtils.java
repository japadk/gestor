package com.verifik.carteira.application.utils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

import com.verifik.carteira.application.model.base.entity.BaseEntity;

public class CompareUtils {

	public static boolean oneIsNull(Object o1, Object o2) {
		// XOR
		return o1 == null ^ o2 == null;
	}

	public static boolean bothIsNull(Object o1, Object o2) {
		return o1 == null && o2 == null;
	}
	
	public static String nullIfEmpty(String s) {
		if(s == null) {
			return null;
		}
		
		if(s.trim().isEmpty()) {
			return null;
		}
		
		return s;
	}

	public static boolean changed(String s1, String s2) {

		s1 = nullIfEmpty(s1);
		s2 = nullIfEmpty(s2);
		
		// Se um esta nulo e outro não, então mudou
		if (oneIsNull(s1, s2)) {
			return true;
		}

		// Se os dois estão nulos, então não mudou
		if (bothIsNull(s1, s2)) {
			return false;
		}

		return s1.trim().compareTo(s2.trim()) != 0;

	}

	public static boolean changed(Boolean b1, Boolean b2) {

		// Se um esta nulo e outro não, então mudou
		if (oneIsNull(b1, b2)) {
			return true;
		}

		// Se os dois estão nulos, então não mudou
		if (bothIsNull(b1, b2)) {
			return false;
		}

		return b1.compareTo(b2) != 0;
	}
	
	public static boolean changed(Long l1, Long l2) {

		// Se um esta nulo e outro não, então mudou
		if (oneIsNull(l1, l2)) {
			return true;
		}

		// Se os dois estão nulos, então não mudou
		if (bothIsNull(l1, l2)) {
			return false;
		}

		return l1.compareTo(l2) != 0;
	}
	
	public static boolean changed(Date d1, Date d2) {

		// Se um esta nulo e outro não, então mudou
		if (oneIsNull(d1, d2)) {
			return true;
		}

		// Se os dois estão nulos, então não mudou
		if (bothIsNull(d1, d2)) {
			return false;
		}

		return d1.compareTo(d2) != 0;
	}
	
	public static boolean changed(LocalDate d1, LocalDate d2) {

		// Se um esta nulo e outro não, então mudou
		if (oneIsNull(d1, d2)) {
			return true;
		}

		// Se os dois estão nulos, então não mudou
		if (bothIsNull(d1, d2)) {
			return false;
		}

		return d1.compareTo(d2) != 0;
	}
	
	public static boolean changed(LocalTime t1, LocalTime t2) {

		// Se um esta nulo e outro não, então mudou
		if (oneIsNull(t1, t2)) {
			return true;
		}

		// Se os dois estão nulos, então não mudou
		if (bothIsNull(t1, t2)) {
			return false;
		}

		return t1.compareTo(t2) != 0;
	}
	
	public static boolean changed(BigDecimal bd1, BigDecimal bd2) {

		// Se um esta nulo e outro não, então mudou
		if (oneIsNull(bd1, bd2)) {
			return true;
		}

		// Se os dois estão nulos, então não mudou
		if (bothIsNull(bd1, bd2)) {
			return false;
		}

		return bd1.compareTo(bd2) != 0;
	}
	
	public static boolean changed(BaseEntity be1, BaseEntity be2) {

		// Se um esta nulo e outro não, então mudou
		if (oneIsNull(be1, be2)) {
			return true;
		}

		// Se os dois estão nulos, então não mudou
		if (bothIsNull(be1, be2)) {
			return false;
		}

		return be1.getId().compareTo(be2.getId()) != 0;
	}

}
