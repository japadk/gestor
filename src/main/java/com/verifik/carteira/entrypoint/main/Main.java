package com.verifik.carteira.entrypoint.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.verifik.carteira.application.configuration.StartupConfig;

@SpringBootApplication
@ComponentScan( { "com.verifik.carteira.application", "com.verifik.carteira.entrypoint.main"})
@EnableJpaRepositories({ "com.verifik.carteira.application.repository" })
@EntityScan({ "com.verifik.carteira.application.model.entities" })
public class Main {

	@Autowired(required = false)
	private StartupConfig startupConfig;

	public static void main(String[] args) {
		SpringApplication.run(Main.class,args);
	}

	@Bean
	@Profile({"!test"})
	public CommandLineRunner init() {

		return (args) -> {
			startupConfig.run();
		};

	}

}
