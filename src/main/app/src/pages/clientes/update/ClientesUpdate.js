import React, { useState, useCallback, useEffect } from 'react';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Alert, AlertTitle } from '@material-ui/lab';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import useHttp from '../../../hooks/useHttp';
import { Redirect, useParams } from 'react-router-dom';
import AlertDialog from '../../../components/AlertDialog/AlertDialog';
import InputMask from 'react-input-mask';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        width: '100%'
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        display: 'flex',
        flexDirection: 'column',
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const defaultError = {
    message: '',
    title: '',
    show: false
}

const defaultFormData = {
    nome: '',
    nomeFantasia: '',
    codigo: '',
    contato: '',
    cnpj: '',
    telefone: '',
    email: '',
    idCarteira: '',
    registroAtivo: true
}

function ClientesUpdate(props) {
    const classes = useStyles();

    let { id } = useParams();
    const [erro, setErro] = useState(defaultError);
    const [ignoreLoading] = useState(false);
    const [httpClient, loading, clientError] = useHttp();
    const [redirect, setRedirect] = useState(false);
    const [showDialog, setShowDialog] = useState(false);
    const [carteiras, setCarteiras] = useState([]);
    const [formData, setFormData] = useState(defaultFormData);

    useEffect(()=> {
        const params = {
            limit : -1 ,
            offset: 0 ,
            orderBy : 'nome',
            registroAtivo: true
        }
        httpClient.get('carteiras/search/',{ params : params })
        .then((response)=> {
            setCarteiras(response.data.list.map((carteira => {
                return {
                    id : carteira.id,
                    nome: carteira.nome,
                }
            })));
        }).catch((error)=> {
            console.log(error);
        })
    }, [httpClient, setCarteiras]);

    useEffect(()=> {
        httpClient.get(`/clientes/${id}`)
            .then((response)=> {
                setFormData({
                    nome: response.data.nome,
                    nomeFantasia: response.data.nomeFantasia,
                    codigo: response.data.codigo,
                    contato: response.data.contato,
                    cnpj: response.data.cnpj,
                    telefone: response.data.telefone,
                    email: response.data.email,
                    idCarteira: response.data.idCarteira,
                    registroAtivo: response.data.registroAtivo
                })
            }).catch((error)=> {
                console.log(error);
            })
    }, [httpClient, setFormData, id]);

    const autalizarHandler = useCallback(event => {
        event.preventDefault();
        if(erro.show){
            return;
        }
        if(formData.idCarteira === ''){
            setErro({
                show: true,
                title: 'Erro ao atualizar cliente',
                message: 'A carteira é obrigatória'
            });
            return;
        }
        httpClient.put(`/clientes/${id}`, {
            ...formData
        }).then((response)=>{
            setShowDialog(true);
        }).catch((error)=>{
            if(error.response.status === 400){
                setErro({
                    show: true,
                    title: 'Erro ao ataulizar cliente',
                    message: error.response.data.message
                });
            } else if(error.response.status === 500) {
                setErro({
                    show: true,
                    title: 'Erro ao atualizar cliente',
                    message: 'Erro inesperado do servidor. Tente novamente mais tarde'
                });
            }
            console.log(clientError);
            console.log(error);
        });
    }, [httpClient, erro, formData, clientError, setShowDialog, id]);

    const handleFormDataUpdate = useCallback((data)=> {
        setErro(defaultError);
        setFormData({
            ...formData,
            ...data
        })
    }, [setFormData, formData]);

    const handleListagemRedirect = useCallback(() => {
        setRedirect(true);
        setShowDialog(false);
    }, [setRedirect, setShowDialog]);

    const alertButtons = [{
            key: 1,
            handler: handleListagemRedirect,
            text: 'Voltar para listagem'
        }];


    return (
        <Container component="main" >
            <div className={classes.paper}>

                <Typography component="h1" variant="h5">
                    Atualizar um cliente
                </Typography>

                { (erro.show && !loading) && (
                    <Alert severity="error" className={classes.form}>
                        { erro.title && <AlertTitle>{erro.title}</AlertTitle> }
                        { erro.message } 
                    </Alert>) 
                }

                { (loading) && (!ignoreLoading) && <CircularProgress/> }

                { (showDialog) && <AlertDialog open text="Cliente atualizado com sucesso!" buttons={alertButtons} />}
                { (redirect) && <Redirect to="/clientes" push/>}

                <form className={classes.form} noValidate>
                    <FormControl >
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="nome"
                            label="Nome"
                            name="nome"
                            autoFocus
                            value={formData.nome}
                            onChange={(event)=>{handleFormDataUpdate({nome: event.target.value})}}
                        />
                    </FormControl>
                    <FormControl >
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="nomeFantasia"
                            label="Nome Fantasia"
                            name="nomeFantasia"
                            autoFocus
                            value={formData.nomeFantasia}
                            onChange={(event)=>{handleFormDataUpdate({nomeFantasia: event.target.value})}}
                        />
                    </FormControl>
                    <FormControl >
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="codigo"
                            label="Código"
                            name="codigo"
                            autoFocus
                            value={formData.codigo}
                            onChange={(event)=>{handleFormDataUpdate({codigo: event.target.value})}}
                        />
                    </FormControl>
                    <FormControl >
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="contato"
                            label="Contato"
                            name="contato"
                            autoFocus
                            value={formData.contato}
                            onChange={(event)=>{handleFormDataUpdate({contato: event.target.value})}}
                        />
                    </FormControl>
                    <FormControl >
                        <InputMask
                            value={formData.cnpj}
                            onChange={(event)=>{handleFormDataUpdate({cnpj: event.target.value})}}
                            mask="99.999.999/9999-99"
                        >
                            {() =>  <TextField
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        id="cnpj"
                                        label="CNPJ"
                                        name="cnpj"
                                        autoFocus
                                    />
                            }
                        </InputMask>
                    </FormControl>
                    <FormControl >
                        <InputMask
                            value={formData.telefone}
                            onChange={(event)=>{handleFormDataUpdate({telefone: event.target.value})}}
                            mask="(99)99999-9999"
                        >
                            { () => <TextField
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                id="telefone"
                                label="Telefone"
                                name="telefone"
                                autoFocus
                                
                            /> }
                        </InputMask>
                    </FormControl>
                    <FormControl >
                        <TextField
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="email"
                            label="Email"
                            name="email"
                            autoFocus
                            value={formData.email}
                            onChange={(event)=>{handleFormDataUpdate({email: event.target.value})}}
                        />
                    </FormControl>
                    <FormControl >
                        <TextField
                            id="carteira"
                            select
                            required
                            margin="normal"
                            label="Carteira"
                            variant="outlined"
                            value={formData.idCarteira}
                            onChange={(event)=>handleFormDataUpdate({idCarteira: event.target.value})}
                            >
                            {
                                carteiras.map((carteira) => {
                                    return <MenuItem key={carteira.id} value={carteira.id}>{carteira.nome}</MenuItem>
                                })
                            }
                        </TextField>
                    </FormControl>
                    <FormControl>
                        <FormControlLabel
                            label={formData.registroAtivo ? 'Cliente ativo' : 'Cliente inativo'}
                            control={
                                <Switch
                                    checked={formData.registroAtivo}
                                    onChange={(event)=>{handleFormDataUpdate({registroAtivo: !formData.registroAtivo})}}
                                    color="primary"
                                    name="registroAtivo"
                                    inputProps={{ 'aria-label': 'primary checkbox' }}
                                />
                            }
                        />
                    </FormControl>
                    <FormControl>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={autalizarHandler}
                        >
                            Atualizar
                        </Button>
                    </FormControl>
                </form>
            </div>
        </Container>
    );
}

export default ClientesUpdate;