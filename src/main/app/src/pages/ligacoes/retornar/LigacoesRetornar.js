import React, { useState, useCallback, useEffect, Fragment } from 'react';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Alert, AlertTitle } from '@material-ui/lab';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from '@material-ui/core/TextField';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import useHttp from '../../../hooks/useHttp';
import { Redirect, useParams } from 'react-router-dom';
import AlertDialog from '../../../components/AlertDialog/AlertDialog';
import DateFnsUtils from '@date-io/date-fns';
import {MuiPickersUtilsProvider, KeyboardDatePicker} from '@material-ui/pickers';
import locale from "date-fns/locale/pt-BR";
import { format } from 'date-fns';
import InputMask from 'react-input-mask';


import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        width: '100%'
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        display: 'flex',
        flexDirection: 'column',
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const defaultError = {
    message: '',
    title: '',
    show: false
}

const defaultFormData = {
    nome : '',
    cnpj : '',
    telefone : '',
    observacao : '',
    observacaoAnterior : '',
    precisaRetornar : false,
    dataRetorno: format(new Date(), 'dd/MM/yyy'),
    dataRetornoDisplay: new Date() ,
    horaRetorno : ''
};

function LigacoesRetornar(props) {
    const classes = useStyles();

    let { id } = useParams();

    const [erro, setErro] = useState(defaultError);
    const [ignoreLoading] = useState(false);
    const [httpClient, loading, clientError] = useHttp();
    const [redirect, setRedirect] = useState(false);
    const [showDialog, setShowDialog] = useState(false);
    const [formData, setFormData] = useState(defaultFormData);

    useEffect(()=> {
        httpClient.get(`/ligacoes/${id}`)
            .then((response)=> {
                const ligacao = response.data;

                setFormData({
                    nome: ligacao.nome,
                    cnpj: ligacao.cnpj,
                    telefone: ligacao.telefone,
                    observacaoAnterior: ligacao.observacao,
                })
            }).catch((error)=> {
                console.log(error);
            })
    }, [httpClient, setFormData, id]);

    const retornarHandler = useCallback(event => {
        event.preventDefault();
        if(erro.show){
            return;
        }

        let postData = {
            idLigacaoAnterior : id,
            observacao : formData.observacao,
        }

        if(formData.precisaRetornar){
            postData.retorno = {
                dataRetorno : formData.dataRetorno,
                horaRetorno : formData.horaRetorno
            }
        }

        httpClient.post(`/ligacoes`, {
            ...postData
        }).then((response)=>{
            setShowDialog(true);
        }).catch((error)=>{
            if(error.response.status === 400){
                setErro({
                    show: true,
                    title: 'Erro ao retornar ligacao',
                    message: error.response.data.message
                });
            } else if(error.response.status === 500) {
                setErro({
                    show: true,
                    title: 'Erro ao retornar ligacao',
                    message: 'Erro inesperado do servidor. Tente novamente mais tarde'
                });
            }
            console.log(clientError);
            console.log(error);
        });
    }, [httpClient, erro, formData, clientError, setShowDialog, id]);

    const handleFormDataUpdate = useCallback((data)=> {
        setErro(defaultError);
        setFormData({
            ...formData,
            ...data
        })
    }, [setFormData, formData]);

    const handleListagemRedirect = useCallback(() => {
        setRedirect(true);
        setShowDialog(false);
    }, [setRedirect, setShowDialog]);

    const alertButtons = [{
            key: 1,
            handler: handleListagemRedirect,
            text: 'Voltar para listagem'
        }
    ]


    return (
        <Container component="main" >
            <div className={classes.paper}>

                <Typography component="h1" variant="h5">
                    Retornar uma ligação
                </Typography>

                { (erro.show && !loading) && (
                    <Alert severity="error" className={classes.form}>
                        { erro.title && <AlertTitle>{erro.title}</AlertTitle> }
                        { erro.message } 
                    </Alert>) 
                }

                { (loading) && (!ignoreLoading) && (!ignoreLoading) && <CircularProgress/> }

                { (showDialog) && <AlertDialog open text="Ligação retornada com sucesso!" buttons={alertButtons} />}
                { (redirect) && <Redirect to="/retornos" push/>}

                <form className={classes.form} noValidate>
                    <FormControl >
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="nome"
                            label="Nome"
                            name="nome"
                            autoFocus
                            disabled
                            value={formData.nome}
                            onChange={(event)=>{handleFormDataUpdate({nome: event.target.value})}}
                        />
                    </FormControl>

                    <FormControl >
                        <InputMask
                            value={formData.cnpj}
                            onChange={(event)=>{handleFormDataUpdate({cnpj: event.target.value})}}
                            mask="99.999.999/9999-99"
                            disabled
                        >
                            {() =>  <TextField
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        id="cnpj"
                                        label="CNPJ"
                                        name="cnpj"
                                        autoFocus
                                    />
                            }
                        </InputMask>
                    </FormControl>

                    <FormControl >
                        <InputMask
                            value={formData.telefone}
                            onChange={(event)=>{handleFormDataUpdate({telefone: event.target.value})}}
                            mask="(99)99999-9999"
                            disabled
                            >
                            { () => <TextField
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                id="telefone"
                                label="Telefone"
                                name="telefone"
                                autoFocus
                                
                            /> }
                        </InputMask>
                    </FormControl>

                    <FormControl>
                        <TextField
                            multiline
                            rows={5}
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="observacaoanterior"
                            label="Observação Anterior"
                            name="observacaoanterior"
                            autoFocus
                            disabled
                            value={formData.observacaoAnterior}
                            onChange={(event)=>{handleFormDataUpdate({observacaoAnterior: event.target.value})}}
                        />
                    </FormControl>

                    <FormControl>
                        <TextField
                            multiline
                            rows={5}
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="observacao"
                            label="Observação"
                            name="observacao"
                            autoFocus
                            value={formData.observacao}
                            onChange={(event)=>{handleFormDataUpdate({observacao: event.target.value})}}
                        />
                    </FormControl>

                    <FormControl>
                        <FormControlLabel
                            label="Precisa retornar"
                            control={
                                <Switch
                                    checked={formData.precisaRetornar}
                                    onChange={(event)=>{handleFormDataUpdate({precisaRetornar: !formData.precisaRetornar})}}
                                    color="primary"
                                    name="precisaRetornar"
                                    inputProps={{ 'aria-label': 'primary checkbox' }}
                                />
                            }
                        />
                    </FormControl>

                    { formData.precisaRetornar && (
                        <Fragment>
                            <FormControl >
                                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={locale}>
                                    <KeyboardDatePicker
                                        variant="outlined"
                                        margin="normal"
                                        id="dataRetorno"
                                        name="dataRetorno"
                                        label="Data Retorno"
                                        format="dd/MM/yyyy"
                                        value={formData.dataRetornoDisplay}
                                        onChange={(data)=>{handleFormDataUpdate({dataRetorno: format(data, 'dd/MM/yyy'), dataRetornoDisplay: data})}}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                            </FormControl>
                            <FormControl >
                                <InputMask
                                    value={formData.horaRetorno}
                                    onChange={(event)=>{handleFormDataUpdate({horaRetorno: event.target.value})}}
                                    mask="99:99"
                                >
                                    { () => <TextField
                                        variant="outlined"
                                        margin="normal"
                                        fullWidth
                                        id="horaRetorno"
                                        label="Hora Retorno"
                                        name="horaRetorno"
                                        autoFocus
                                        
                                    /> }
                                </InputMask>
                            </FormControl>
                        </Fragment>
                    ) }

                    <FormControl>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={retornarHandler}
                        >
                            Atualizar
                        </Button>
                    </FormControl>
                </form>
            </div>
        </Container>
    );
}

export default LigacoesRetornar;